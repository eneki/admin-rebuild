<?php

namespace App\Orchid\Layouts\Invoice;

use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PaidInvoicesTable extends Table
{
    /**
     * Data source.
     *
     * @var string
     */
    protected $target = 'paid_invoices';

    /**
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('partner_name'),
            TD::set('Description','partner_app')->render(function($paid_invoice){
                return "Premium customers subscribed via {$paid_invoice['partner_app']}";
            }),
            TD::set('total_transactions'),
            TD::set('total_amount'),
            TD::set('action')->render(function($paid_invoice){
                return Link::make("View")
                    ->route('invoice.details',[
                            'partner_id' => $paid_invoice['partner_id'],
                            'partner_app_id' => $paid_invoice['partner_app_id'],
                        ])
                    ->icon('icon-eye');
            }),
            TD::set('invoiced')->render(function($paid_invoice){
                $status = $paid_invoice['invoiced'] == 1?"Paid":"Pending";
                return $status;
            }),
        ];
    }
}

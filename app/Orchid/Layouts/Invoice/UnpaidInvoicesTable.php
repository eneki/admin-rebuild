<?php

namespace App\Orchid\Layouts\Invoice;

use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class UnpaidInvoicesTable extends Table
{
    /**
     * Data source.
     *
     * @var string
     */
    protected $target = 'unpaid_invoices';

    /**
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('partner_name'),
            TD::set('Description','partner_app')->render(function($unpaid_invoice){
                return "Premium customers subscribed via {$unpaid_invoice['partner_app']}";
            }),
            TD::set('total_transactions'),
            TD::set('total_amount'),
            TD::set('action')->render(function($unpaid_invoice){
                return Link::make("View")
                    ->route('invoice.details',[
                            'partner_id' => $unpaid_invoice['partner_id'],
                            'partner_app_id' => $unpaid_invoice['partner_app_id'],
                        ])
                    ->icon('icon-eye');
            }),
            TD::set('invoiced')->render(function($unpaid_invoice){
                $status = $unpaid_invoice['invoiced'] == 1?"Paid":"Pending";
                return $status;
            }),
        ];
    }
}

<?php

namespace App\Orchid\Layouts\Partner;

use App\Models\Partners\Partner;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PartnerListLayout extends Table
{
    /**
     * Data source.
     *
     * @var string
     */
    protected $target = 'partners';

    /**
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('name', 'Name')
                ->render(function (Partner $partner) {
                    return Link::make($partner->name)
                        ->route('platform.partners.create', $partner);
                }),
            TD::set('type', 'Type'),
            TD::set('', 'Status')->render(function (Partner $partner) {
                if(!$partner->deleted_at) {
                    return 'Inactive';
                }
                return 'Active';
            }),
            TD::set('', 'Response Units')
                ->render(function (Partner $partner) {
                    return Link::make('Response Units')
                        ->route('partner.stations', $partner);
                }),
            TD::set('', 'Partner Since')
                ->render(function (Partner $partner) {
                    return $partner->created_at->diffForHumans();
                }),
            TD::set('', 'View')
                ->render(function (Partner $partner) {
                    return
                        Link::make('')
                            ->route('partner.details', $partner)
                            ->icon('icon-eye')
                    ;
                }),
        ];
    }
}

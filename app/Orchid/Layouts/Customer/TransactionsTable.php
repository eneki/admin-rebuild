<?php

namespace App\Orchid\Layouts\Customer;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class TransactionsTable extends Table
{
    /**
     * Data source.
     *
     * @var string
     */
    protected $target = 'customer_transactions';

    /**
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('name')->render(function($transaction){
                return "{$transaction['firstName']} {$transaction['lastName']}";
            }),
            TD::set('phone'),
            TD::set('organisation'),
            TD::set('invoice_number'),
            TD::set('duration_name'),
            TD::set('amount'),
            TD::set('created_at','Subscription Date'),
            TD::set('expires_at'),
            TD::set('partner_app','App'),
            TD::set('partner_name','Provider'),
            TD::set('service_name','Service'),
            TD::set('status'),
        ];
    }
}

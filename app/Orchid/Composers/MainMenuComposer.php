<?php

declare(strict_types=1);

namespace App\Orchid\Composers;

use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemMenu;
use Orchid\Platform\Menu;

class MainMenuComposer
{
    /**
     * @var Dashboard
     */
    private $dashboard;

    /**
     * MenuComposer constructor.
     *
     * @param  Dashboard  $dashboard
     */
    public function __construct(Dashboard $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    /**
     * Registering the main menu items.
     */
    public function compose()
    {
        $authUserId = (array) Auth::user()->id;

        // Profile
        //        $this->dashboard->menu
        //            ->add(Menu::PROFILE,
        //                ItemMenu::label('Action')
        //                    ->icon('icon-compass')
        //                    ->badge(function () {
        //                        return 6;
        //                    })
        //            )
        //            ->add(Menu::PROFILE,
        //                ItemMenu::label('Another action')
        //                    ->icon('icon-heart')
        //            );

        $this->dashboard->menu
            ->add(
                Menu::MAIN,
                ItemMenu::label('User Profile')
                    ->title('')
                    ->icon('icon-monitor')
                    ->route('platform.systems.users.edit', $authUserId)
            );

        // Partners
        if (Auth::user()->partner_id == 1) {
            $this->dashboard->menu
                ->add(
                    Menu::MAIN,
                    ItemMenu::label('Partners')
                        ->title('')
                        ->slug('partners-menu')
                        ->icon('icon-briefcase')
                        ->childs()
                )
                ->add(
                    'partners-menu',
                    ItemMenu::label('All Partners')
                        ->icon('icon-briefcase')
                        ->route('partners')
                )
                ->add(
                    'partners-menu',
                    ItemMenu::label('Add Partner')
                        ->icon('icon-plus')
                        ->route('partner.create')
                );
        }

        // Incidents
        $this->dashboard->menu
            ->add(
                Menu::MAIN,
                ItemMenu::label('Incidents')
                    ->title('')
                    ->slug('incidents-menu')
                    ->icon('icon-bell')
                    ->childs()
            )
            ->add(
                'incidents-menu',
                ItemMenu::label('Pending Incidents')
                    ->icon('icon-exclamation')
                    ->route('pending-incidents')
            )
            ->add(
                'incidents-menu',
                ItemMenu::label('Dispatched Incidents')
                    ->icon('icon-clock')
                    ->route('dispatched-incidents')
            )
            ->add(
                'incidents-menu',
                ItemMenu::label('Resolved Incidents')
                    ->icon('icon-close')
                    ->route('resolved-incidents')
            );

        // Customers
        $this->dashboard->menu
            ->add(
                Menu::MAIN,
                ItemMenu::label('Customers')
                    ->title('')
                    ->slug('customers-menu')
                    ->icon('icon-friends')
                    ->childs()
            )
            ->add(
                'customers-menu',
                ItemMenu::label('Free Customers')
                    ->icon('icon-number-list')
                    ->route('customers')
            )
            ->add(
                'customers-menu',
                ItemMenu::label('Premium Customers')
                    ->icon('icon-event')
                    ->route('subscriptions')
            );

        // Invoices
        if (Auth::user()->partner_id == 1) {
            $this->dashboard->menu
                ->add(
                    Menu::MAIN,
                    ItemMenu::label('Invoices')
                        ->title('')
                        ->icon('icon-note')
                        ->route('grouped-invoices')
                );
        }
    }
}

<?php

namespace App\Orchid\Cards;


use Orchid\Screen\Contracts\Cardable;
use Orchid\Screen\Layouts\Card;
use Orchid\Support\Color;

class PartnerCard implements Cardable
{
    private $partner;
    public function __construct($partner_id) {
        $this->partner = \App\Models\Partners\Partner::find($partner_id);
    }
    public function title(): string
    {
        return $this->partner->name;
    }

    public function description(): string
    {
        return '';
    }

    public function image(): ?string
    {
        return '';
    }

    public function color(): ?Color
    {
        return Color::INFO();
    }
}
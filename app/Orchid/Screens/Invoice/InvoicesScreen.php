<?php

namespace App\Orchid\Screens\Invoice;

use App\Orchid\Layouts\Invoice\{PaidInvoicesTable,UnpaidInvoicesTable};
use App\Repositories\TransactionRepository;
use Orchid\Screen\{Repository,Screen,Layout};

class InvoicesScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'InvoicesScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'InvoicesScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $paid_invoices = (new TransactionRepository($partner_id = null,$partner_app_id = null, $invoiced = true))
            ->fetchGroupedTransactions()
            ->get()->transform(function($invoice){
                $invoice = (array) $invoice;
                return new Repository($invoice);
            });

        $unpaid_invoices = (new TransactionRepository)
            ->fetchGroupedTransactions()
            ->get()->transform(function($invoice){
                $invoice = (array) $invoice;
                return new Repository($invoice);
            });
            
        return [
            'paid_invoices' => $paid_invoices,
            'unpaid_invoices' => $unpaid_invoices
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            UnpaidInvoicesTable::class,
            PaidInvoicesTable::class,
        ];
    }
}

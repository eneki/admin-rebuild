<?php

namespace App\Orchid\Screens\Invoice;

use App\Orchid\Layouts\Customer\TransactionsTable;
use App\Models\Partners\{Partner};
use App\Repositories\TransactionRepository;
use Orchid\Screen\Contracts\Cardable;
use Orchid\Screen\Layouts\Card;
use Orchid\Screen\{Repository,Screen};
use Orchid\Support\Color;

class InvoiceDetails extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'InvoiceDetails';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'InvoiceDetails';

    /**
     * Query data.
     *
     * @return array
     */
    public function query($partner_id,$partner_app_id): array
    {
        $transactions = (new TransactionRepository($partner_id,$partner_app_id))
                        ->getCustomerTransactions()
                        ->get()
                        ->transform(function($transaction){
                            $transaction = (array) $transaction;
                            return new Repository($transaction);
                        });
        return [
            'partner_card'        => new class($partner_id,$partner_app_id) implements Cardable {
                
                public function __construct($partner_id,$partner_app_id) {
                    $this->partner_id = $partner_id;
                    $this->partner_app_id = $partner_app_id;
                    $this->partner = Partner::find($this->partner_id);
                }

                public function title(): string
                {
                    return $this->partner->name;
                }
    
                public function description(): string
                {
                    $partner = $this->partner;
                    $physical_address = $partner->physical_address;
                    $postal_address = $partner->postal_address;
                    $postal_code = $partner->postal_code;
                    $city = $partner->city;
                    return "
                        {$physical_address},<br>
                        {$postal_address} {$postal_code} {$city}<br><br>
                        <strong>Invoice to</strong>
                        N/A
                    ";
                }
    
                public function image(): ?string
                {
                    return '';
                }
    
                public function color(): ?Color
                {
                    return Color::INFO();
                }
            },
            'customer_transactions' => $transactions,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            new Card('partner_card'),
            TransactionsTable::class,
        ];
    }
}

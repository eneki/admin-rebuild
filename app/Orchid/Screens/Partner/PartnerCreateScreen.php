<?php

namespace App\Orchid\Screens\Partner;

use App\Models\Partners\Partner;
use App\Models\Users\User;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class PartnerCreateScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Add Partner';

    /**
     * @var bool
     */
    public $exists = false;

    /**
     * Query data.
     *
     * @param  Partner  $partner
     * @return array
     */
    public function query(Partner $partner): array
    {
        $this->exists = $partner->exists;

        if ($this->exists) {
            $this->name = 'Edit partner';
        } else {
            $this->name = 'Create partner';
        }

        return [
            'partner' => $partner
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('partner.name')
                    ->title('Title')
                    ->placeholder('Attractive but mysterious title')
                    ->help('Specify a short descriptive title for this post.'),

                TextArea::make('post.description')
                    ->title('Description')
                    ->rows(3)
                    ->maxlength(200)
                    ->placeholder('Brief description for preview'),

                Relation::make('post.author')
                    ->title('Author')
                    ->fromModel(User::class, 'name'),

                Quill::make('post.body')
                    ->title('Main text'),

            ])
        ];
    }

    /**
     * @param  Partner  $partner
     * @param  Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Partner $partner, Request $request)
    {
        $partner->fill($request->get('partner'))->save();

        Alert::info('You have successfully created an post.');

        return redirect()->route('partners');
    }

    /**
     * @param  Partner  $partner
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Partner $partner)
    {
        $partner->delete()
            ? Alert::info('You have successfully deleted the post.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('partners');
    }
}

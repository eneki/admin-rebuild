<?php

namespace App\Orchid\Screens\Partner;

use App\Models\Partners\Partner;
use App\Orchid\Layouts\Partner\PartnerListLayout;
use App\Models\Users\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Screen\Fields\Relation;
use Orchid\Support\Facades\Alert;

class PartnerListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Partners';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'List of all partners - both premium and regular';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'partners' => Partner::all()->paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Create new')
                ->icon('icon-pencil')
                ->route('platform.partners.create')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            PartnerListLayout::class
        ];
    }

    public function sendMessage(Request $request)
    {
        $request->validate([
            'subject' => 'required|min:6|max:50',
            'users'   => 'required',
            'content' => 'required|min:10'
        ]);

        Mail::raw($request->get('content'), function (Message $message) use ($request) {

            $message->subject($request->get('subject'));

            foreach ($request->get('users') as $email) {
                $message->to($email);
            }
        });

        Alert::info('Your email message has been sent successfully.');
        return back();
    }
}

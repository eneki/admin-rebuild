<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Orchid\Platform\Models\User as Authenticatable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use Notifiable, SoftDeletes;

    protected $guarded = [
        'id'
    ];

    protected $appends = ['member_since', 'fullName', 'profile_pic'];
    protected $table = 'users';

    protected $fillable = [
        'partner_id',
        'firstName',
        'lastName',
        'email',
        'phone',
        'role',
        'id_number',
        'password',
        'permissions',
        'last_login'
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

//    /**
//     * The attributes that should be cast to native types.
//     *
//     * @var array
//     */
//    protected $casts = [
//        'permissions'       => 'array',
//        'email_verified_at' => 'datetime',
//        'last_login'        => 'datetime',
//    ];
//
//    /**
//     * The attributes for which you can use filters in url.
//     *
//     * @var array
//     */
//    protected $allowedFilters = [
//        'id',
//        'name',
//        'email',
//        'permissions',
//    ];
//
//    /**
//     * The attributes for which can use sort in url.
//     *
//     * @var array
//     */
//    protected $allowedSorts = [
//        'id',
//        'name',
//        'email',
//        'last_login',
//        'updated_at',
//        'created_at',
//    ];

    public function getFullNameAttribute()
    {
        return ucwords(strtolower($this->firstName)).' '.ucwords(strtolower($this->lastName));
    }

    public function getMemberSinceAttribute()
    {
        $date = $this->created_at->format('dS M Y');
        return $date;
    }

    public function partner()
    {
        return $this->belongsTo('App\Models\Partners\Partner', 'partner_id');
    }

    public function getProfilePicAttribute()
    {
        if (Storage::exists($this->image_url)) {
            return Storage::url($this->image_url);
        }
        return Storage::url('/img/face.png');
    }
}

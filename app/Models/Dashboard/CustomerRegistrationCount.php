<?php

namespace App\Models\Incidents;

use Illuminate\Database\Eloquent\Model;

class CustomerRegistrationCount extends Model
{
    protected $table = 'customer_registration_counts';

    protected $guarded = ['id'];
    
    public function partner()
    {
        return $this->belongsTo('App\Models\Partners\Partner');
    }
}

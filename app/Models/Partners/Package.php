<?php

namespace App\Models\Partners;

use App\Models\Subscriptions\Packages\PackagePricing;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;

    protected $table = 'packages';
    protected $guarded = ['id'];

    public function service()
    {
        return $this->belongsTo('App\Models\Incidents\Service');
    }
}

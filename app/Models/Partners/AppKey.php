<?php

namespace App\Models\Partners;

use Illuminate\Database\Eloquent\Model;

class AppKey extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'secret_key',
        'partner_id',
        'app_name',
        'channel'
    ];

    protected $table = 'appkeys';
}

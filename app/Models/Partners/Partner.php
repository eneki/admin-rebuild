<?php

namespace App\Models\Partners;

use App\Models\Incidents\Report;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Orchid\Screen\AsSource;
use Illuminate\Support\Facades\Storage;

class Partner extends Model
{
    use AsSource, SoftDeletes;

    protected $fillable = [
        'name',
        'physical_address',
        'postal_address',
        'postal_code',
        'city',
        'bank_name',
        'account_name',
        'account_number',
        'type',
        'image_id',
        'sms_balance',
        'sms_status',
        'image_url',
        'phone',
        'code',
        'partner_id',
        'status'
    ];

    protected $table = 'partners';
    protected $appends = ['image'];

    public function getTypeTitleAttribute()
    {
        return ucwords($this->type);
    }

    public function standardPartner()
    {
        return $this->hasOne('App\Models\Users\Partner', 'partner_id');
    }

    public function users()
    {
        return $this->hasMany('App\Models\Users\User');
    }

    public function packages()
    {
        return $this->hasMany('App\Models\Partners\Package');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Incidents\Service', 'partnerServices', 'partner_id', 'service_id');
    }

    public function reports()
    {
        return $this->hasMany(Report::class);
    }

    public function getImageAttribute(){
        if (Storage::exists($this->image_url)) {
            return Storage::url($this->image_url);
        }
        return Storage::url('/img/face.png');
    }
}

<?php
/**
 *
 * @author: Joan Eneki <jeneki@cytonn.com>
 *
 * Project: new-upesy-admin-web-app.
 *
 */

namespace App\Models\Subscriptions;

use App\Models\Incidents\Service;
use App\Models\Partners\Partner;
use App\Models\Subscriptions\Packages\PackagePricing;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    use SoftDeletes;
    
    protected $table = 'subscriptions';
    protected $appends = ['expiration_date', 'subscription_date'];
    protected $guarded = ['id'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }

    public function packagePricing()
    {
        return $this->belongsTo(PackagePricing::class, 'package_id');
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method');
    }

    public function providing_partner()
    {
        return $this->hasOne(SubscriptionProvider::class);
    }

    public function getExpirationDateAttribute()
    {
        $expiration_datetime = new \DateTime($this->expires_at);

        $date = $expiration_datetime->format('dS M Y');
        $time = $expiration_datetime->format('g:ia');

        return $date . ' at ' . $time;
    }

    public function getSubscriptionDateAttribute()
    {
        $date = $this->created_at->format('dS M Y');
        $time = $this->created_at->format('g:ia');

        return $date . ' at ' . $time;
    }
}

<?php

namespace App\Models\Subscriptions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentHistory extends Model
{
    use SoftDeletes;
    
    protected $table = 'paymenthistory';
    protected $guarded = ['id'];
    protected $dates = ['invoice_date'];
    protected $appends = ['modified_invoice_date', 'modified_payment_date'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function getModifiedInvoiceDateAttribute()
    {
        $date = $this->invoice_date->format('dS F Y');
        return $date;
    }

    public function getModifiedPaymentDateAttribute()
    {
        $date = $this->created_at->format('dS F Y');
        return $date;
    }
}

<?php

namespace App\Models\Subscriptions;

use App\Models\Beneficiaries\Beneficiary;
use App\Models\Beneficiaries\CustomerBeneficiary;
use App\Models\Incidents\Report;
use App\Models\Partners\Partner;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Customer extends Model
{
    use SoftDeletes;
    
    protected $table = 'customers';
    protected $appends = ['member_since', 'fullName', 'age', 'profile_pic'];
    protected $guarded = ['id'];

    public function getFullNameAttribute()
    {
        return ucwords(strtolower($this->firstName)).' '.ucwords(strtolower($this->lastName));
    }

    public function getMemberSinceAttribute()
    {
        $date = $this->created_at ? $this->created_at->format('dS F Y') : 'Not Known';
        $time = $this->created_at->format('g:ia');

        return $date . ' at ' . $time;
    }

    public function getAgeAttribute()
    {
        if(!$this->dob) {
            return 'Unknown';
        }
        $dateOfBirth = Carbon::parse($this->dob);
        $today = Carbon::today();
        $age = $today->diffInYears($dateOfBirth);
        return $age;
    }

    public function getProfilePicAttribute(){
        if(Storage::exists($this->image_url)) {
            return Storage::url($this->image_url);
        }
        return Storage::url('/img/face.png');
    }

    public function partner()
    {
        return $this->belongsTo(Partner::class, 'partner_id');
    }

    public function friends()
    {
        return $this->hasMany(Friend::class, 'friend_id');
    }

    public function reportedIncidents()
    {
        return $this->hasMany(Report::class, 'reported_by');
    }

    public function residences()
    {
        return $this->hasMany(CustomerLocation::class, 'customer_id');
    }

    public function beneficiaries()
    {
        return $this->hasMany(Beneficiary::class, 'customer_id');
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'customer_id');
    }
}

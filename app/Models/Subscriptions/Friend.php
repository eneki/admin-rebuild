<?php

namespace App\Models\Subscriptions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Friend extends Model
{
    use SoftDeletes;

    protected $table = 'friends';
    protected $guarded = ['id'];

    const UPDATED_AT = null;

    public function dependant()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function guardian()
    {
        return $this->belongsTo(Customer::class, 'friend_id');
    }
}

<?php
/**
 *
 * @author: Joan Eneki <jeneki@cytonn.com>
 *
 * Project: new-upesy-admin-web-app.
 *
 */

namespace App\Models\Subscriptions\Packages;

use App\Models\Partners\Package;
use App\Models\Partners\Partner;
use Illuminate\Database\Eloquent\Model;

class PackagePricing extends Model
{
    protected $table = 'packagesPricing';
    protected $guarded = ['id'];

    public function package()
    {
        return $this->belongsTo(Package::class);
    }
}

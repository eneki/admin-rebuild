<?php
/**
 *
 * @author: Joan Eneki <jeneki@cytonn.com>
 *
 * Project: new-upesy-admin-web-app.
 *
 */

namespace App\Models\Subscriptions;

use App\Models\Partners\Partner;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptionProvider extends Model
{
    use SoftDeletes;
    
    protected $table = 'subscription_providers';

    protected $guarded = ['id'];

    public function partner() {
        return $this->belongsTo(Partner::class);
    }
}

<?php

namespace App\Models\Beneficiaries;

use App\Models\Partners\Package;
use App\Models\Subscriptions\Customer;
use App\Models\Subscriptions\Subscription;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PendingBeneficiary extends Model
{
    use SoftDeletes;
    
    protected $table = 'pendingBeneficiaries';
    protected $guarded = ['id'];
    protected $appends = ['date_invited'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class, 'subscription_id');
    }

    public function package()
    {
        return $this->belongsTo(Package::class, 'partner_id');
    }

    public function getDateInvitedAttribute()
    {
        $date = $this->created_at->format('dS F Y');
        return $date;
    }
}

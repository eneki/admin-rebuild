<?php

namespace App\Models\Beneficiaries;

use App\Models\Subscriptions\Customer;
use Illuminate\Database\Eloquent\Model;

class Beneficiary extends Model
{
    protected $table = 'beneficiaries';
    protected $guarded = ['id'];
    protected $appends = ['member_since', 'fullName'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function getFullNameAttribute()
    {
        return ucwords(strtolower($this->first_name)) . ' ' . ucwords(strtolower($this->last_name));
    }

    public function getMemberSinceAttribute()
    {
        $date = $this->created_at ? $this->created_at->format('dS F Y') : 'Not Known';
        return $date;
    }
}

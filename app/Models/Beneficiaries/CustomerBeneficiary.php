<?php

namespace App\Models\Beneficiaries;

use App\Models\Partners\Package;
use App\Models\Subscriptions\Customer;
use App\Models\Subscriptions\Subscription;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerBeneficiary extends Model
{
    use SoftDeletes;
    
    protected $table = 'customerBeneficiaries';
    protected $guarded = ['id'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function beneficiary()
    {
        return $this->belongsTo(Customer::class, 'beneficiary_id');
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class, 'subscription_id');
    }

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id');
    }
}

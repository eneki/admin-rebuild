<?php

namespace App\Models\Incidents;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class ReportMedia extends Model
{
    use SoftDeletes;
    
    protected $table = 'reportMedia';

    protected $fillable = ['id'];

    protected $appends = ['report_media'];

    public function getReportMediaAttribute()
    {
        if(Storage::exists($this->name)) {
            return Storage::url($this->name);
        }
        return null;
    }
}

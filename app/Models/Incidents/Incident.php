<?php

namespace App\Models\Incidents;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Incident extends Model
{
    protected $table = 'incidents';
    
    public function partner()
    {
        return $this->belongsTo('App\Models\Partners\Partner');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Incidents\Service');
    }

    public function report()
    {
        return $this->hasOne('App\Models\Incidents\Report');
    }

    public function getProfilePicAttribute()
    {
        if (Storage::exists($this->image_url)) {
            return Storage::url($this->image_url);
        }
        return Storage::url('/img/face.png');
    }
}

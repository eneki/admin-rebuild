<?php

namespace App\Models\Incidents;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportLocation extends Model
{
    use SoftDeletes;
    
    protected $table = 'reportlocations';
}

<?php

namespace App\Models\Incidents;

use App\Models\Partners\Partner;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model
{
    use SoftDeletes;
    
    protected $table = 'reports';
    protected $appends = ['reported_on'];
    protected $guarded = ['id'];

    public function customer()
    {
        return $this->belongsTo('App\Models\Subscriptions\Customer', 'reported_by');
    }

    public function dispatchOfficer()
    {
        return $this->belongsTo(User::class, 'officer_id');
    }

    public function reportMedia()
    {
        return $this->hasMany(ReportMedia::class, 'report_id');
    }

    public function incident()
    {
        return $this->belongsTo('App\Models\Incidents\Incident');
    }

    public function reportLocations()
    {
        return $this->hasMany(ReportLocation::class);
    }

    public function getReportedOnAttribute()
    {
        $date = $this->created_at->format('dS F Y');
        $time = $this->created_at->format('g:ia');
        return $date . ' at ' . $time;
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Google\Cloud\Storage\StorageClient;
use League\Flysystem\Filesystem;
use Superbalist\Flysystem\GoogleStorage\GoogleStorageAdapter;

class StorageProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \Storage::extend('google', function ($app, $config) {
            $storageClient = new StorageClient([
                'projectId' => $config['projectId'],
                'keyFilePath' => $config['keyFilePath'],
            ]);
            $bucket = $storageClient->bucket($config['bucket-name']);
            
            $adapter = new GoogleStorageAdapter($storageClient,$bucket);
            
            return new Filesystem($adapter);
        });
    }
}

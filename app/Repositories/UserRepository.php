<?php
/**
 *
 * @author: Joan Eneki <jeneki@cytonn.com>
 *
 * Project: new-upesy-admin-web-app.
 *
 */

namespace App\Repositories;

use App\Models\Users\User;

class UserRepository
{
    public function getAllDistinctRolesArrayWithoutSuperAdmin()
    {
        return User::select('role')->distinct()
            ->where('role', '<>', config('constants.superAdmin'))
            ->pluck('role', 'role')
            ->toArray();
    }

    public function getUserById($id)
    {
        return User::where('id', $id)->with('partner')->first();
    }

    public function getSystemAdmins()
    {
        return User::where('role', 'Admin')->orWhere('role', 'Super Admin')->get();
    }

    public function getUsers()
    {
        return User::all();
    }
}

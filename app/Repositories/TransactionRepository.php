<?php

namespace App\Repositories;

use App\Models\Partners\Partner;
use DB;

class TransactionRepository
{
    private $transactions;
    private $partner_id,$partner_app_id,$invoiced;

    /** 
     * GEt transactions
     * 
     * @param int $partner_id
     * @param int $partner_app_id
     * @param bool $invoiced
     */
    public function __construct(
        int $partner_id = null,
        int $partner_app_id = null,
        bool $invoiced = false
    ) {
        $this->partner_id = $partner_id;
        $this->partner_app_id = $partner_app_id;
        $this->invoiced = $invoiced;
        
        $transactions = DB::table('transactions')
            ->join('subscriptions','subscriptions.id','transactions.subscription_id')
            ->join('customers','customers.id','subscriptions.customer_id')
            ->join('packagesPricing','packagesPricing.id','subscriptions.package_id')
            ->join('packages','packages.id','packagesPricing.package_id');
        
            if(!is_null($this->partner_id)){
                $transactions = $transactions->where('transactions.partner_id',$this->partner_id);
            }
    
            if(!is_null($this->partner_app_id)){
                $transactions = $transactions->where('transactions.partner_app_id',$this->partner_app_id);
            }
    
            if($this->invoiced){
                $transactions = $transactions->where('transactions.invoiced',1);
            }else{
                $transactions = $transactions->where('transactions.invoiced',0);
            }

            $this->transactions = $transactions;
    }

    /**
     * Get customer transactions
     * 
     * 
     * @return object $transactions
     */
    public function getCustomerTransactions()
    {
        return $this->transactions->select(
            'customers.firstName',
            'customers.lastName',
            'customers.phone',
            'customers.id_number',
            'customers.organisation',
            'transactions.invoice_number',
            'packagesPricing.duration_name',
            'transactions.amount',
            'transactions.discount',
            'transactions.amount_to_upesy',
            'transactions.amount_to_partner',
            'transactions.partner_app',
            'transactions.partner_name',
            'packages.name as service_name',
            'subscriptions.status',
            'subscriptions.created_at',
            'subscriptions.expires_at',
            'customers.id',
            'customers.image_url'
        );
    }

    /**
     * Get transactions grouped by partner_id
     *  filter status
     * 
     * @return object $transactions
     */
    public function fetchGroupedTransactions()
    {
        $transactions = $this->transactions;

        return $transactions->select(
            'transactions.partner_app_id',
            'transactions.partner_id',
            'transactions.partner_name as partner_name',
            'transactions.partner_app as partner_app',
            DB::raw('count(transactions.id) as total_transactions'),
            DB::raw('SUM(transactions.amount) as total_amount'),
            'transactions.invoice_to as invoice_to',
            'transactions.invoiced as invoiced',
            'transactions.created_at as created_at',
            'transactions.updated_at as updated_at'
        )
        ->groupBy(
            'transactions.partner_app_id',
            'transactions.partner_id',
            'transactions.invoiced',
        )
        ->orderBy('created_at', 'desc');
    }

}
<?php

namespace App\Repositories;

use App\Models\Incidents\Service;

class ServiceRepository
{
    public function getAllServicesArray()
    {
        return Service::all()->pluck('name', 'id')->toArray();
    }

    public function getAllServices()
    {
        return Service::all();
    }

    public function getIncidentTypes()
    {
        return Service::select('name', 'id')->get();
    }
}

<?php

namespace App\Repositories;

use App\Helpers\General\CollectionHelper;
use App\Models\Beneficiaries\Beneficiary;
use App\Models\Incidents\CustomerRegistrationCount;
use App\Models\Incidents\Report;
use App\Models\Subscriptions\Customer;
use App\Models\Partners\Partner;
use App\Models\Subscriptions\Friend;
use App\Models\Subscriptions\PaymentHistory;
use App\Models\Subscriptions\Subscription;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Cache;

class CustomerRepository
{
    // Free customers
    public function getCustomers($request, $partnerId)
    {
        if (!empty($request->filter_start_date) || !empty($request->filter_end_date) || !empty($request->filter_partner_application) || !empty($request->searchValue)) {
            $pageSize = 10;

            $results = $this->getFilteredCustomers($partnerId, Customer::query(), $request);

            $count = $results->count();

            return [
                'customers' => CollectionHelper::paginate($results, $pageSize),
                'count' => $count
            ];
        } else {
            return [
                'customers' => $this->getUnfilteredCustomers($partnerId, Customer::query())->paginate(10),
                'count' => $this->getUnfilteredCustomers($partnerId, Customer::query())->count()
            ];
        }
    }

    private function getFilteredCustomers($partnerId, $query, $request)
    {
        $q = $this->customersPartnerIsAuthorizedToView($partnerId, $query);

        if (isset($request->searchValue)) {
            $q->where(function ($q) use ($request) {
                $q->whereHas('partner', function ($q) use ($request) {
                    $q->where('name', 'LIKE', "%$request->searchValue%");
                })
                    ->orWhere('firstName', 'LIKE', "%{$request->searchValue}%")
                    ->orWhere('lastName', 'LIKE', "%{$request->searchValue}%")
                    ->orWhere('phone', 'LIKE', "%{$request->searchValue}%")
                    ->orWhere('email', 'LIKE', "%{$request->searchValue}%")
                    ->orWhere('organisation', 'LIKE', "%{$request->searchValue}%");
            })->get();
        }
        $collection = $q->get();

        if (!empty($request->filter_start_date)) {
            $startDate = Carbon::createFromFormat(
                'Y-m-d',
                $request->filter_start_date
            )->startOfDay()->toDateTimeString();

            if (!$request->filter_end_date) {
                $request->filter_end_date = $request->filter_start_date;
            }

            $endDate = Carbon::createFromFormat(
                'Y-m-d',
                $request->filter_end_date
            )->endOfDay()->toDateTimeString();

            $collection = $collection->filter(function ($item) use ($startDate, $endDate) {
                return data_get($item, 'created_at') >= $startDate && data_get($item, 'created_at') <= $endDate;
            });
        }

        if (!empty($request->filter_end_date) && empty($request->filter_start_date)) {
            $request->filter_start_date = $request->filter_end_date;

            $startDate = Carbon::createFromFormat(
                'Y-m-d',
                $request->filter_start_date
            )->startOfDay()->toDateTimeString();
            $endDate = Carbon::createFromFormat(
                'Y-m-d',
                $request->filter_end_date
            )->endOfDay()->toDateTimeString();

            $collection = $collection->filter(function ($item) use ($startDate, $endDate) {
                return data_get($item, 'created_at') >= $startDate && data_get($item, 'created_at') <= $endDate;
            });
        }

        if (!empty($request->filter_partner_application)) {
            $partnerApplicationId = $request->filter_partner_application;

            $collection = $collection->filter(function ($item) use ($partnerApplicationId) {
                return data_get($item, 'partner_id') == $partnerApplicationId;
            });
        }

        return $collection;
    }

    private function getUnfilteredCustomers($partnerId, $query)
    {
        return $this->customersPartnerIsAuthorizedToView($partnerId, $query);
    }

    private function customersPartnerIsAuthorizedToView($partnerId, $query)
    {
        $customersInSubscriptionTable = Subscription::groupBy('customer_id')->pluck('customer_id')->all();

        // Only the super admin and standard partners can log into the app
        if ($partnerId == config('constants.superAdminId')) {
            return $query->with('partner')
                ->where(function ($q) {
                    $q->whereHas('subscriptions', function ($q) {
                        $q->where('status', '!=', config('constants.subscriptionStatus.active'));
                    });
                })
                ->orWhere(function ($q) use ($customersInSubscriptionTable) {
                    $q->whereNotIn('id', $customersInSubscriptionTable);
                })
                ->with('subscriptions')
                ->orderBy('created_at', 'desc');
        } else {
            if (Partner::where('id', $partnerId)->first()->type == 'standard') {
                //If logged in user is standard, get the corresponding premium partner
                $alternativePartnerId = Partner::where('id', $partnerId)->first()->partner_id;
            } else {
                //If logged in user is premium, get the corresponding standard partner
                $alternativePartnerId = Partner::where('partner_id', $partnerId)->first() ? Partner::where('partner_id', $partnerId)->first()->id : null;
            }

            return $query->with('partner')
                ->where(function ($q) use ($partnerId, $alternativePartnerId) {
                    $q->where('partner_id', $partnerId)
                        ->orWhere('partner_id', $alternativePartnerId);
                })
                ->where(function ($q) use ($customersInSubscriptionTable) {
                    $q->where(function ($q) {
                        $q->whereHas('subscriptions', function ($q) {
                            $q->where('status', '!=', config('constants.subscriptionStatus.active'));
                        });
                    })
                        ->orWhere(function ($q) use ($customersInSubscriptionTable) {
                            $q->whereNotIn('id', $customersInSubscriptionTable);
                        });
                })
                ->with('subscriptions')
                ->orderBy('created_at', 'desc');
        }
    }

    public function getCustomerPartnerAppsForDropdown($partnerId)
    {
        if ($partnerId == config('constants.superAdminId')) {
            return Partner::all();
        } else {
            $premiumId = Partner::where('id', $partnerId)->first()->partner_id;

            return Partner::where('id', $partnerId)->orWhere('id', $premiumId)->get();
        }
    }

    public function getCustomerById($id)
    {
        return Customer::where('id', $id)
            ->with('residences')
            ->first();
    }

    public function getLastPaymentDetails($id)
    {
        return PaymentHistory::where('customer_id', $id)->orderBy('updated_at', 'desc')->first();
    }

    public function getSubscriptions($id)
    {
        return Subscription::where('customer_id', $id)
            ->with(['packagePricing', 'packagePricing.package'])
            ->with('partner')
            ->with('providing_partner')
            ->with('service')
            ->orderBy('created_at', 'desc')
            ->paginate(5);
    }

    public function searchSubscriptions($id, $input)
    {
        return Subscription::where('customer_id', $id)
            ->where(function ($q) use ($input) {
                $q->with('packagePricing')
                    ->whereHas('packagePricing', function ($q) use ($input) {
                        $q->where('duration_name', 'LIKE', "%{$input}%")
                            ->orWhere('amount', 'LIKE', "%{$input}%");
                    })
                    ->orWhere('status', 'LIKE', "%{$input}%")
                    ->with('partner')
                    ->orWhereHas('partner', function ($q) use ($input) {
                        $q->where('name', 'LIKE', "%{$input}%");
                    })
                    ->with('providing_partner')
                    ->orWhereHas('providing_partner', function ($q) use ($input) {
                        $q->where('partner_name', 'LIKE', "%{$input}%");
                    })
                    ->with('service')
                    ->orWhereHas('service', function ($q) use ($input) {
                        $q->where('name', 'LIKE', "%{$input}%");
                    });
            })
            ->paginate(5);
    }

    public function getCurrentModeOfPayment($id)
    {
        return Subscription::where('customer_id', $id)
            ->where('status', 'active')
            ->with('paymentMethod')
            ->orderBy('updated_at', 'desc')
            ->first();
    }

    public function getGuardians($id)
    {
        return Friend::where('customer_id', $id)->with('guardian')->paginate(5);
    }

    public function searchGuardians($id, $input)
    {
        return Friend::where('customer_id', $id)
            ->with('guardian')
            ->whereHas('guardian', function ($q) use ($input) {
                $q->where('firstName', 'LIKE', "%{$input}%")
                    ->orWhere('lastName', 'LIKE', "%{$input}%")
                    ->orWhere('phone', 'LIKE', "%{$input}%")
                    ->orWhere('email', 'LIKE', "%{$input}%");
            })
            ->paginate(5);
    }

    public function getDependants($id)
    {
        return Friend::where('friend_id', $id)->paginate(5);
    }

    public function searchDependants($id, $input)
    {
        return Friend::where('friend_id', $id)
            ->with('dependant')
            ->whereHas('dependant', function ($q) use ($input) {
                $q->where('firstName', 'LIKE', "%{$input}%")
                    ->orWhere('lastName', 'LIKE', "%{$input}%")
                    ->orWhere('phone', 'LIKE', "%{$input}%")
                    ->orWhere('email', 'LIKE', "%{$input}%");
            })
            ->paginate(5);
    }

    public function getCountOfDependants($id)
    {
        return Friend::where('friend_id', $id)->count();
    }

    public function getBeneficiaries($id)
    {
        //     return Beneficiary::where('customer_id', $id)->paginate(5);
    }

    // public function searchDependants($id, $input)
    // {
    //     return Beneficiary::where('customer_id', $id)
    //         ->where('first_name', 'LIKE', "%{$input}%")
    //         ->orWhere('last_name', 'LIKE', "%{$input}%")
    //         ->orWhere('phone', 'LIKE', "%{$input}%")
    //         ->paginate(5);
    // }

    public function getCountOfBeneficiaries($id)
    {
        //     return Beneficiary::where('customer_id', $id)->count();
    }

    public function getReportedIncidents($id)
    {
        return Report::where('reported_by', $id)->orderBy('created_at', 'desc')->paginate(5);
    }

    public function getFilteredReportedIncidents($id, $input)
    {
        $query = Report::where('reported_by', $id)->orderBy('created_at', 'desc');

        if (isset($input['alertHistorySearchValue'])) {
            $query->with('incident')
                ->whereHas('incident', function ($q) use ($input) {
                    $q->whereHas('service', function ($q) use ($input) {
                        $q->where('name', 'LIKE', "%{$input['alertHistorySearchValue']}%");
                    });
                })
                ->orWhere('status', 'LIKE', "%{$input['alertHistorySearchValue']}%");
        }

        if (isset($input['filter_service'])) {
            $query->whereHas('incident', function ($q) use ($input) {
                $q->where('service_id', $input['filter_service']);
            });
        }

        if (isset($input['filter_status'])) {
            $query->where('status', $input['filter_status']);
        }

        if (!empty($input['filter_start_date'])) {
            $startDate = Carbon::createFromFormat(
                'Y-m-d',
                $input['filter_start_date']
            )->startOfDay()->toDateTimeString();

            if (empty($input['filter_end_date'])) {
                $input['filter_end_date'] = $input['filter_start_date'];
            }

            $endDate = Carbon::createFromFormat(
                'Y-m-d',
                $input['filter_end_date']
            )->endOfDay()->toDateTimeString();

            $query->whereBetween('created_at', [$startDate, $endDate]);
        }

        if (!empty($input['filter_end_date']) && empty($input['filter_start_date'])) {
            $input['filter_start_date'] = $input['filter_end_date'];

            $startDate = Carbon::createFromFormat(
                'Y-m-d',
                $input['filter_start_date']
            )->startOfDay()->toDateTimeString();

            $endDate = Carbon::createFromFormat(
                'Y-m-d',
                $input['filter_end_date']
            )->endOfDay()->toDateTimeString();

            $query->whereBetween('created_at', [$startDate, $endDate]);
        }

        return $query->paginate(5);
    }

    public function getCountOfGuardians($id)
    {
        return Friend::where('customer_id', $id)->count();
    }

    public function getCustomerGuardianById($guardianId)
    {
        return Friend::where('friend_id', $guardianId)->delete();
    }

    public function customerCount($loggedInUserPartnerId)
    {
        $premiumPartnersCustomerCountArray = array();
        $standardPartnersCustomerCountArray = array();
        $registeredCustomersCountArray = array();

        $months = array_reverse(CustomerRegistrationCount::select('month_name')->distinct()->get()->pluck('month_name')->toArray());

        if ($loggedInUserPartnerId == config('constants.superAdminId')) {
            if (Cache::get('premium_partners_customers_monthly_data')) {
                foreach (Cache::get('premium_partners_customers_monthly_data') as $key => $value) {
                    $premiumPartnersCustomerCountArray[] = $value;
                }
            }

            if (Cache::get('standard_partners_customers_monthly_data')) {
                foreach (Cache::get('standard_partners_customers_monthly_data') as $key => $value) {
                    $standardPartnersCustomerCountArray[] = $value;
                }
            }
        } else {
            if (Cache::get('partner_' . $loggedInUserPartnerId . '_customers_monthly_data')) {
                foreach (Cache::get('partner_' . $loggedInUserPartnerId . '_customers_monthly_data') as $key => $value) {
                    $registeredCustomersCountArray[] = $value;
                }
            }
        }

        return [
            'months' => $months,
            'premiumPartnerCustomers' => $premiumPartnersCustomerCountArray,
            'standardPartnerCustomers' => $standardPartnersCustomerCountArray,
            'registeredCustomers' => $registeredCustomersCountArray
        ];
    }

    public function updateDailyCustomerTallyOnDb()
    {
        $partners = Partner::select('id', 'partner_id')->get();
        $premiumPartners = Partner::where('partner_id', null)->get()->pluck('id');
        $standardPartners = Partner::where('partner_id', '<>', null)->get()->pluck('id');

        $now = Carbon::now();

        CustomerRegistrationCount::where('month_name', $now->copy()->format('M') . ' ' . $now->copy()->format('Y'))->delete();

        foreach ($partners as $partner) {
            $partnerId = $partner->id;

            $startDate = $now->copy()->startOfMonth();
            $endDate = $now->copy();
            $count = Customer::where('created_at', '>=', $startDate)
                ->where('created_at', '<=', $endDate)
                ->where('partner_id', $partnerId)
                ->count();
            $customerRegistrationCountInput['month_name'] = $now->copy()->format('M') . ' ' . $now->copy()->format('Y');
            $customerRegistrationCountInput['number_of_customers'] = $count;
            $customerRegistrationCountInput['partner_id'] = $partner->id;
            CustomerRegistrationCount::create($customerRegistrationCountInput);
        }

        $premiumPartnersCustomerCountArray = array_sum(CustomerRegistrationCount::select('number_of_customers')
            ->whereIn('partner_id', $premiumPartners)
            ->where('month_name', $now->copy()->format('M'))
            ->get()
            ->pluck('number_of_customers')
            ->toArray());

        $key = Cache::get('premium_partners_customers_monthly_data');
        $key[$now->copy()->format('M') . ' ' . $now->copy()->format('Y')] = $premiumPartnersCustomerCountArray;
        Cache::put('premium_partners_customers_monthly_data', $key);

        $standardPartnersCustomerCountArray = array_sum(CustomerRegistrationCount::select('number_of_customers')
            ->whereIn('partner_id', $standardPartners)
            ->where('month_name', $now->copy()->format('M'))
            ->get()
            ->pluck('number_of_customers')
            ->toArray());

        $key = Cache::get('standard_partners_customers_monthly_data');
        $key[$now->copy()->format('M') . ' ' . $now->copy()->format('Y')] = $standardPartnersCustomerCountArray;
        Cache::put('standard_partners_customers_monthly_data', $key);

        // foreach ($partners as $partner) {
        //     $registeredCustomersCountArray = array_sum(CustomerRegistrationCount::select('number_of_customers')
        //         ->where('partner_id', $partner->id)
        //         ->orWhere('partner_id', $partner->partner_id)
        //         ->where('month_name', $now->copy()->format('M'))
        //         ->get()
        //         ->pluck('number_of_customers')
        //         ->toArray());

        //     $key = Cache::get('partner_' . $partner->id . '_customers_monthly_data');
        //     $key[$now->copy()->format('M') . ' ' . $now->copy()->format('Y')] = $registeredCustomersCountArray;
        //     Cache::put('partner_' . $partner->id . '_customers_monthly_data', $key);
        // }
    }

    public function oneTimeCustomerTallyToCache()
    {
        $months = CustomerRegistrationCount::select('month_name')->distinct()->get();

        $partners = Partner::select('id', 'partner_id')->get();

        $premiumPartnersCustomerCountArray = array();
        $premiumPartners = Partner::where('partner_id', null)->get()->pluck('id');

        $standardPartnersCustomerCountArray = array();
        $standardPartners = Partner::where('partner_id', '<>', null)->get()->pluck('id');

        $registeredCustomersCountArray = array();

        foreach ($months as $month) {
            $premiumPartnersCustomerCountArray[$month->month_name] = array_sum(CustomerRegistrationCount::select('number_of_customers')
                ->whereIn('partner_id', $premiumPartners)
                ->where('month_name', $month->month_name)
                ->get()
                ->pluck('number_of_customers')
                ->toArray());
        }
        Cache::put('premium_partners_customers_monthly_data', array_reverse($premiumPartnersCustomerCountArray));

        foreach ($months as $month) {
            $standardPartnersCustomerCountArray[$month->month_name] = array_sum(CustomerRegistrationCount::select('number_of_customers')
                ->whereIn('partner_id', $standardPartners)
                ->where('month_name', $month->month_name)
                ->get()
                ->pluck('number_of_customers')
                ->toArray());
        }
        Cache::put('standard_partners_customers_monthly_data', array_reverse($standardPartnersCustomerCountArray));

        foreach ($partners as $partner) {
            foreach ($months as $month) {
                $registeredCustomersCountArray[$month->month_name] = array_sum(CustomerRegistrationCount::select('number_of_customers')
                    ->where('partner_id', $partner->id)
                    ->where('month_name', $month->month_name)
                    ->get()
                    ->pluck('number_of_customers')
                    ->toArray());
            }
            Cache::put('partner_' . $partner->id . '_customers_monthly_data', array_reverse($registeredCustomersCountArray));
        }
    }

    public function oneTimeCustomerTallyToDb()
    {
        $partners = Partner::select('id')->get();

        $firstSignUp = Customer::orderBy('created_at', 'asc')->first()->created_at;
        $firstMonthWithSignUp = $firstSignUp->copy()->startOfMonth();
        $endOfFirstYearWithSignUps = $firstSignUp->copy()->endOfYear();

        $mostRecentSignUp = Customer::orderBy('created_at', 'desc')->first()->created_at;
        $startOfYearWithMostRecentSignUp = $mostRecentSignUp->copy()->startOfYear();

        $rangeOfYearsWithSignUps = $rangeOfMonthsInTheFirstYearWithSignUps = $rangeOfMonthsInTheLastYearWithSignUps = $rangeOfMonthsInAYear = array();

        $rangeOfYearsWithSignUpsCarbonPeriod = CarbonPeriod::create($firstMonthWithSignUp->copy()->startOfYear(), '1 year', $mostRecentSignUp->copy()->endOfYear());
        $rangeOfMonthsInTheFirstYearWithSignUpsCarbonPeriod = CarbonPeriod::create($firstMonthWithSignUp, '1 month', $endOfFirstYearWithSignUps);
        $rangeOfMonthsInTheLastYearWithSignUpsCarbonPeriod = CarbonPeriod::create($startOfYearWithMostRecentSignUp, '1 month', $mostRecentSignUp->copy()->endOfMonth());
        $rangeOfMonthsInAYearCarbonPeriod = CarbonPeriod::create(Carbon::parse('2014-01-01'), '1 month', Carbon::parse('2014-12-31'));


        foreach ($rangeOfYearsWithSignUpsCarbonPeriod as $date) {
            $rangeOfYearsWithSignUps[] = $date->format('Y');
        }

        foreach ($rangeOfMonthsInTheFirstYearWithSignUpsCarbonPeriod as $date) {
            $rangeOfMonthsInTheFirstYearWithSignUps[] = $date->format('M');
        }

        foreach ($rangeOfMonthsInTheLastYearWithSignUpsCarbonPeriod as $date) {
            $rangeOfMonthsInTheLastYearWithSignUps[] = $date->format('M');
        }

        foreach ($rangeOfMonthsInAYearCarbonPeriod as $date) {
            $rangeOfMonthsInAYear[] = $date->format('M');
        }

        foreach ($rangeOfYearsWithSignUps as $year) {
            if ($year == $rangeOfYearsWithSignUps[0]) {
                foreach ($rangeOfMonthsInTheFirstYearWithSignUps as $month) {
                    foreach ($partners as $partner) {
                        $this->populateCustomerRegistrationModel($month, $year, $partner);
                    }
                }
            } elseif ($year == $rangeOfYearsWithSignUps[count($rangeOfYearsWithSignUps) - 1]) {
                foreach ($rangeOfMonthsInTheLastYearWithSignUps as $month) {
                    foreach ($partners as $partner) {
                        $this->populateCustomerRegistrationModel($month, $year, $partner);
                    }
                }
            } else {
                foreach ($rangeOfMonthsInAYear as $month) {
                    foreach ($partners as $partner) {
                        $this->populateCustomerRegistrationModel($month, $year, $partner);
                    }
                }
            }
        }
    }

    private function populateCustomerRegistrationModel($month, $year, $partner): void
    {
        $partnerId = $partner->id;
        // Carbon will always assume it is the current day when an alternative is not supplied as attribute
        // If we are checking for Feb on 31/07, it will skip to March because Feb doesn't have a day 31
        // So always supply a day that is common in all months (1st - 28th)
        $startDate = Carbon::createFromFormat('d M Y', '01' . $month . ' ' . $year)->startOfMonth();
        $endDate = Carbon::createFromFormat('d M Y', '01' . $month . ' ' . $year)->endOfMonth();
        $count = Customer::where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->where('partner_id', $partnerId)
            ->count();
        $customerRegistrationCountInput['month_name'] = $month . ' ' . $year;
        $customerRegistrationCountInput['number_of_customers'] = $count;
        $customerRegistrationCountInput['partner_id'] = $partner->id;
        CustomerRegistrationCount::create($customerRegistrationCountInput);
    }
}

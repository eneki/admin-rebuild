<?php

namespace App\Repositories;

use App\Models\Incidents\Service;
use App\Models\Subscriptions\Packages\PackagePricing;

class PackagePricingRepository
{
    public function getUniquePackagePricingDurationNames()
    {
        return PackagePricing::select('duration_name')->groupBy('duration_name')->get();
    }
}

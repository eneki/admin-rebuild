<?php

namespace App\Repositories;

use App\Models\Partners\Partner;

class PartnerRepository
{
    public function getAllPartners()
    {
        return Partner::orderBy('updated_at', 'desc')->paginate(10);
    }

    public function getCountOfAllPartners()
    {
        return Partner::all()->count();
    }

    public function getPartnerById($id)
    {
        return Partner::with('users')->findOrFail($id);
    }

    public function getPartnerPackages($id)
    {
        return Partner::with(['packages', 'packages.service'])
            ->findOrFail($id)
            ->packages()
            ->paginate(10);
    }

    public function takeFivePartnerPackages($id)
    {
        return $this->getPartnerPackages($id)->take(5);
    }

    public function getCountOfPartnerPackages($id)
    {
        return $this->getPartnerPackages($id)->count();
    }

    public function getAllPremiumPartners()
    {
        return Partner::where('partner_id', null)->get();
    }

    public function getPaginatedPartnerUsers($id)
    {
        return $this->getPartnerById($id)->users()->paginate(10);
    }

    public function takeFivePartnerUsers($id)
    {
        return $this->getPartnerById($id)->users->take(5);
    }

    public function getCountOfPartnerUsers($id)
    {
        return $this->getPartnerById($id)->users->count();
    }

    public function getAllPremiumPartnersArray()
    {
        return $this->getAllPremiumPartners()->pluck('name', 'id')->toArray();
    }

    public function getActivePartners()
    {
        return Partner::where('status', 1)->get();
    }

    public function getAllPartnersWithoutPaginate()
    {
        return Partner::all();
    }
}

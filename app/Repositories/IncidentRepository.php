<?php

namespace App\Repositories;

use App\Models\Incidents\Incident;
use App\Models\Incidents\Report;
use App\Models\Incidents\ReportLocation;
use App\Models\Partners\Partner;
use App\Models\Subscriptions\Subscription;
use App\Models\Users\User;
use App\Notifications\PendingIncident as IncidentNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Report\Repositories\ReportRepository;

class IncidentRepository
{
    protected $reportRepository;

    public function __construct()
    {
        $this->reportRepository = new ReportRepository();
    }

    public function getIncidents($request, $status, $partnerId)
    {
        if (!empty($request->filter_start_date || $request->filter_end_date || $request->filter_incident_type || !empty($request->searchValue))) {
            return [
                'reports' => $this->getFilteredIncidents($request, $partnerId, $status, Report::query())->paginate(10),
                'count' => $this->getFilteredIncidents($request, $partnerId, $status, Report::query())->count()
            ];
        } else {
            return [
                'reports' => $this->getUnfilteredIncidents($partnerId, $status, Report::query())->paginate(10),
                'count' => $this->getUnfilteredIncidents($partnerId, $status, Report::query())->count()
            ];
        }
    }

    private function getFilteredIncidents($request, $partnerId, $status, $query)
    {
        $query = $this->incidentsPartnerIsAuthorizedToView($partnerId, $status, $query);

        if (isset($request->searchValue)) {
            $query->where(function ($q) use ($request) {
                $q->whereHas('incident', function ($q) use ($request) {
                    $q->whereHas('service', function ($q) use ($request) {
                        $q->where('name', 'LIKE', "%$request->searchValue%");
                    });
                })
                    ->orWhereHas('customer', function ($q) use ($request) {
                        $q->where('firstName', 'LIKE', "%{$request->searchValue}%")
                            ->orWhere('lastName', 'LIKE', "%{$request->searchValue}%")
                            ->orWhere('phone', 'LIKE', "%{$request->searchValue}%");
                    });
            });
        }

        if (!empty($request->filter_start_date)) {
            $startDate = Carbon::createFromFormat(
                'Y-m-d',
                $request->filter_start_date
            )->startOfDay()->toDateTimeString();

            if (!$request->filter_end_date) {
                $request->filter_end_date = $request->filter_start_date;
            }

            $endDate = Carbon::createFromFormat(
                'Y-m-d',
                $request->filter_end_date
            )->endOfDay()->toDateTimeString();

            $query->whereBetween('created_at', [$startDate, $endDate]);
        }

        if (!empty($request->filter_end_date) && empty($request->filter_start_date)) {
            $request->filter_start_date = $request->filter_end_date;

            $startDate = Carbon::createFromFormat(
                'Y-m-d',
                $request->filter_start_date
            )->startOfDay()->toDateTimeString();

            $endDate = Carbon::createFromFormat(
                'Y-m-d',
                $request->filter_end_date
            )->endOfDay()->toDateTimeString();

            $query->whereBetween('created_at', [$startDate, $endDate]);
        }

        if (!empty($request->filter_incident_type)) {
            $incidentTypeId = $request->filter_incident_type;

            $query->whereHas('incident', function ($q) use ($incidentTypeId) {
                $q->where('id', $incidentTypeId);
            });
        }

        return $query;
    }

    private function getUnfilteredIncidents($partnerId, $status, $query)
    {
        $this->incidentsPartnerIsAuthorizedToView($partnerId, $status, $query);

        return $query;
    }

    private function incidentsPartnerIsAuthorizedToView($partnerId, $status, $query)
    {
        // Admin can view all that match the status passed in the parameters
        if ($partnerId == config('constants.superAdminId')) {
            return $query->has('customer')
                ->with('incident')
                ->with('customer')
                ->where('is_demo', '<>', 1)
                ->where('incident_id', '<>', 1)
                ->where('incident_id', '<>', 2)
                ->where('status', 'LIKE', '%' . $status . '%')
                ->orderBy('created_at', 'desc');
        } else {
            if (Partner::where('id', $partnerId)->first()->type == 'standard') {
                //If logged in user is standard, get the corresponding premium partner
                $alternativePartnerId = Partner::where('id', $partnerId)->first()->partner_id;
            } else {
                //If logged in user is premium, get the corresponding standard partner
                $alternativePartnerId = Partner::where('partner_id', $partnerId)->first() ? Partner::where('partner_id', $partnerId)->first()->id : null;
            }

            //if g4s ---> get all free + theirs
            // else just theirs

            if ($partnerId == 8) {
                $customersInSubscriptionTable = Subscription::groupBy('customer_id')->pluck('customer_id')->all();

                return $query->has('customer')
                    ->with('incident')
                    ->where('incident_id', '<>', 1)
                    ->where('incident_id', '<>', 2)
                    ->where('is_demo', '<>', 1)
                    ->where(function ($q) use ($customersInSubscriptionTable, $partnerId, $alternativePartnerId) {
                        $q->whereHas('customer', function ($q) use ($customersInSubscriptionTable, $partnerId, $alternativePartnerId) {
                            // inactive or no subscriptions (free customers)
                            $q->where(function ($q) use ($customersInSubscriptionTable) {
                                $q->where(function ($q) {
                                    $q->whereHas('subscriptions', function ($q) {
                                        $q->where('status', '!=', config('constants.subscriptionStatus.active'));
                                    });
                                })
                                    ->orWhere(function ($q) use ($customersInSubscriptionTable) {
                                        $q->whereNotIn('id', $customersInSubscriptionTable);
                                    });
                            })
                                // g4s customers
                                ->orWhere(function ($q) use ($partnerId, $alternativePartnerId) {
                                    $q->whereHas('subscriptions', function ($q) use ($partnerId, $alternativePartnerId) {
                                        $q->where('status', config('constants.subscriptionStatus.active'))
                                            ->whereHas('providing_partner', function ($q) use ($partnerId, $alternativePartnerId) {
                                                $q->where('partner_id', $alternativePartnerId)
                                                    ->orWhere('partner_id', $partnerId);
                                            });
                                    });
                                });
                        });
                    })
                    ->with('customer')
                    ->where('status', 'LIKE', '%' . $status . '%')
                    ->orderBy('created_at', 'desc');
            } else {
                return $query->has('customer')
                    ->where('status', 'LIKE', '%' . $status . '%')
                    ->where('is_demo', '<>', 1)
                    ->with('incident')
                    ->where('incident_id', '<>', 1)
                    ->where('incident_id', '<>', 2)
                    ->whereHas('customer', function ($q) use ($partnerId, $alternativePartnerId) {
                        $q->whereHas('subscriptions', function ($q) use ($partnerId, $alternativePartnerId) {
                            $q->where('status', config('constants.subscriptionStatus.active'))
                                ->whereHas('providing_partner', function ($q) use ($partnerId, $alternativePartnerId) {
                                    $q->where('partner_id', $partnerId)
                                        ->orWhere('partner_id', $alternativePartnerId);
                                });
                        });
                    })
                    ->with('customer')
                    ->orderBy('created_at', 'desc');
            }
        }
    }

    public function show($id)
    {
        return Report::with('customer')
            ->with('reportMedia')
            ->where('id', $id)
            ->first();
    }

    public function refreshPendingIncidents($id)
    {
        if($id){
            $incident = $this->incidentsPartnerIsAuthorizedToView(Auth::user()->partner_id, 'pending', Report::query())
                ->where('id', '>', $id)
                ->first();
        } else {
            $incident = $this->incidentsPartnerIsAuthorizedToView(Auth::user()->partner_id, 'pending', Report::query())
                ->orderBy('created_at', 'asc')
                ->first();
        }
        if (!empty($incident)) {
            return response()->json([
                'incident' => $incident->toArray(),
                'location' => !empty($incident->reportLocations()->latest()->first()) ? $incident->reportLocations()->latest()->first()->toArray() : null
            ]);
        } else {
            return response()->json('false');
        }
    }

    public function getIncidentTypes()
    {
        return Incident::select('name', 'id')->get();
    }
}

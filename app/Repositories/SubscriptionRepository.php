<?php

namespace App\Repositories;

use App\Helpers\General\CollectionHelper;
use App\Models\Partners\Partner;
use App\Models\Subscriptions\Customer;
use App\Models\Subscriptions\Subscription;
use App\Models\Subscriptions\SubscriptionProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SubscriptionRepository
{
    public function getPartnerSubscriptions($request, $partnerId)
    {
        if (!empty($request->filter_start_date) || !empty($request->filter_end_date) || !empty($request->filter_partner_application ||
            !empty($request->filter_partner) || !empty($request->filter_service || !empty($request->filter_package)
                || !empty($request->filter_status) || !empty($request->searchValue)))) {
            $pageSize = 10;

            $results = $this->getFilteredSubscriptions($partnerId, Subscription::query(), $request);

            $count = $results->count();
            return [
                'subscriptions' => CollectionHelper::paginate($results, $pageSize),
                'count' => $count
            ];
        } else {
            return [
                'subscriptions' => $this->getUnfilteredSubscriptions($partnerId, Subscription::query())->paginate(10),
                'count' => $this->getUnfilteredSubscriptions($partnerId, Subscription::query())->count()
            ];
        }
    }

    public function getUnpaginatedPartnerSubscriptions($request, $partnerId)
    {
        if (!empty($request->filter_start_date) || !empty($request->filter_end_date) || !empty($request->filter_partner_application ||
            !empty($request->filter_partner) || !empty($request->filter_service || !empty($request->filter_package)
                || !empty($request->filter_status)))) {
            return $this->getFilteredSubscriptions($partnerId, Subscription::query(), $request);
        } else {
            return $this->getUnfilteredSubscriptions($partnerId, Subscription::query())->get();
        }
    }

    private function getFilteredSubscriptions($partnerId, $query, $request)
    {
        $q = $this->getSubscriptionsPartnerIsAuthorizedToView($partnerId, $query);

        if (isset($request->searchValue)) {
            $q->where(function ($q) use ($request) {
                $q->whereHas('customer', function ($q) use ($request) {
                    $q->where('firstName', 'LIKE', "%{$request->searchValue}%")
                        ->orWhere('lastName', 'LIKE', "%{$request->searchValue}%")
                        ->orWhere('id_number', 'LIKE', "%{$request->searchValue}%")
                        ->orWhere('phone', 'LIKE', "%{$request->searchValue}%")
                        ->orWhere('organisation', 'LIKE', "%{$request->searchValue}%");
                })
                    ->orWhereHas('packagePricing', function ($q) use ($request) {
                        $q->where('duration_name', 'LIKE', "%{$request->searchValue}%");
                    })
                    ->orWhereHas('partner', function ($q) use ($request) {
                        $q->where('name', 'LIKE', "%{$request->searchValue}%");
                    })
                    ->orWhereHas('providing_partner', function ($q) use ($request) {
                        $q->whereHas('partner', function ($q) use ($request) {
                            $q->where('name', 'LIKE', "%{$request->searchValue}%");
                        });
                    })
                    ->orWhereHas('service', function ($q) use ($request) {
                        $q->where('name', 'LIKE', "%{$request->searchValue}%");
                    })
                    ->orWhere('invoice_number', 'LIKE', "%{$request->searchValue}%")
                    ->orWhere('status', 'LIKE', "%{$request->searchValue}%")
                    ->orWhere('amount', 'LIKE', "%{$request->searchValue}%");
            })->get();
        }

        $collection = $q->get();

        if (!empty($request->filter_start_date)) {
            $startDate = Carbon::createFromFormat(
                'Y-m-d',
                $request->filter_start_date
            )->startOfDay()->toDateTimeString();

            if (!$request->filter_end_date) {
                $request->filter_end_date = $request->filter_start_date;
            }

            $endDate = Carbon::createFromFormat('Y-m-d', $request->filter_end_date)->endOfDay()->toDateTimeString();

            $collection = $collection->filter(function ($item) use ($startDate, $endDate) {
                return data_get($item, 'created_at') >= $startDate && data_get($item, 'created_at') <= $endDate;
            });
        }

        if (!empty($request->filter_end_date)) {
            if (($request->filter_end_date) && empty($request->filter_start_date)) {
                $request->filter_start_date = $request->filter_end_date;

                $startDate = Carbon::createFromFormat(
                    'Y-m-d',
                    $request->filter_start_date
                )->startOfDay()->toDateTimeString();

                $endDate = Carbon::createFromFormat('Y-m-d', $request->filter_end_date)->endOfDay()->toDateTimeString();

                $collection = $collection->filter(function ($item) use ($startDate, $endDate) {
                    return data_get($item, 'created_at') >= $startDate && data_get($item, 'created_at') <= $endDate;
                });
            }
        }

        if (!empty($request->filter_partner_application)) {
            $partnerId = $request->filter_partner_application;

            $collection = $collection->filter(function ($item) use ($partnerId) {
                return data_get($item, 'providing_partner.partner_id') == $partnerId;
            });
        }

        if (!empty($request->filter_partner)) {
            $partnerId = $request->filter_partner;

            $collection = $collection->filter(function ($item) use ($partnerId) {
                return data_get($item, 'partner_id') == $partnerId;
            });
        }

        if (!empty($request->filter_service)) {
            $serviceId = $request->filter_service;

            $collection = $collection->filter(function ($item) use ($serviceId) {
                return data_get($item, 'service_id') == $serviceId;
            });
        }

        if (!empty($request->filter_package)) {
            $packageName = $request->filter_package;

            $collection = $collection->filter(function ($item) use ($packageName) {
                return data_get($item, 'packagePricing.duration_name') == $packageName;
            });
        }

        if (!empty($request->filter_status)) {
            $statusName = $request->filter_status;

            $collection = $collection->filter(function ($item) use ($statusName) {
                return data_get($item, 'status') == $statusName;
            });
        }

        return $collection;
    }

    private function getUnfilteredSubscriptions($partnerId, $query)
    {
        return $this->getSubscriptionsPartnerIsAuthorizedToView($partnerId, $query);
    }

    private function getSubscriptionsPartnerIsAuthorizedToView($partnerId, $query)
    {
        if ($partnerId == config('constants.superAdminId')) {
            return $query->with('partner')
                ->with('customer')
                ->has('customer')
                ->with('service')
                ->with('providing_partner')
                ->with(['providing_partner', 'providing_partner.partner'])
                ->with('packagePricing')
                ->where('status', config('constants.subscriptionStatus.active'))
                ->orderBy('created_at', 'desc');
        } else {
            if (Partner::where('id', $partnerId)->first()->type == 'standard') {
                //If logged in user is standard, get the corresponding premium partner
                $alternativePartnerId = Partner::where('id', $partnerId)->first()->partner_id;
            } else {
                //If logged in user is premium, get the corresponding standard partner
                $alternativePartnerId = Partner::where('partner_id', $partnerId)->first() ? Partner::where('partner_id', $partnerId)->first()->id : null;
            }
            
            return $query->with('partner')
                ->with('customer')
                ->has('customer')
                ->with('service')
                ->with('providing_partner')
                ->with(['providing_partner', 'providing_partner.partner'])
                ->with('packagePricing')
                ->where('status', config('constants.subscriptionStatus.active'))
                ->whereHas('providing_partner', function ($q) use ($partnerId, $alternativePartnerId) {
                    $q->where('partner_id', $alternativePartnerId)
                        ->orWhere('partner_id', $partnerId);
                })
                ->orderBy('created_at', 'desc');
        }
    }

    public function getSubscriptionPartnersForDropdown($partnerId)
    {
        if ($partnerId == config('constants.superAdminId')) {
            return Partner::all();
        } else {
            $premiumId = Partner::where('id', $partnerId)->first()->partner_id;

            return Partner::where('id', $partnerId)->orWhere('id', $premiumId)->get();
        }
    }

    public function getSubscriptionById($id)
    {
        return Subscription::where('id', $id)
            ->with('customer')
            ->with('packagePricing')
            ->firstOrFail();
    }

    public function getSubscriptionProviderBySubscriptionId($subscriptionId)
    {
        return SubscriptionProvider::where('subscription_id', $subscriptionId)->first();
    }

    public function getSubscriptionCustomerBySubscriptionId($subscriptionId)
    {
        $customerId = Subscription::where('id', $subscriptionId)->first()->customer_id;
        return Customer::where('id', $customerId)->first();
    }

    public function getUniqueStatuses()
    {
        return Subscription::select('status')->groupBy('status')->get();
    }
}

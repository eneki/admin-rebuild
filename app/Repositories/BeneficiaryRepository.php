<?php

namespace App\Repositories;


use App\Models\Beneficiaries\CustomerBeneficiary;
use App\Models\Beneficiaries\PendingBeneficiary;

class BeneficiaryRepository
{
    public function getActiveBeneficiaries($subscriptionId)
    {
        return CustomerBeneficiary::where('subscription_id', $subscriptionId)
            ->with('customer')
            ->with('beneficiary')
            ->paginate(5);
    }

    public function getCountOfActiveBeneficiaries($subscriptionId)
    {
        return CustomerBeneficiary::where('subscription_id', $subscriptionId)->count();
    }

    public function getPendingBeneficiaries($subscriptionId)
    {
        return PendingBeneficiary::where('subscription_id', $subscriptionId)
            ->paginate(5);
    }

    public function getCountOfPendingBeneficiaries($subscriptionId)
    {
        return PendingBeneficiary::where('subscription_id', $subscriptionId)->count();
    }

}

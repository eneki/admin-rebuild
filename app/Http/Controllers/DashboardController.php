<?php

namespace App\Http\Controllers;

use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Report\Repositories\ReportRepository;

class DashboardController extends Controller
{
    public $customerRepository;
    public $reportRepository;

    public function __construct()
    {
        $this->customerRepository = new CustomerRepository();
        $this->reportRepository = new ReportRepository();
    }

    public function index()
    {
        $loggedInUserPartnerId = Auth::user()->partner_id;

        $customerCounts = $this->customerRepository->customerCount($loggedInUserPartnerId);
        $incidentCounts = $this->reportRepository->getIncidentTally();

        $incidentDistribution = Cache::get('incident_type_distribution');

        dd($customerCounts, $incidentCounts, $loggedInUserPartnerId);
        return view('dashboard', [
            'incidentDistribution' => $incidentDistribution,
            'loggedInUserPartnerId' => $loggedInUserPartnerId,
            'months' => $customerCounts['months'],
            'premiumPartnerCustomers' => $customerCounts['premiumPartnerCustomers'],
            'standardPartnerCustomers' => $customerCounts['standardPartnerCustomers'],
            'registeredCustomers' => $customerCounts['registeredCustomers'],
            'totalPendingIncidents' => $incidentCounts['totalPendingIncidents'],
            'totalDispatchedIncidents' => $incidentCounts['totalDispatchedIncidents'],
            'totalIncidents' => $incidentCounts['totalIncidents'],
            'totalResolvedIncidents' => $incidentCounts['totalResolvedIncidents']
        ]);
    }

    public function runScheduler() {
        Artisan::call('schedule:run');
    }
}

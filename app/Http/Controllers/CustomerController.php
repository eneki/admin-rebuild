<?php

namespace App\Http\Controllers;

use App\Models\Incidents\Report;
use App\Models\Subscriptions\Customer;
use App\Models\Subscriptions\Friend;
use App\Repositories\CustomerRepository;
use App\Repositories\PartnerRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\SubscriptionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\Psr7\str;

class CustomerController extends Controller
{
    protected $customerRepository;
    protected $subscriptionRepository;
    protected $partnerRepository;
    protected $serviceRepository;

    public function __construct()
    {
        $this->customerRepository = new CustomerRepository();
        $this->subscriptionRepository = new SubscriptionRepository();
        $this->partnerRepository = new PartnerRepository();
        $this->serviceRepository = new  ServiceRepository();
    }

    public function index(Request $request)
    {
        // If the filter is to work on search results, the original search parameter has to be passed into the request array.
        // Thus, the request array will return more than one parameter 
        // AND
        // one of the parameters will be searchValue
        // WHICH
        // will contain the entire search parameter (key + value)

        if ((count($request->all()) > 1) && (isset($request->searchValue))) {
            if (strpos($request->searchValue, '=')) {
                $string = $request->searchValue;
                $array = explode('=', $string);
                $request->searchValue = $array[1];
            }
        }

        $partnerId = Auth::user()->partner_id;
        $partnerApplications = $this->customerRepository->getCustomerPartnerAppsForDropdown($partnerId);

        $customers = $this->customerRepository->getCustomers($request, $partnerId)['customers'];
        $customerCount = $this->customerRepository->getCustomers($request, $partnerId)['count'];

        return view('customers.index', [
            'partnerApplications' => $partnerApplications,
            'customers' => $customers,
            'customerCount' => $customerCount
        ]);
    }

    public function show(Request $request, $id)
    {
        $customer = $this->customerRepository->getCustomerById($id);
        $currentModeOfPayment = $this->customerRepository->getCurrentModeOfPayment($id);
        $subscriptionProviders = $this->partnerRepository->getActivePartners();
        $services = $this->serviceRepository->getAllServices();
        $statuses = $this->subscriptionRepository->getUniqueStatuses();

        if ($request->get('subscriptionsSearchValue')) {
            $subscriptions = $this->customerRepository->searchSubscriptions($id, $request->get('subscriptionsSearchValue'));
        } else {
            $subscriptions = $this->customerRepository->getSubscriptions($id);
        }

        if ($request->get('guardianSearchValue')) {
            $guardians = $this->customerRepository->searchGuardians($id, $request->get('guardianSearchValue'));
        } else {
            $guardians = $this->customerRepository->getGuardians($id);
        }

        if ($request->get('alertHistorySearchValue') || $request->get('filter_service') || $request->get('filter_status') || $request->get('filter_start_date') || $request->get('filter_end_date')) {
            $reportedIncidents = $this->customerRepository->getFilteredReportedIncidents($id, $request->only(['alertHistorySearchValue', 'filter_service', 'filter_status', 'filter_start_date', 'filter_end_date']));
        } else {
            $reportedIncidents = $this->customerRepository->getReportedIncidents($id);
        }

        if ($request->get('dependantsSearchValue')) {
            $dependants = $this->customerRepository->searchDependants($id, $request->get('searchValue'));
        } else {
            $dependants = $this->customerRepository->getDependants($id);
        }

        if ($request->get('beneficiariesSearchValue')) {
            $beneficiaries = $this->customerRepository->searchDependants($id, $request->get('searchValue'));
        } else {
            $beneficiaries = $this->customerRepository->getBeneficiaries($id);
        }

        return view('customers.details', [
            'customer' => $customer,
            'subscriptionProviders' => $subscriptionProviders,
            'currentModeOfPayment' => $currentModeOfPayment,
            'statuses' => $statuses,
            'services' => $services,
            'subscriptions' => $subscriptions,
            'guardians' => $guardians,
            'dependants' => $dependants,
            'beneficiaries' => $beneficiaries,
            'reportedIncidents' => $reportedIncidents,
        ]);
    }

    public function customerGuardianDelete($guardianId)
    {
        Friend::where('friend_id', $guardianId)->delete();

        return back();
    }

    public function commonCustomerQueries($id)
    {
        $customer = $this->customerRepository->getCustomerById($id);
        $currentModeOfPayment = $this->customerRepository->getCurrentModeOfPayment($id);
        $subscriptionProviders = $this->partnerRepository->getActivePartners();
        $services = $this->serviceRepository->getAllServices();
        $statuses = $this->subscriptionRepository->getUniqueStatuses();

        return [
            'customer' => $customer,
            'currentModeOfPayment' => $currentModeOfPayment,
            'subscriptionProviders' => $subscriptionProviders,
            'services' => $services,
            'statuses' => $statuses
        ];
    }
}

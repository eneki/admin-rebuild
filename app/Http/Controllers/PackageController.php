<?php

namespace App\Http\Controllers;

use App\Repositories\PartnerRepository;

class PackageController extends Controller
{
    public $partnerRepository;

    public function __construct()
    {
        $this->partnerRepository = new  PartnerRepository();
    }

    public function partnerPackages($id)
    {
        $packages = $this->partnerRepository->getPartnerPackages($id);
        $count = $this->partnerRepository->getCountOfPartnerPackages($id);
        $partner = $this->partnerRepository->getPartnerById($id);

        return view('partners.packages', [
            'packages' => $packages,
            'count' => $count,
            'partner' => $partner
        ]);
    }
}

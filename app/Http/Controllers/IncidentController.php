<?php

namespace App\Http\Controllers;

use App\Http\Requests\ActUponIncidentRequest;
use App\Http\Requests\DispatchIncidentRequest;
use App\Repositories\IncidentRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\CustomerRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Report\Repositories\ReportRepository;

class IncidentController extends Controller
{
    protected $customerRepository;
    protected $incidentRepository;
    protected $serviceRepository;
    protected $reportRepository;

    public function __construct()
    {
        $this->incidentRepository = new IncidentRepository();
        $this->customerRepository = new CustomerRepository();
        $this->serviceRepository = new ServiceRepository();
        $this->reportRepository = new ReportRepository();
    }

    public function pendingIncidents(Request $request)
    {
        $status = config('constants.reportStatus.pending');

        $data = $this->getDataForView($request, $status);

        return view('incidents.pending-incidents', [
            'incidentTypes' => $data['incidentTypes'],
            'reports' => $data['reports'],
            'reportCount' => $data['reportCount']
        ]);
    }

    public function dispatchedIncidents(Request $request)
    {
        $status = config('constants.reportStatus.dispatched');

        $data = $this->getDataForView($request, $status);

        return view('incidents.dispatched-incidents', [
            'incidentTypes' => $data['incidentTypes'],
            'reports' => $data['reports'],
            'reportCount' => $data['reportCount']
        ]);
    }

    public function resolvedIncidents(Request $request)
    {
        $status = config('constants.reportStatus.resolved');

        $data = $this->getDataForView($request, $status);

        return view('incidents.resolved-incidents', [
            'incidentTypes' => $data['incidentTypes'],
            'reports' => $data['reports'],
            'reportCount' => $data['reportCount']
        ]);
    }

    private function getDataForView($request, $status){
        
        // Check index method of the CustomerController to understand the logic here
        if ((count($request->all()) > 1) && (isset($request->searchValue))) {
            if (strpos($request->searchValue, '=')) {
                $string = $request->searchValue;
                $array = explode('=', $string);
                $request->searchValue = $array[1];
            }
        }

        $partnerId = Auth::user()->partner_id;
        $incidentTypes = $this->incidentRepository->getIncidentTypes();
        $reports = $this->incidentRepository->getIncidents($request, $status, $partnerId)['reports'];
        $reportCount = $this->incidentRepository->getIncidents($request, $status, $partnerId)['count'];

        return [
            'reports' => $reports,
            'incidentTypes' => $incidentTypes,
            'reportCount' => $reportCount
        ];
    }

    public function showPendingIncident($id, $customerId)
    {
        $incidentReport = $this->incidentRepository->show($id);
        $customer = $this->customerRepository->getCustomerById($customerId);
        $subscriptions = $this->customerRepository->getSubscriptions($customerId);
        $guardians = $this->customerRepository->getGuardians($customerId);
        $count = $this->customerRepository->getCountOfGuardians($customerId);
        $currentModeOfPayment = $this->customerRepository->getCurrentModeOfPayment($customerId);

        return view('incidents.pending-incidents-report', [
            'incidentReport' => $incidentReport,
            'customer' => $customer,
            'subscriptions' => $subscriptions,
            'guardians' => $guardians,
            'currentModeOfPayment' => $currentModeOfPayment,
            'count' => $count
        ]);
    }

    public function showDispatchedIncident($id, $customerId)
    {
        $incidentReport = $this->incidentRepository->show($id);
        $reportStatusArray = config('constants.reportStatusArray');
        $customer = $this->customerRepository->getCustomerById($customerId);
        $subscriptions = $this->customerRepository->getSubscriptions($customerId);
        $guardians = $this->customerRepository->getGuardians($customerId);
        $count = $this->customerRepository->getCountOfGuardians($customerId);
        $currentModeOfPayment = $this->customerRepository->getCurrentModeOfPayment($customerId);

        return view('incidents.dispatched-and-resolved-incidents-report', [
            'incidentReport' => $incidentReport,
            'reportStatusArray' => $reportStatusArray,
            'customer' => $customer,
            'subscriptions' => $subscriptions,
            'guardians' => $guardians,
            'currentModeOfPayment' => $currentModeOfPayment,
            'count' => $count
        ]);
    }

    public function showDispatchIncidentForm($id)
    {
        $pendingIncidentReport = $this->incidentRepository->show($id);

        return view('incidents.dispatch-incident', [
            'incidentReport' => $pendingIncidentReport
        ]);
    }

    public function dispatchIncident(DispatchIncidentRequest $request, $reportId, $customerId)
    {
        $pendingIncidentReport = $this->incidentRepository->show($reportId);

        $request['dispatched_at'] = Carbon::now();
        $request['officer_id'] = auth()->id();
        $pendingIncidentReport->update($request->all());

        return redirect()->route('pending-incident-report', [
            'report' => $reportId, 
            'customer' => $customerId
        ]);
    }

    public function showCreateIncidentReportForm($id){
        $incidentReport = $this->incidentRepository->show($id);
        $reportStatusArray = config('constants.reportStatusArray');

        return view('incidents.create-incident-report', [
            'reportStatusArray' => $reportStatusArray,
            'incidentReport' => $incidentReport
        ]);
    }

    public function actUponIncident(ActUponIncidentRequest $request, $reportId, $customerId)
    {
        $pendingIncidentReport = $this->incidentRepository->show($reportId);

        $request['acted_upon_at'] = Carbon::now();
        $pendingIncidentReport->update($request->all());

        return redirect()->route('dispatched-incident-report', [
            'report' => $reportId,
            'customer' => $customerId
        ]);
    }

    public function refreshCurrentLocation($id)
    {
        $pendingIncidentReport = $this->incidentRepository->show($id)->reportLocations()->orderBy('id', 'desc')->first();

        return response()->json(['data' => $pendingIncidentReport]);
    }

    public function refreshPendingIncidents($id)
    {
        return $this->incidentRepository->refreshPendingIncidents($id);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\BankDetailsRequest;
use App\Http\Requests\PartnerRequest;
use App\Models\Partners\AppKey;
use App\Models\Partners\Partner;
use App\Models\Users\User;
use App\Repositories\PartnerRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class PartnerController extends Controller
{
    protected $partnerRepository;
    protected $userRepository;
    protected $serviceRepository;

    public function __construct()
    {
        $this->partnerRepository = new  PartnerRepository();
        $this->userRepository = new  UserRepository();
        $this->serviceRepository = new  ServiceRepository();
    }

    public function index()
    {
        $partners = $this->partnerRepository->getAllPartners();
        $count = $this->partnerRepository->getCountOfAllPartners();

        return view('partners.index', [
            'partners' => $partners,
            'count' => $count
        ]);
    }

    public function show($id)
    {
        $partner = $this->partnerRepository->getPartnerById($id);
        $partnerUsers = $this->partnerRepository->takeFivePartnerUsers($id);
        $partnerPackages = $this->partnerRepository->takeFivePartnerPackages($id);
        $roles = ['' => 'Select Role'] + $this->userRepository->getAllDistinctRolesArrayWithoutSuperAdmin();

        return view('partners.details', [
            'partnerUsers' => $partnerUsers,
            'partner' => $partner,
            'roles' => $roles,
            'partnerPackages' => $partnerPackages
        ]);
    }

    public function users($id)
    {
        $partner = $this->partnerRepository->getPartnerById($id);
        $partnerUsers = $this->partnerRepository->getPaginatedPartnerUsers($id);
        $count = $this->partnerRepository->getCountOfPartnerUsers($id);

        return view('partners.users', [
            'partnerUsers' => $partnerUsers,
            'partner' => $partner,
            'count' => $count
        ]);
    }

    public function deactivateOrReactivate($id)
    {
        // 0 is inactive; 1 is active
        $partner = $this->partnerRepository->getPartnerById($id);

        if ($partner->status == 1) {
            $partner->update([
                'status' => 0
            ]);
        } else {
            $partner->update([
                'status' => 1
            ]);
        }

        return redirect()->route('partners');
    }

    public function showEditBankDetails($id)
    {
        $partner = $this->partnerRepository->getPartnerById($id);

        return view('partners.edit-bank-details', [
            'partner' => $partner
        ]);
    }

    public function bankStore(BankDetailsRequest $request, $id)
    {
        $partner = $this->partnerRepository->getPartnerById($id);
        $partner->update($request->all());

        return redirect()->route('partner.details', $id);
    }

    public function create()
    {
        $services = ['' => 'Select Service(s)'] + $this->serviceRepository->getAllServicesArray();
        $premiumPartners = ['' => 'Select Type'] +
            [0 => 'This is a premium partner'] +
            $this->partnerRepository->getAllPremiumPartnersArray();

        return view('partners.create', [
            'services' => $services,
            'premiumPartners' => $premiumPartners
        ]);
    }

    public function store(Request $request)
    {
        $partnerInput = $request->except('services', 'firstName', 'lastName', 'phone', 'email', 'id_number');
        $partnerInput['role'] = null;

        if ($request->get('partner_id') == 0) {
            $partnerInput['type'] = 'Premium';
            $partnerInput['partner_id'] = null;
        } else {
            $partnerInput['type'] = 'Standard';
            $partnerInput['partner_id'] = $request->get('partner_id');
        }

        if ($request->get('firstname') && $request->get('lastname')) {
            $partnerInput['role'] = 'Contact Person';
        }

        $partner = Partner::create($partnerInput);

        if ($partner->role) {
            $userInput = $request->only(['firstName', 'lastName', 'phone', 'email', 'id_number', 'role']);
            $userInput['partner_id'] = $partner->id;
            User::create($userInput);
        }

        $appKeyInputIos['partner_id'] = $partner->id;
        $appKeyInputIos['app_name'] = $request->get('name');
        $appKeyInputIos['secret_key'] = uniqid() . Str::random(19);
        $appKeyInputIos['channel'] = 'ios';

        $appKeyInputAndroid['partner_id'] = $partner->id;
        $appKeyInputAndroid['app_name'] = $request->get('name');
        $appKeyInputAndroid['secret_key'] = uniqid() . Str::random(19);
        $appKeyInputAndroid['channel'] = 'android';

        AppKey::create($appKeyInputIos);
        AppKey::create($appKeyInputAndroid);

        $partner = $this->partnerRepository->getPartnerById($partner->id);
        $partner->services()->attach($request->get('services'));

        return redirect()->route('partners');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Users\User;
use App\Repositories\PartnerRepository;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    protected $userRepository;
    protected $partnerRepository;

    public function __construct()
    {
        $this->partnerRepository = new PartnerRepository();
        $this->userRepository = new UserRepository();
    }

    public function showCreateUser($id){
        $partner = $this->partnerRepository->getPartnerById($id);
        $roles = ['' => 'Select Role'] + $this->userRepository->getAllDistinctRolesArrayWithoutSuperAdmin();

        return view('partners.create-user', [
            'roles' => $roles,
            'partner' => $partner
        ]);
    }

    public function store(UserRequest $request, $partnerId)
    {
        $defaultPermissionsJsonObject = [];
        $defaultPermissionsJsonObject['platform.index'] = "1";
        $defaultPermissionsJsonObject['platform.systems.index'] = "1";
        $defaultPermissionsJsonObject['platform.systems.roles'] = "1";
        $defaultPermissionsJsonObject['platform.systems.users'] = "1";
        $defaultPermissionsJsonObject['platform.systems.attachment'] = "1";

        $encryptedPassword = bcrypt($request->get('password'));
        $user = new User();
        $user->create([
            'firstName' => $request->get('firstName'),
            'lastName' => $request->get('lastName'),
            'phone' => $request->get('phone'),
            'id_number' => $request->get('id_number'),
            'password' => $encryptedPassword,
            'email' =>$request->get('email'),
            'role' =>$request->get('role'),
            'partner_id' =>$request->get('partner_id'),
            'permissions' => $defaultPermissionsJsonObject,
        ]);

        return redirect()->route('partner.details', [
            'partner' => $partnerId
        ]);
    }
    public function details($id)
    {
        $user = $this->userRepository->getUserById($id);

        return view('users.details', [
            'user' => $user
        ]);
    }

    public function admins()
    {
        if (request()->ajax()) {
            $data = $this->userRepository->getSystemAdmins();
            return datatables()->of($data)
                ->addColumn('view', function ($row) {
                    return '<a href="/users/'.$row->id.'/details/"><i class="fas fa-eye"></i></a>';
                })
                ->rawColumns(['view' => 'view', 'delete' => 'delete'])
                ->make(true);
        }

        return view('users.admins');
    }
}

<?php

namespace App\Http\Controllers;

use App\Exports\InvoiceCustomerListExport;
use App\Helpers\General\CollectionHelper;
use App\Models\Partners\Partner;
use App\Repositories\PartnerRepository;
use App\Repositories\SubscriptionRepository;
use App\Repositories\TransactionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Excel;

class InvoiceController extends Controller
{
    protected $transactionRepository;
    protected $subscriptionRepository;
    protected $partnerRepository;

    public function __construct()
    {
        $this->transactionRepository = new TransactionRepository();
        $this->subscriptionRepository = new SubscriptionRepository();
        $this->partnerRepository = new PartnerRepository();
    }

    public function getGroupedInvoices()
    {
        $unpaidInvoices = $this->transactionRepository->fetchGroupedTransactions()->get()->toArray();
        $paidInvoices = (new TransactionRepository(null, null, true))->fetchGroupedTransactions()->get()->toArray();

        $groupedInvoiceCollection = collect(array_merge($unpaidInvoices, $paidInvoices));

        $groupedInvoiceCollection->map(function ($invoice) {
            $invoice->month_year = Carbon::parse($invoice->created_at)->format('F Y');
        });

        $pageSize = 5;

        $groupedInvoices = CollectionHelper::paginate($groupedInvoiceCollection->groupBy('month_year'), $pageSize);
        $groupedInvoiceCount = $groupedInvoiceCollection->count();

        $transactionCount = $groupedInvoices->count();

        return view('invoices.grouped-invoices', [
            'groupedInvoices' => $groupedInvoices,
            'transactionCount' => $transactionCount,
            'groupedInvoiceCount' => $groupedInvoiceCount
        ]);
    }

    public function getCustomerInvoices($partnerId, $partnerAppId)
    {
        $unpaidInvoices = (new TransactionRepository($partnerId, $partnerAppId))->fetchGroupedTransactions()->get()->toArray();
        $paidInvoices = (new TransactionRepository($partnerId, $partnerAppId, true))->fetchGroupedTransactions()->get()->toArray();
        $invoices = (collect(array_merge($unpaidInvoices, $paidInvoices)));

        $unpaidCustomerTransactions = (new TransactionRepository($partnerId, $partnerAppId))->getCustomerTransactions()->get()->toArray();
        $paidCustomerTransactions = (new TransactionRepository($partnerId, $partnerAppId, true))->getCustomerTransactions()->get()->toArray();
        // $customerTransactions = CollectionHelper::paginate((collect(array_merge($unpaidCustomerTransactions, $paidCustomerTransactions))), 5);
        $customerTransactions = collect(array_merge($unpaidCustomerTransactions, $paidCustomerTransactions));

        $customerTransactionCount = collect(array_merge($unpaidCustomerTransactions, $paidCustomerTransactions))->count();

        $partner = Partner::where('id', $partnerId)->first();

        $amountArray = array();

        foreach ($invoices as $invoice) {
            $invoice->partner_name = $partner->name;
            $invoice->partner_address = $partner->physical_address;
            $invoice->partner_city = $partner->city;
            $amountArray[] = $invoice->total_amount;
        }

        $input = array();
        $input['filter_partner_application'] = $partnerAppId;

        $customerCount = $invoices->count();

        $totalAmount = collect($amountArray)->sum();

        return view('invoices.invoices', [
            'customerTransactions' => $customerTransactions,
            'invoices' => $invoices,
            'customerCount' => $customerCount,
            'totalAmount' => $totalAmount,
            'customerTransactionCount' => $customerTransactionCount
        ]);
    }

    public function exportCsv(Request $request)
    {
        $string = $request->get('current_url');
        $pattern = "/\d/";
        if (preg_match_all($pattern, $string, $matches)) {
            $partner_id = $matches[0][0];
            $partner_app_id = $matches[0][1];
        }

        $partner_name = $this->partnerRepository->getPartnerById($partner_id)->name;
        $partner_app_name = $this->partnerRepository->getPartnerById($partner_app_id)->name;
        return Excel::download(new InvoiceCustomerListExport($partner_id, $partner_app_id), $partner_name . '-'. $partner_app_name .'-invoice-customer-list.csv');
    }

    public function sendMail($partnerId, $partnerAppId)
    {
        return Mail::to($request->user())->send(new OrderShipped($order));
    }
}

<?php

namespace App\Http\Controllers;

use App\Exports\SubscriptionsExport;
use App\Models\Subscriptions\SubscriptionProvider;
use App\Repositories\BeneficiaryRepository;
use App\Repositories\PackagePricingRepository;
use App\Repositories\PartnerRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\SubscriptionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use Excel;
class SubscriptionController extends Controller
{
    protected $subscriptionRepository;
    protected $partnerRepository;
    protected $beneficiaryRepository;
    protected $packagePricingRepostiory;
    protected $serviceRepository;

    public function __construct()
    {
        $this->subscriptionRepository = new SubscriptionRepository();
        $this->partnerRepository = new PartnerRepository();
        $this->beneficiaryRepository = new BeneficiaryRepository();
        $this->serviceRepository = new ServiceRepository();
        $this->packagePricingRepostiory = new PackagePricingRepository();
    }

    public function index(Request $request)
    {
        // Check index method of the CustomerController to understand the logic here

        if ((count($request->all()) > 1) && (isset($request->searchValue))) {
            if (strpos($request->searchValue, '=')) {
                $string = $request->searchValue;
                $array = explode('=', $string);
                $request->searchValue = $array[1];
            }
        }

        $partnerId = Auth::user()->partner_id;
        $partners = $this->subscriptionRepository->getSubscriptionPartnersForDropdown($partnerId);
        $services = $this->serviceRepository->getAllServices();
        $packagePricingDurationNames = $this->packagePricingRepostiory->getUniquePackagePricingDurationNames();
        $statuses = $this->subscriptionRepository->getUniqueStatuses();
        $subscriptions = $this->subscriptionRepository->getPartnerSubscriptions($request, $partnerId)['subscriptions'];
        $subscriptionCount = $this->subscriptionRepository->getPartnerSubscriptions($request, $partnerId)['count'];

        return view('customers.subscriptions.index', [
            'subscriptions' => $subscriptions,
            'partners' => $partners,
            'services' => $services,
            'packagePricingDurationNames' => $packagePricingDurationNames,
            'statuses' => $statuses,
            'subscriptionCount' => $subscriptionCount
        ]);
    }

    public function show($id)
    {
        $subscription = $this->subscriptionRepository->getSubscriptionById($id);
        $subscriptionCustomer = $this->subscriptionRepository->getSubscriptionCustomerBySubscriptionId($subscription->id);
        $subscriptionProviders = $this->partnerRepository->getActivePartners();
        $activeBeneficiaries = $this->beneficiaryRepository->getActiveBeneficiaries($subscription->id);
        $activeBeneficiariesCount = $this->beneficiaryRepository->getCountOfActiveBeneficiaries($subscription->id);
        $pendingBeneficiaries = $this->beneficiaryRepository->getPendingBeneficiaries($subscription->id);
        $pendingBeneficiariesCount = $this->beneficiaryRepository->getCountOfPendingBeneficiaries($subscription->id);

        return view('customers.subscriptions.details', [
            'subscription' => $subscription,
            'subscriptionCustomer' => $subscriptionCustomer,
            'subscriptionProviders' => $subscriptionProviders,
            'activeBeneficiaries' => $activeBeneficiaries,
            'activeBeneficiariesCount' => $activeBeneficiariesCount,
            'pendingBeneficiaries' => $pendingBeneficiaries,
            'pendingBeneficiariesCount' => $pendingBeneficiariesCount
        ]);
    }

    public function assignProvider(Request $request, $subscriptionId)
    {
        $partnerId = $request->get('provider');
        $partner = $this->partnerRepository->getPartnerById($partnerId);
        $subscriptionProvider = $this->subscriptionRepository->getSubscriptionProviderBySubscriptionId($subscriptionId);

        $subscriptionProvider->update([
            'partner_id' => $partnerId,
            'partner_name' => $partner->name,
            'subscription_id' => $subscriptionId
        ]);

        return back();
    }

    public function exportPdf(Request $request)
    {
        if ($request->get('current_url')) {
            $newRequestArray = array();

            $parametersArray = explode('&', $request->get('current_url'));
         
            foreach ($parametersArray as $queryString) {
                $parameter_pair_array = explode('=', $queryString);
                $newRequestArray[$parameter_pair_array[0]] =  $parameter_pair_array[1];
            }

            $newRequestArray['subscriptions-export'] = $request->get('subscriptions-export');
            $input = (object) $newRequestArray;
            // Assumption: Logged in user is standard
        } else {
            $input = $request;
        }

        $partnerId = Auth::user()->partner_id;
        $data = $this->subscriptionRepository->getPartnerSubscriptions($input, $partnerId)['subscriptions'];
        
        // share data to view
        view()->share('employee', $data);
        $pdf = PDF::loadView('customers.subscriptions.pdf_view', $data)->setPaper('a4', 'landscape');

        // download PDF file with download method
        return $pdf->download('pdf_file.pdf');
    }

    public function exportExcel(Request $request){
        if ($request->get('current_url')) {
            $newRequestArray = array();

            $parametersArray = explode('&', $request->get('current_url'));

            foreach ($parametersArray as $queryString) {
                $parameter_pair_array = explode('=', $queryString);
                $newRequestArray[$parameter_pair_array[0]] =  $parameter_pair_array[1];
            }

            $newRequestArray['subscriptions-export'] = $request->get('subscriptions-export');
            $input = (object) $newRequestArray;
            // Assumption: Logged in user is standard
        } else {
            $input = $request;
        }

        $partnerId = Auth::user()->partner_id;

        return Excel::download(new SubscriptionsExport($input, $partnerId), 'subscriptions.xlsx');
    }

    public function exportCsv(Request $request)
    {
        if ($request->get('current_url')) {
            $newRequestArray = array();

            $parametersArray = explode('&', $request->get('current_url'));

            foreach ($parametersArray as $queryString) {
                $parameter_pair_array = explode('=', $queryString);
                $newRequestArray[$parameter_pair_array[0]] =  $parameter_pair_array[1];
            }

            $newRequestArray['subscriptions-export'] = $request->get('subscriptions-export');
            $input = (object) $newRequestArray;
            // Assumption: Logged in user is standard
        } else {
            $input = $request;
        }

        $partnerId = Auth::user()->partner_id;

        return Excel::download(new SubscriptionsExport($input, $partnerId), 'subscriptions.csv');
    }
}

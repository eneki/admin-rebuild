<?php

namespace App\Http\Requests;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

class DispatchIncidentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dispatch_comments' => 'required',
            'mode_of_dispatch' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'dispatch_comments.required'  => 'Enter the dispatch comments',
            'mode_of_dispatch.required'  => 'Enter the mode of dispatch'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'Form Validation Failed',
            'errors' => $errors
        ]));
    }

    protected function failedAuthorization()
    {
        throw new AuthorizationException(response()->json([
            'success' => false,
            'message' => 'Form Validation Failed',
        ]));
    }
}

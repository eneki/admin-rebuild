<?php

namespace App\Http\Requests;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required',
            'lastName' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'id_number' => 'required',
            'role' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'firstName.required'  => 'Enter your first name',
            'lastName.required'  => 'Enter your last name',
            'phone.required'  => 'Enter your phone number',
            'email.required'  => 'Enter your email address',
            'id_number.required'  => 'Enter your ID or passport number',
            'role.required'  => 'Select a role'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'Form Validation Failed',
            'errors' => $errors
        ]));
    }

    protected function failedAuthorization()
    {
        throw new AuthorizationException(response()->json([
            'success' => false,
            'message' => 'Form Validation Failed',
        ]));
    }
}

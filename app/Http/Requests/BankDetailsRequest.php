<?php

namespace App\Http\Requests;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

class BankDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_name' => 'required',
            'account_name' => 'required',
            'account_number' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'bank_name.required'  => 'Enter your bank name',
            'account_name.required'  => 'Enter your account name',
            'account_number.required'  => 'Enter your account number'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'Form Validation Failed',
            'errors' => $errors
        ]));
    }

    protected function failedAuthorization()
    {
        throw new AuthorizationException(response()->json([
            'success' => false,
            'message' => 'Form Validation Failed',
        ]));
    }
}

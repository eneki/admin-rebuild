<?php

namespace App\Http\Requests;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

class StationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'lat' => 'required',
            'long' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'  => 'Enter the station\'s name',
            'phone.required'  => 'Enter the station\'s number',
            'address.required'  => 'Enter the station\'s address'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'Form Validation Failed',
            'errors' => $errors
        ]));
    }

    protected function failedAuthorization()
    {
        throw new AuthorizationException(response()->json([
            'success' => false,
            'message' => 'Form Validation Failed',
        ]));
    }
}

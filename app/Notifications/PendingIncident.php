<?php

namespace App\Notifications;

use App\Models\Incidents\Report;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Orchid\Platform\Notifications\DashboardChannel;
use Orchid\Platform\Notifications\DashboardMessage;

class PendingIncident extends Notification implements ShouldQueue
{
    use Queueable;

    private $report, $location;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Report $report, $location)
    {
        $this->report = $report;
        $this->location = $location;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [DashboardChannel::class];
    }

    public function toDashboard($notifiable)
    {
        return (new DashboardMessage)
            ->title($this->report->incident->name)
            ->message('User: '. $this->report->customer->fullName)
            ->action(url('pending-incidents/'. $this->report->id . '/report/customer/' . $this->report->customer->id));
    }
}

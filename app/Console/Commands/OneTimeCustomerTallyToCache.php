<?php

namespace App\Console\Commands;

use App\Repositories\CustomerRepository;
use Illuminate\Console\Command;

class OneTimeCustomerTallyToCache extends Command
{
    public $customerRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upesy:one-time-customer-tally-to-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add historical customer tally to cache once';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->customerRepository = new CustomerRepository();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->customerRepository->oneTimeCustomerTallyToCache();
    }
}

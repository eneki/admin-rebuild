<?php

namespace App\Console\Commands;

use App\Repositories\CustomerRepository;
use Illuminate\Console\Command;

class OneTimeCustomerTallyToDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upesy:one-time-customer-tally-to-db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pseudo-warehouse historical customer tally once';

    protected $customerRepository;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->customerRepository = new CustomerRepository();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return $this->customerRepository->oneTimeCustomerTallyToDb();
    }
}

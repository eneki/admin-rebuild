<?php

namespace App\Console\Commands;

use App\Models\Partners\Partner;
use App\Repositories\CustomerRepository;
use Illuminate\Console\Command;
use Report\Repositories\ReportRepository;

class OneTimeCommands extends Command
{
    public $customerRepository;
    public $reportRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upesy:one-time-commands';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'One time commands';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->customerRepository = new CustomerRepository();
        $this->reportRepository = new ReportRepository();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->cacheDailyIncidentTally();
    }

    public function cacheDailyIncidentTally(){
        $this->reportRepository->cacheDailyIncidentTally();
        dd('that');
    }

    private function oneTimeCustomerTallyToCache()
    {
        $this->customerRepository->oneTimeCustomerTallyToCache();
        dd('this');
    }

    private function updateDailyCustomerTallyOnDb()
    {
        $this->customerRepository->updateDailyCustomerTallyOnDb();
        dd('here');
    }

    private function deactivatedDeletedPartners()
    {
        $activePartners = Partner::whereNull('deleted_at')->get();

        foreach ($activePartners as $activePartner) {
            $activePartner->update([
                'status' => 1
            ]);
        }

        $inactivePartners = Partner::withTrashed()->whereNull('status')->get();

        foreach ($inactivePartners as $inactivePartner) {
            $inactivePartner->update([
                'status' => 0
            ]);

            $inactivePartner->restore();
        }
    }
}

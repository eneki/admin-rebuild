<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Report\Repositories\ReportRepository;

class CacheDailyIncidentTally extends Command
{
    public $reportRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upesy:cache-daily-incident-tally';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add incident tally to cache daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->reportRepository = new ReportRepository();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->reportRepository->cacheDailyIncidentTally();
    }
}

<?php

namespace App\Console\Commands;

use App\Repositories\CustomerRepository;
use Illuminate\Console\Command;

class UpdateDailyCustomerTallyOnDb extends Command
{
    public $customerRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upesy:update-daily-customer-tally-on-db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update customer tally db entries daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->customerRepository = new CustomerRepository();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->customerRepository->updateDailyCustomerTallyOnDb();
    }
}

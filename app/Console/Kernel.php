<?php

namespace App\Console;

use App\Console\Commands\CacheDailyIncidentTally;
use App\Console\Commands\OneTimeCommands;
use App\Console\Commands\OneTimeCustomerTallyToCache;
use App\Console\Commands\OneTimeCustomerTallyToDb;
use App\Console\Commands\UpdateDailyCustomerTallyOnDb;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        OneTimeCommands::class,
        OneTimeCustomerTallyToDb::class,
        OneTimeCustomerTallyToCache::class,
        CacheDailyIncidentTally::class,
        UpdateDailyCustomerTallyOnDb::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('upesy:one-time-customer-tally-to-db')->everyThirtyMinutes()->withoutOverlapping();
        // $schedule->command('upesy:one-time-customer-tally-to-cache')->everyThirtyMinutes()->withoutOverlapping();
        $schedule->command('upesy:cache-daily-incident-tally')->daily();
        $schedule->command('upesy:update-daily-customer-tally-on-db')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

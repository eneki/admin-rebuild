<?php

namespace App\Exports;

use App\Repositories\SubscriptionRepository;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SubscriptionsExport implements FromCollection, WithMapping, WithHeadings
{
    protected $partnerId, $input, $subscriptionRepository;

    public function __construct($input, $partnerId)
    {
        $this->partnerId = $partnerId;
        $this->input = $input;
        $this->subscriptionRepository = new SubscriptionRepository();
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->subscriptionRepository->getUnpaginatedPartnerSubscriptions($this->input, $this->partnerId);
    }

    public function map($subscription): array
    {
        return [
            $subscription->customer->fullName,
            $subscription->customer->phone,
            $subscription->customer->organisation,
            $subscription->customer->id_number,
            $subscription->invoice_number,
            $subscription->packagePricing->duration_name,
            $subscription->amount,
            $subscription->subscription_date,
            $subscription->expiration_date,
            $subscription->partner->name,
            $subscription->providing_partner->partner->name,
            $subscription->service->name,
            $subscription->status
        ];
    }

    public function headings(): array
    {
        return [
            'Name',
            'Phone',
            'Organization'
        ];
    }
}

<?php

namespace App\Exports;

use App\Repositories\TransactionRepository;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class InvoiceCustomerListExport implements FromCollection, WithMapping, WithHeadings
{
    protected $partnerId, $partnerAppId;

    public function __construct($partnerId, $partnerAppId)
    {
        $this->partnerAppId = $partnerAppId;
        $this->partnerId = $partnerId;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $unpaidCustomerTransactions = (new TransactionRepository($this->partnerId, $this->partnerAppId))->getCustomerTransactions()->get()->toArray();
        $paidCustomerTransactions = (new TransactionRepository($this->partnerId, $this->partnerAppId, true))->getCustomerTransactions()->get()->toArray();
       
        return collect(array_merge($unpaidCustomerTransactions, $paidCustomerTransactions));
    }

    public function map($customer): array
    {
        return [
            $customer->firstName . ' ' . $customer->lastName,
            $customer->phone,
            $customer->organisation,
            $customer->id_number,
            $customer->invoice_number,
            $customer->duration_name,
            $customer->amount,
            $customer->created_at,
            $customer->expires_at,
            $customer->partner_name,
            $customer->partner_app,
            $customer->service_name,
            $customer->status
        ];
    }

    public function headings(): array
    {
        return [
            'Name',
            'Phone',
            'Organization',
            'Customer ID',
            'Invoice Number',
            'Package',
            'Amount',
            'Subscription Date',
            'Expiration Date',
            'App',
            'Provider',
            'Service',
            'Status',
        ];
    }
}

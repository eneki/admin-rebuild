$(document).ready(function () {
    let url = window.location.pathname
    let id = url.substring(url.lastIndexOf('/') + 1)
    fill_datatable();

    function fill_datatable(id_parameter = id) {
        let dataTable = $('#customer-subscriptions-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "/customers/"+id_parameter
            },
            columns: [
                {
                    data: 'status',
                    name: 'status',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'providing_partner.partner_name',
                    name: 'providing_partner.partner_name',
                    defaultContent: "<i>Not set</i>",
                    orderable: false,
                },
                {
                    data: 'package_pricing.duration_name',
                    name: 'package_pricing.duration_name',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'service.name',
                    name: 'service.name',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'subscription_date',
                    name: 'subscription_date',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                }, {
                    data: 'expiration_date',
                    name: 'expiration_date',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'partner.name',
                    name: 'partner.name',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'package_pricing.amount',
                    name: 'package_pricing.amount',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                }
            ],
            "pageLength": 5,
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [':visible']
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [':visible']
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [':visible']
                    }
                },
            ],
            lengthMenu: [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]]
        });
    }

});

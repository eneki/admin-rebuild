function initialize(lat, long) {
    console.log(lat, long, 'taste')
    var latlng = new google.maps.LatLng(lat, long);
    var map = new google.maps.Map(document.getElementById('residence_location_map'), {
        center: latlng,
        zoom: 13
    });
    var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: false,
        anchorPoint: new google.maps.Point(0, -29)
    });
    var infowindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marker, 'click', function () {
        var iwContent = '<div id="iw_container">' +
            '<div class="iw_title"><b>Location</b> : Noida</div></div>';
        // including content to the infowindow
        infowindow.setContent(iwContent);
        // opening the infowindow in the current map and at the current marker location
        infowindow.open(map, marker);
    });
}

google.maps.event.addDomListener(window, 'load', initialize);

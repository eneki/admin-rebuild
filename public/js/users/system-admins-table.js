$(document).ready(function () {
    fill_datatable();

    function fill_datatable() {
        let dataTable = $('#system-admins-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "/users/admins"
            },
            columns: [
                {
                    data: "fullName",
                    name: "fullName",
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'phone',
                    name: 'phone',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'email',
                    name: 'email',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'member_since',
                    name: 'member_since',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'role',
                    name: 'role',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'view',
                    name: 'view',
                    orderable: false
                }
            ],
            columnDefs: [ {
                targets: [ 0 ]
            }
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, ':visible' ]
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, ':visible' ]
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, ':visible' ]
                    }
                },
                'colvis'
            ],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
        });
    }
});

$(document).ready(function () {
    fill_datatable();

    function fill_datatable(pending_incidents_filter_start_date = '', pending_incidents_filter_end_date = '', pending_incidents_filter_incident_type = '') {
        let dataTable = $('#pending-incidents-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "/pending-incidents",
                data: {
                    filter_start_date: pending_incidents_filter_start_date,
                    filter_end_date: pending_incidents_filter_end_date,
                    filter_incident_type: pending_incidents_filter_incident_type
                },
            },
            columns: [
                {
                    data: "image_url",
                    name: "image_url",
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'service.name',
                    name: 'service.name',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'report.customer.fullName',
                    name: 'report.customer.fullName',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'report.reported_on',
                    name: 'report.reported_on',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'report.customer.phone',
                    name: 'report.customer.phone',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'view-on-map',
                    name: 'view-on-map',
                    orderable: false
                },
                {
                    data: 'open',
                    name: 'open',
                    orderable: false
                }
            ],
            columnDefs: [ {
                targets: [ 0 ]
            }
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [':visible' ]
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [':visible' ]
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [':visible' ]
                    }
                },
                'colvis'
            ],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
        });
    }

    $('#pending_incidents_filter').click(function () {
        let filter_start_date = $('#pending_incidents_filter_start_date').val();
        let filter_end_date = $('#pending_incidents_filter_end_date').val();
        let filter_incident_type = $('#pending_incidents_filter_incident_type').val();

        if ((filter_start_date == '' && filter_end_date != '') || (filter_start_date != '' && filter_end_date == '')) {
            alert('Select both date filters')
        } else if (filter_start_date > filter_end_date) {
            alert('The start date given should be before the end date')
        } else {
            $('#pending-incidents-table').DataTable().destroy();
            fill_datatable(filter_start_date, filter_end_date, filter_incident_type);
        }
    })

    $('#pending_incidents_reset').click(function () {
        $('#pending_incidents_filter_start_date').val('');
        $('#pending_incidents_filter_end_date').val('');
        $('#pending_incidents_filter_incident_type').val('');
        $('#pending-incidents-table').DataTable().destroy();
        fill_datatable();
    })
});

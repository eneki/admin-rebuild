$(document).ready(function () {
    fill_datatable();

    function fill_datatable(resolved_filter_start_date = '', resolved_filter_end_date = '', resolved_filter_incident_type = '') {
        let dataTable = $('#resolved-incidents-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "/resolved-incidents",
                data: {
                    filter_start_date: resolved_filter_start_date,
                    filter_end_date: resolved_filter_end_date,
                    filter_incident_type: resolved_filter_incident_type
                },
            },
            columns: [
                {
                    data: "image_url",
                    name: "image_url",
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'service.name',
                    name: 'service.name',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'report.customer.fullName',
                    name: 'report.customer.fullName',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'report.reported_on',
                    name: 'report.reported_on',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'report.customer.phone',
                    name: 'report.customer.phone',
                    defaultContent: "<i>Not set</i>",
                    orderable: false
                },
                {
                    data: 'view-on-map',
                    name: 'view-on-map',
                    orderable: false
                }, {
                    data: 'open',
                    name: 'open',
                    orderable: false
                }
            ],
            columnDefs: [{
                targets: [0]
            }
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [':visible']
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [':visible']
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [':visible']
                    }
                },
                'colvis'
            ],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
        });
    }

    $('#resolved_filter').click(function () {
        let filter_start_date = $('#resolved_filter_start_date').val();
        let filter_end_date = $('#resolved_filter_end_date').val();
        let filter_incident_type = $('#resolved_filter_incident_type').val();

        if ((filter_start_date == '' && filter_end_date != '') || (filter_start_date != '' && filter_end_date == '')) {
            alert('Select both date filters')
        } else if (filter_start_date > filter_end_date) {
            alert('The start date given should be before the end date')
        } else {
            $('#resolved-incidents-table').DataTable().destroy();
            fill_datatable(filter_start_date, filter_end_date, filter_incident_type);
        }
    })

    $('#resolved_reset').click(function () {
        $('#resolved_filter_start_date').val('');
        $('#resolved_filter_end_date').val('');
        $('#resolved_filter_incident_type').val('');
        $('#resolved-incidents-table').DataTable().destroy();
        fill_datatable();
    })
});

@extends('platform::dashboard')

@section('content')
    <h1 class="ml-5 mb-3 font-thin h3 text-black">Resolved Incidents</h1>
    <div class="white-background ml-5 mr-5 pb-3 mb-3">
        <form action="/resolved-incidents">
            <div class="ml-3-pc pt-5">
                <div id="resolved_incidents_table_map" class="incident-map--margin incident-map--height mb-5"></div>

                <label for="resolved_filter_incident_type">Incident Type</label>
                <select class="d-inline-block custom-select" id="resolved_filter_incident_type" name="filter_incident_type">
                    <option value="">Select</option>
                    @foreach ($incidentTypes as $incidentType)
                        <option value="{{ $incidentType->id }}">{{ $incidentType->name }}</option>
                    @endforeach
                </select>

                <label for="resolved_filter_start_date" class="m-l-2-4">Start Date</label>
                <input type="date" class="form-control custom-select date-select" placeholder="Start Date"
                    aria-label="Start Date" aria-describedby="basic-addon1" name="filter_start_date"
                    id="resolved_filter_start_date">

                <label for=resolved_filter_end_date" class="m-l-2-4">End Date</label>
                <input type="date" class="form-control custom-select date-select" placeholder="End Date"
                    aria-label="End Date" aria-describedby="basic-addon1" name="filter_end_date"
                    id="resolved_filter_end_date">

                <input id="customers-search-hidden-input" hidden name="searchValue">

                <button type="submit" id="resolved_filter" class="btn filter-button d-inline-block ml-3"
                    onclick="checkWhetherTheTableContainsSearchResults()">Filter
                </button>
                <button onclick="clearFiltersResolvedIncidentsTable()" id="resolved_reset"
                    class="btn reset-button d-inline-block ml-3">Reset
                </button>
            </div>
        </form>
        <div class="row">
            <div class="col-6 offset-6">
                <form action="/resolved-incidents" class="ml-5 incidents-table__search-form">
                    <label for="">Search</label>
                    <input class="form-control form__table-search--border" type="text" name="searchValue"
                        aria-label="Search">
                </form>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-striped w-95 mx-auto mt-3" id="resolved-incidents-table">
                <thead class="table-head-color text-left">
                    <tr>
                        <th scope="col" class="w-10">PROFILE PIC</th>
                        <th scope="col" class="w-15">INCIDENT TYPE</th>
                        <th scope="col" class="w-20">NAME</th>
                        <th scope="col" class="w-20">REPORTED ON</th>
                        <th scope="col" class="w-15">USER PHONE</th>
                        <th scope="col" class="text-center w-10">VIEW ON MAP</th>
                        <th scope="col" class="text-center">OPEN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($reports as $report)
                        <tr>
                            <td class="text-center"><img src="{!!  $report->customer->profile_pic !!}" alt=""
                                    class="small-table__image rounded-circle"></td>
                            <td>{!! $report->incident->name !!}</td>
                            <td>{!! $report->customer->fullName !!}</td>
                            <td>{!! $report->reported_on !!}</td>
                            <td>{!! $report->customer->phone !!}</td>
                            <td class="text-center">
                                @if (
                                $report->reportLocations()->orderBy('id', 'desc')->first() == null 
                                || 
                                $report->reportLocations()->latest()->first() == null
                                )
                                    <i onclick="customerHasNoKnownAddress()"
                                        class="fas fa-exclamation-triangle blue-font text-center" aria-hidden="true"></i>
                                @else
                                    <i onclick="initialize('{{ $report->reportLocations()->orderBy('id', 'desc')->first()->lat }}' , '{{ $report->reportLocations()->latest()->first()->long }}')"
                                        class="fas fa-map-marker blue-font text-center" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="text-center">
                                <a
                                    href="{{ url('dispatched-and-resolved-incidents/' . $report->id . '/report/customer/' . $report->reported_by) }}"><i
                                        class="fas fa-eye blue-font"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <span class="ml-5">Showing 1 - 10 of {{ $reportCount }} incident reports</span>

            {{ $reports->links() }}
        </div>
    @stop

    @section('pageSpecificScripts')
        <script>
            initialize(-1.286389, 36.817223)

            function customerHasNoKnownAddress() {
                alert('Customer has no known address')
            }

            function initialize(lat, long) {
                var latlng = new google.maps.LatLng(lat, long);
                var map = new google.maps.Map(document.getElementById('resolved_incidents_table_map'), {
                    center: latlng,
                    zoom: 17
                });
                var marker = new google.maps.Marker({
                    map: map,
                    position: latlng,
                    draggable: false,
                    anchorPoint: new google.maps.Point(0, -29)
                });
                var infowindow = new google.maps.InfoWindow();
                google.maps.event.addListener(marker, 'click', function() {
                    var iwContent = '<div id="iw_container">' +
                        '<div class="iw_title"><b>Location</b> : Noida</div></div>';
                    // including content to the infowindow
                    infowindow.setContent(iwContent);
                    // opening the infowindow in the current map and at the current marker location
                    infowindow.open(map, marker);
                });
            }

        </script>
        <script>
            clearFiltersResolvedIncidentsTable = () => {
                let href = '/resolved-incidents'
                window.href = href
            }

        </script>

        <script>
            function checkWhetherTheTableContainsSearchResults() {
                let search = location.search.substring(1)
                searchInputElement = document.getElementById('customers-search-hidden-input');

                if (search != '') {
                    searchInputElement.setAttribute('value', search);
                }
            }

        </script>
    @stop
</div>

@extends('platform::dashboard')

@section('content')
    <div class="container">
        <h1 class="ml-1-neg mb-5 mt-1 font-thin h3 text-black">Incident: {{ $incidentReport->incident->partner->name }}</h1>

        <div class="customer-details-container">
            <div class="container mb-5">
                <div class="row">
                    <div class="col-sm-6">
                        @include('customers.partials.customer-details-panel')
                    </div>
                    @if (!empty($customer->residences->toArray()))
                        <div class="col-sm-6">
                            @include('customers.partials.customer-residence-details-panel')
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    @include('customers.partials.subscriptions')
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    @include('customers.partials.guardians')
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-sm-8">
                    @include('incidents.partials.map')
                </div>
                <div class="col-sm-4">
                    @include('incidents.partials.media')
                </div>
            </div>
            <div class="row" style="margin-top: 2rem; margin-left: -2rem;">
                <div class="col-sm-6">
                    @include('incidents.partials.pending-workflow')
                </div>
                <div class="col-sm-6">
                    @include('incidents.partials.details')
                </div>
            </div>
        </div>
    </div>
@stop

@section('pageSpecificScripts')
    <script>
        function ajaxCall() {
            $.ajax({
                type: 'GET',
                url: 'current-location/refresh',
                success: function(response) {
                    var latlng = new google.maps.LatLng(response.data.lat, response.data.long);
                    var map = new google.maps.Map(document.getElementById(
                        'pending_incident_details_table_map'), {
                        center: latlng,
                        zoom: 20
                    });
                    var marker = new google.maps.Marker({
                        map: map,
                        position: latlng,
                        draggable: false,
                        anchorPoint: new google.maps.Point(0, -29)
                    });
                    var infowindow = new google.maps.InfoWindow();
                    google.maps.event.addListener(marker, 'click', function() {
                        var iwContent = '<div id="iw_container">' +
                            '<div class="iw_title"><b>Location</b> : Noida</div></div>';
                        // including content to the infowindow
                        infowindow.setContent(iwContent);
                        // opening the infowindow in the current map and at the current marker location
                        infowindow.open(map, marker);
                    });
                }
            });
        }

        $(document).ready(function() {
            setInterval(ajaxCall, 10000);
        });

    </script>
@endsection

@extends('platform::dashboard')

@section('title', 'Dispatch Incident')

@section('content')
    <div class="container">

        <form action="/incident/{{ $incidentReport->id }}/{{ $incidentReport->customer->id }}/dispatch" method="POST">
            @csrf
            <div class="row my-2">
                <div class="col-2">
                    <label for="mode_of_dispatch">Mode of Dispatch</label>
                </div>
                <div class="col-6">
                    <input type="text" name="mode_of_dispatch" id="mode_of_dispatch" class="w-100 form__input" required>
                </div>
            </div>
            <div class="row my-2">
                <div class="col-2">
                    <label for="dispatch_comments">Dispatch Comments</label>
                </div>
                <div class="col-6">
                    <textarea id="dispatch_comments" name="dispatch_comments" rows="4" cols="30"
                        class="w-100 form__textarea" required></textarea>
                </div>
            </div>
            <input type="hidden" name="status" value="Dispatched">

            <div class="row my-2">
                <div class="col-4 offset-8">
                    <a href="javascript:history.back()">
                        <button type="button"
                            class="btn full-page-form__button full-page-form__button--back-border px-4 d-inline-block"
                            data-dismiss="modal">Back</button>
                    </a>
                    <button type="submit"
                        class="btn full-page-form__button d-inline-block full-page-form__button--save-color px-4">Dispatch
                        Officer</button>
                </div>
            </div>
        </form>

    </div>

@stop

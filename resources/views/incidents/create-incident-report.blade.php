@extends('platform::dashboard')

@section('title', 'Create Incident Report')

@section('content')
    <div class="container">
        {!! Form::open(['route' => ['incident-act-upon', 
        'report' => $incidentReport->id, 
        'customer' => $incidentReport->customer->id]]) !!}

        <div class="row my-2">
            <div class="col-2">
                {!! Form::label('status', 'Report Status') !!}
            </div>
            <div class="col-6">
                {!! Form::select('status', $reportStatusArray, '', ['class' => 'w-100 form__input', 'required']) !!}
            </div>
        </div>
        <div class="row my-2">
            <div class="col-2">
                {!! Form::label('report', 'Brief Report') !!}
            </div>
            <div class="col-6">
                {!! Form::textarea('report', null, ['placeholder' => 'Brief Report', 'class' => 'w-100 form__textarea', 'required']) !!}
            </div>
        </div>

        <div class="row my-2">
            <div class="col-4 offset-8">
                <a href="javascript:history.back()">
                    <button type="button"
                        class="btn full-page-form__button full-page-form__button--back-border px-4 d-inline-block"
                        data-dismiss="modal">Back</button>
                </a>
                <button type="submit"
                    class="btn full-page-form__button d-inline-block full-page-form__button--save-color px-4">Create
                    Report</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
    </div>
    </div>
@stop

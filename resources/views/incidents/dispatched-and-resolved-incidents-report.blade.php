@extends('platform::dashboard')

@section('content')
    {{-- <div class="container">
        @include('incidents.partials.details')
        @include('incidents.partials.premium-subscriptions')
        @include('incidents.partials.friends')
        @include('incidents.partials.dispatched-and-resolved-status')
    </div> --}}
    <div class="container">
        <h1 class="ml-1-neg mb-5 mt-1 font-thin h3 text-black">Incident: {{ $incidentReport->incident->partner->name }}</h1>

        <div class="customer-details-container">
            <div class="row justify-content-between d-flex">
                <div class="col-sm-5">
                    @include('customers.partials.customer-details-panel')
                </div>
                @if (!empty($customer->residences->toArray()))
                    <div class="col-sm-5">
                        @include('customers.partials.customer-residence-details-panel')
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-sm-12">
                    @include('customers.partials.subscriptions')
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    @include('customers.partials.guardians')
                </div>
            </div>

            {{-- @include('incidents.partials.premium-subscriptions')
            --}}
            {{-- @include('incidents.partials.friends') --}}

            <div class="row mt-4">
                <div class="col-sm-8">
                    @include('incidents.partials.map')
                </div>
                <div class="col-sm-4">
                    @include('incidents.partials.media')
                </div>
            </div>
            <div class="row" style="margin-top: 2rem; margin-left: -2rem;">
                <div class="col-sm-6">
                    @include('incidents.partials.dispatched-and-resolved-workflow')
                </div>
                <div class="col-sm-6">
                    @include('incidents.partials.details')
                </div>
            </div>
        </div>
    </div>
@stop

<div class="row">
    <div class="col-12 workflow-border">
        <div class="col-sm-6 customer-details-title-font mt-3">Workflow</div>

        <table class="table w-95 mx-auto mt-3">
            <thead class="table-head-color text-left">
            <tr>
                <th scope="col">ACTION</th>
                <th scope="col">TIME</th>
                <th scope="col">STATUS</th>
                <th scope="col">ACTIONS</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Reported</td>
                <td>{!! $incidentReport->created_at !!}</td>
                <td><i class="fas fa-check green-text"></i></td>
                <td>
                    <p>N/A</p>
                </td>
            </tr>
            <tr>
                <td>Dispatched</td>
                <td>{!! $incidentReport->dispatched_at !!}</td>
                @if($incidentReport->dispatched_at)
                    <td><i class="fas fa-check green-text"></i></td>
                    <td>
                        <p>Dispatched</p>
                    </td>
                @else
                    <td><i class="fas fa-times red-text"></i></td>
                    <td>
                        <a class="link-color" href="{{ route('incident-dispatch-show', $incidentReport->id) }}">Dispatch</a>
                    </td>
                @endif

            </tr>
            <tr>
                <td>Acted Upon</td>
                <td>{!! $incidentReport->acted_upon_at !!}</td>
                <td><i class="fas fa-times red-text"></i></td>
                <td>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

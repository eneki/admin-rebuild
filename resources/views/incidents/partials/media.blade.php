<div class="row">
    <div class="col-12 location-card--border">
        <div class="customer-details-title-font mt-3 mb-3">Media captured at user's location</div>

        <div style="width: 100%; height: 328px;">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                @if ($incidentReport->reportMedia)
                    @foreach ($incidentReport->reportMedia as $reportMedia)
                        @if ($reportMedia->type == 'video')
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                    aria-controls="home" aria-selected="true">Video</a>
                            </li>
                        @elseif($reportMedia->type == 'audio')
                            <li class="nav-item">
                                <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                    aria-controls="profile" aria-selected="false">Audio</a>
                            </li>
                        @elseif($reportMedia->type == 'image')

                            <li class="nav-item">
                                <a class="nav-link active" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                                    aria-controls="contact" aria-selected="false">Image</a>
                            </li>
                        @endif
                    @endforeach
                @endif
            </ul>
            <div class="tab-content" id="myTabContent">
                @if ($incidentReport->reportMedia)
                    @foreach ($incidentReport->reportMedia as $reportMedia)
                        @if ($reportMedia->type == 'video')
                            @if ($reportMedia->report_media)
                                <div class="tab-pane fade show active" id="home" role="tabpanel"
                                    aria-labelledby="home-tab">
                                    <video width="320" height="240" controls>
                                        <source src="{{ $reportMedia->report_media }}" type="video/mp4">
                                        <source src="{{ $reportMedia->report_media }}" type="video/ogg">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                            @else
                                <p class="mt-3">This report's media cannot be found.</p>
                            @endif
                        @elseif($reportMedia->type == 'audio')
                            @if ($reportMedia->report_media)
                                <div class="tab-pane fade show active" id="profile" role="tabpanel"
                                    aria-labelledby="profile-tab">
                                    <audio controls>
                                        <source src="{{ $reportMedia->report_media }}" type="audio/3gp">
                                        <source src="{{ $reportMedia->report_media }}" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                    </audio>
                                </div>
                            @else
                                <p class="mt-3">This report's media cannot be found.</p>
                            @endif
                        @elseif($reportMedia->type == 'image')
                            @if ($reportMedia->report_media)
                                <div class="tab-pane fade show active" id="contact" role="tabpanel"
                                    aria-labelledby="contact-tab">
                                    <img src="{{ $reportMedia->report_media }}" alt="">
                                </div>
                            @else
                                <p class="mt-3">This report's media cannot be found.</p>
                            @endif
                        @endif
                    @endforeach
                @else
                    <p class="mt-3">This report has no media</p>
                @endif
            </div>
        </div>
    </div>
</div>

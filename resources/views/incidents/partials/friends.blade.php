<div class="row">
    <div class="col-12">
        <div class="font-1-25 ml-2-rem mt-2-rem">Friends</div>

        <table class="table table-striped w-95 mx-auto mt-3">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Phone</th>
                <th scope="col">Email Address</th>
                <th scope="col">Member Since</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($incidentReport->customer->friends as $friend)
                <tr>
                    <td>{!! $friend->customer->fullName !!}</td>
                    <td>{!! $friend->customer->phone !!}</td>
                    <td>{!! $friend->customer->email !!}</td>
                    <td>{!! $friend->customer->created_at->diffForHumans() !!}</td>
                    <td>
                        <a href="{{ route('customers.details', $friend->friend_id) }}"><i class="fas fa-eye"></i></a>
                        @if(!$friend->customer->deleted_at)
                            <a href="{{ route('customer.delete', $friend->friend_id) }}"><i
                                    class="fas fa-trash red-text"></i></a>
                        @else
                            <a href="{{ route('customer.delete', $friend->friend_id) }}"><i
                                    class="fas fa-undo-alt green-text"></i></a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="mx-2-25">
            <span>Showing 1 - 10 of {{ $incidentReport->customer->friends->count() }} friends</span>
        </div>
    </div>
</div>

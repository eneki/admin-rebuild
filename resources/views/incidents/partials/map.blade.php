<div class="row">
    <div class="col-12 location-card--border">
        <div class="customer-details-title-font mt-3 mb-3">User's Current Location</div>

        <div id="pending_incident_details_table_map" style="width: 100%; height: 300px; margin-bottom: 2rem;"></div>
    </div>
</div>

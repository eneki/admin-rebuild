<div class="row">
    <div class="col-12">
        <div class="col-sm-6 customer-details-title-font mt-3 pb-3">Logs</div>

        @if ($incidentReport->dispatch_comments)
            <div>
                <span class="panel-sub-title pl-3 pb-2">Mode of Dispatch:</span>
                <span class="panel-details pl-3 pb-2">{{ $incidentReport->mode_of_dispatch }}</span>
            </div>
        @endif
        @if ($incidentReport->dispatch_comments)
            <div>
                <span class="panel-sub-title pl-3 pb-2">Dispatch Comments:</span>
                <span class="panel-details pl-3 pb-2">{{ $incidentReport->dispatch_comments }}</span>
            </div>
        @endif
        @if ($incidentReport->officer_id && $incidentReport->status != 'Pending')
            <div>
                <span class="panel-sub-title pl-3 pb-2">Dispatch Officer:</span>
                <a href="{{ route('user.profile', $incidentReport->dispatchOfficer->id) }}">
                    <span class="panel-details pl-3 pb-2 link-color">{{ $incidentReport->dispatchOfficer->fullName }}</span>
                </a>
            </div>
        @endif
        <div>
            <span class="panel-sub-title pl-3 pb-2">Report Status:</span>
            <span class="panel-details pl-3 pb-2">{{ $incidentReport->status }}</span>
        </div>
        @if ($incidentReport->report)
            <div>
                <span class="panel-sub-title pl-3 pb-2">Report:</span>
                <span class="panel-details pl-3 pb-2">{{ $incidentReport->report }}</span>
            </div>
        @endif
        <div>
            <span class="panel-sub-title pl-3 pb-2">Reported on:</span>
            <span class="panel-details pl-3 pb-2">{{ $incidentReport->created_at }}</span>
        </div>
    </div>
</div>
</div>

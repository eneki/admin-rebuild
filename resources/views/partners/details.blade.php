@extends('platform::dashboard')

@section('title', $partner->name)
@section('description', 'Partner Details')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                @include('partners.partials.details-panel')
                @include('partners.partials.bank-details')
                @include('partners.partials.services')
            </div>
            <div class="col-sm-4">
                @include('partners.partials.users')
            </div>
        </div>
    </div>
@stop

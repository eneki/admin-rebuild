@extends('platform::dashboard')

@section('title','Create Partner')

@section('content')
    {!! Form::open(['route' => ['partner.store']]) !!}
    <div class="font-1-25 px-3 mx-5 font-bold">Core Details</div>
    <div class="row px-5 mx-auto mt-3">
        <div class="col">
            <div>
                {!! Form::label('name', 'Partner Name', ['class' => 'font-bold']) !!} <span
                    class="red-text font-1-25">*</span>
            </div>
            <div>
                {!! Form::text('name', null, ['class' => 'w-100 form__input', 'required']) !!}
            </div>
            <div>
                {!! Form::label('postal_address', 'Postal Address', ['class' => 'font-bold']) !!} <span
                    class="red-text font-1-25">*</span>
            </div>
            <div>
                {!! Form::text('postal_address', null, ['class' => 'w-100 form__input', 'required']) !!}
            </div>
            <div>
                {!! Form::label('city', 'City', ['class' => 'font-bold']) !!} <span class="red-text font-1-25">*</span>
            </div>
            <div>
                {!! Form::text('city', null, ['class' => 'w-100 form__input', 'required']) !!}
            </div>
        </div>
        <div class="col">
            <div>
                {!! Form::label('physical_address', 'Physical Address', ['class' => 'font-bold']) !!} <span
                    class="red-text font-1-25">*</span>
            </div>
            <div>
                {!! Form::text('physical_address', null, ['class' => 'w-100 form__input', 'required']) !!}
            </div>
            <div>
                {!! Form::label('postal_code', 'Postal Code', ['class' => 'font-bold']) !!} <span
                    class="red-text font-1-25">*</span>
            </div>
            <div>
                {!! Form::text('postal_code', null, ['class' => 'w-100 form__input', 'required']) !!}
            </div>
            <div>
                {!! Form::label('partner_id', 'Corresponding Premium Partner', ['class' => 'font-bold']) !!} <span
                    class="red-text font-1-25">*</span>
            </div>
            <div>
                {!! Form::select('partner_id', $premiumPartners, null, ['class' => 'custom-select']) !!}
            </div>
        </div>
    </div>
    <hr class="w-95">

    <div class="font-1-25 px-3 mx-5 font-bold">Bank Details</div>
    <div class="row px-5 mx-auto mt-3">
        <div class="col">
            <div>
                {!! Form::label('bank_name', 'Bank Name', ['class' => 'font-bold']) !!}
            </div>
            <div>
                {!! Form::text('bank_name', null, ['class' => 'w-100 form__input']) !!}
            </div>
            <div>
                {!! Form::label('account_number', 'Account Number', ['class' => 'font-bold']) !!}
            </div>
            <div>
                {!! Form::text('account_number', null, ['class' => 'w-100 form__input']) !!}
            </div>
        </div>
        <div class="col">
            <div>
                {!! Form::label('account_name', 'Account Name', ['class' => 'font-bold']) !!}
            </div>
            <div>
                {!! Form::text('account_name', null, ['class' => 'w-100 form__input']) !!}
            </div>
        </div>
    </div>
    <hr class="w-95">

    <div class="font-1-25 px-3 mx-5 font-bold">Contact Person</div>
    <div class="row px-5 mx-auto mt-3">
        <div class="col">
            <div>
                {!! Form::label('firstName', 'First Name', ['class' => 'font-bold']) !!}
            </div>
            <div>
                {!! Form::text('firstName', null, ['class' => 'w-100 form__input']) !!}
            </div>
            <div>
                {!! Form::label('phone', 'Phone', ['class' => 'font-bold']) !!}
            </div>
            <div>
                {!! Form::text('phone', null, ['class' => 'w-100 form__input']) !!}
            </div>
            <div>
                {!! Form::label('email', 'Email', ['class' => 'font-bold']) !!}
            </div>
            <div>
                {!! Form::text('email', null, ['class' => 'w-100 form__input']) !!}
            </div>
        </div>
        <div class="col">
            <div>
                {!! Form::label('lastName', 'Last Name', ['class' => 'font-bold']) !!}
            </div>
            <div>
                {!! Form::text('lastName', null, ['class' => 'w-100 form__input']) !!}
            </div>
            <div>
                {!! Form::label('id_number', 'ID/Passport Number', ['class' => 'font-bold']) !!} 
            </div>
            <div>
                {!! Form::text('id_number', null, ['class' => 'w-100 form__input']) !!}
            </div>
        </div>
    </div>
    <hr class="w-95">

    <div class="font-1-25 px-3 mx-5 font-bold">Services <span class="red-text font-1-25">*</span><span class="font-1-rem">(hold the Shift key to select multiple)</span></div>
    <div class="row px-5 mx-auto mt-3">
        <div class="col">
            {{Form::select('services[]', $services, null, ['multiple' => true, 'class' => 'custom-select'])}}
        </div>
    </div>
    <div class="float-right mr-4-0">
        {!! Form::hidden('status', 1) !!}
        {!! Form::submit('Add Partner', ['class' => 'btn full-page-form__button full-page-form__button--save-color mb-5 px-4']) !!}
    </div>
    {!! Form::close() !!}

@stop

@extends('platform::dashboard')

@section('content')
    <h1 class="ml-5 mb-3 font-thin h3 text-black">Partners</h1>
    <div class="white-background ml-5 mr-5 pb-3 mb-3">
        <a href="{{ route('partner.create') }}">
            <button class="btn filters-dropdown-button ml-3-neg" type="button" data-target="#filterCustomersTable"
                data-toggle="modal">
                Create Partner
            </button>
        </a>

        <div class="table-responsive">
            <table class="table mx-auto mt-5">
                <thead class="table-head-color text-left">
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">TYPE</th>
                        <th scope="col">PARTNER SINCE</th>
                        <th scope="col">STATUS</th>
                        <th scope="col">ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($partners as $partner)
                        <tr>
                            <td>{!! $partner->name !!}</td>
                            <td>{!! $partner->type_title !!}</td>
                            <td>{!! $partner->created_at->diffForHumans() !!}</td>
                            @if ($partner->status == 1)
                                <td>Active</td>
                            @else
                                <td>Inactive</td>
                            @endif
                            <td>
                                <a href="{{ route('partner.details', $partner->id) }}"><i class="fas fa-eye"></i></a>
                                @if ($partner->status == 1)
                                    <a href="{{ route('partner.deactivate', $partner->id) }}"><i
                                            class="fas fa-trash red-text"></i></a>
                                @else
                                    <a href="{{ route('partner.deactivate', $partner->id) }}"><i
                                            class="fas fa-undo-alt green-text"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <span class="ml-5">Showing 1 - 10 of {{ $count }} partners</span>
            {{ $partners->links() }}
        </div>
    </div>
    @stop

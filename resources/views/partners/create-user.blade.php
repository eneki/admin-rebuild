@extends('platform::dashboard')

@section('title', 'Create User')

@section('content')
    <div class="container">

        {!! Form::open(['route' => ['partner.user.store', $partner->id]]) !!}

        <div class="row my-2">
            <div class="col-3">
                {!! Form::label('firstName', 'First Name') !!}
            </div>
            <div class="col-6">
                {!! Form::text('firstName', null, ['placeholder' => 'John', 'class' => 'w-100 form__input', 'required']) !!}
            </div>
        </div>
        <div class="row my-2">
            <div class="col-3">
                {!! Form::label('lastName', 'Last Name') !!}
            </div>
            <div class="col-6">
                {!! Form::text('lastName', null, ['placeholder' => 'Doe', 'class' => 'w-100 form__input', 'required']) !!}
            </div>
        </div>

        <div class="row my-2">
            <div class="col-3">
                {!! Form::label('phone', 'Phone Number') !!}
            </div>
            <div class="col-6">
                {!! Form::text('phone', null, ['placeholder' => '+2547xxxxxxx/07xxxxxxxx', 'class' => 'w-100 form__input', 'required'])
                !!}
            </div>
        </div>

        <div class="row my-2">
            <div class="col-3">
                {!! Form::label('id_number', 'ID/Passport Number') !!}
            </div>
            <div class="col-6">
                {!! Form::text('id_number', null, ['class' => 'w-100 form__input', 'required']) !!}
            </div>
        </div>

        <div class="row my-2">
            <div class="col-3">
                {!! Form::label('password', 'Password') !!}
            </div>
            <div class="col-6">
                {!! Form::password('password', null, ['class' => 'w-100 form__input', 'required']) !!}
            </div>
        </div>

        <div class="row my-2">
            <div class="col-3">
                {!! Form::label('email', 'Email Address') !!}
            </div>
            <div class="col-6">
                {!! Form::text('email', null, ['placeholder' => 'jdoe@email.com', 'class' => 'w-100 form__input', 'required']) !!}
            </div>
        </div>

        <div class="row my-2">
            <div class="col-3">
                {!! Form::label('role') !!}
            </div>
            <div class="col-6">
                {!! Form::select('role', $roles, '', ['class' => 'w-100 custom-select', 'required']) !!}
            </div>
        </div>

        {!! Form::hidden('partner_id', $partner->id) !!}

        <div class="row my-2">
            <div class="col-4 offset-8">
                <a href="javascript:history.back()">
                    <button type="button"
                        class="btn full-page-form__button full-page-form__button--back-border px-4 d-inline-block"
                        data-dismiss="modal">Back</button>
                </a>
                <button type="submit"
                    class="btn full-page-form__button d-inline-block full-page-form__button--save-color px-4">
                    Create User</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop

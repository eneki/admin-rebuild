@extends('platform::dashboard')

@section('title', 'Users')
@section('description', 'belonging to ' . $partner->name)

@section('content')
    <div class="white-background ml-5 mr-5 pb-3 mb-3">
        <div class="table-responsive">
            <table class="table mx-auto mt-5">
                <thead class="table-head-color text-left">
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">CONTACT PHONE</th>
                        <th scope="col">EMAIL ADDRESS</th>
                        <th scope="col">ID/PASSPORT</th>
                        <th scope="col">ROLE</th>
                        <th scope="col">ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($partnerUsers as $user)
                        <tr>
                            <td>{!! $user->full_name !!}</td>
                            <td>{!! $user->phone !!}</td>
                            <td>{!! $user->email !!}</td>
                            <td>{!! $user->id_number !!}</td>
                            <td>{!! $user->role !!}</td>
                            <td>
                                <a href="{{ route('user.profile', $user->id) }}"><i class="fas fa-eye"></i></a>
                                <a href="/partner/{{ $user->id }}/delete"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <span class="ml-5">Showing 1 - 10 of {{ $count }} users</span>
            <span class="float-right">{{ $partnerUsers->links() }}</span>
        </div>
    </div>
@stop

<div>
    <div class="row">
        <div class="col-sm-8 font-1-25">Partner Users</div>
        <div class="col-sm-4 float-right">
            <a class="link-color" href="{{route('partner.user.show', $partner->id)}}">
                <i class="fas fa-plus-circle"></i> Add User
            </a>
        </div>
    </div>
    <div class="mt-4">
        @foreach ($partnerUsers as $user)
            <a href="{{route('user.profile', $user->id)}}">
                <div class="row white-background py-3 border-bottom">
                    <div class="col-sm-3">
                        <img src="{!!  $user->profile_pic !!}" alt="" width="100%" height="100%">
                    </div>
                    <div class="col-sm float-right my-auto">
                        <div>{!! $user->full_name !!}</div>
                        <div>{!! $user->phone !!}</div>
                    </div>
                    <div class="col-sm-3 my-auto">{!! $user->role !!}</div>
                </div>
            </a>
        @endforeach
    </div>
    <div class="text-center my-3">
        <a href="{{route('partner.users', $partner->id)}}" class="font-bold link-color text-center">View
            All Users</a>
    </div>
</div>

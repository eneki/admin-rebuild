<div class="row card__customers--border card__customer-details--margin">
    <div class="col-sm-2">
        <img src="{!!  $partner->image !!}" alt="" width="100%" height="auto">
    </div>
    <div class="col-sm-10 my-auto">
        <div class="row p-3">
            <div class="col-sm-12">
                <div class="panel-title">{{ $partner->name }}</div>
                <div>
                    <span class="panel-sub-title">Member since:</span>
                    <span class="panel-details">{{ $partner->created_at->diffForHumans() }}</span>
                </div>
                <div>
                    <span class="panel-details">{{ $partner->type_title }} partner </span>
                </div>
                <div>
                    <span class="panel-details">{{ $partner->users->count() }} members</span>
                </div>
                <div class="mt-3">
                    <a href="{{ route('partner.deactivate', $partner->id) }}">
                        @if ($partner->status == 1)
                            <button type="button" class="btn btn-danger rounded">Deactivate Partner</button>
                        @else
                            <button type="button" class="btn btn-success rounded">Reactivate Partner</button>
                        @endif
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

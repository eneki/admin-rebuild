<div>
    <div class="row mt-5">
        <div class="col-sm-3 ml-1-2-neg font-1-25 ml-5-neg">Partner Services</div>
{{--        <div class="col-sm-3 offset-sm-6 pl-0 m-l-47-5">--}}
{{--            <a href="{{route('partner.show.configure-services', $partner->id)}}" class="link-color">--}}
{{--                <i class="fas fa-plus-circle"></i> Configure Services--}}
{{--            </a>--}}
{{--        </div>--}}
    </div>
    <div class="mt-4">
        <table class="table ml-3-8-neg mt-3">
            <thead>
            <tr>
                <th scope="col" id="partner-service-heading">Package</th>
                <th scope="col">Service</th>
            </tr>
            </thead>
            <tbody>
            @foreach($partnerPackages as $package)
                <tr>
                    <td class="pl-0-75">{!! $package->name !!}</td>
                    <td>{!! $package->service->name !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="text-center my-3">
        <a href="{{route('partner.packages', $partner->id)}}" class="font-bold link-color text-center">View
            All Packages</a>
    </div>
</div>

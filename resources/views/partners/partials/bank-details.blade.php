<div class="row mt-4">
    <div class="col-sm-2 ml-1-2-neg font-1-25 ml-5-neg">Bank details</div>
    <div class="col-sm-3 offset-sm-7 pl-0">
        <a class="link-color" href="{{route('partner.bank.show', $partner->id)}}">
            <i class="fas fa-pen"></i>Edit Bank details
        </a>
    </div>
</div>

<table class="table table-borderless ml-3-8-neg mt-3">
    <tbody>
    <tr>
        <th scope="row" width="30%">Bank Name</th>
        <td>{{ $partner->bank_name }}</td>
    </tr>
    <tr>
        <th scope="row" width="30%">Account Name</th>
        <td>{{ $partner->account_name }}</td>
    </tr>
    <tr>
        <th scope="row" width="30%">Account Name</th>
        <td>{{ $partner->account_number }}</td>
    </tr>
    </tbody>
</table>

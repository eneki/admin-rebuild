@extends('platform::dashboard')

@section('title', 'Packages')
@section('description', 'belonging to ' . $partner->name)

@section('content')
    <div class="white-background ml-5 mr-5 pb-3 mb-3">
        <div class="table-responsive">
            <table class="table mx-auto mt-5">
                <thead class="table-head-color text-left">
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">TYPE</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($packages as $package)
                        <tr>
                            <td>{!! $package->name !!}</td>
                            <td>{!! $package->service->name !!}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <span class="ml-5">Showing 1 - 10 of {{ $count }} packages</span>
            {{ $packages->links() }}
        </div>
    </div>
    @stop

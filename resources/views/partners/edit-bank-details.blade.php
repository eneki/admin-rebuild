@extends('platform::dashboard')

@section('title', 'Edit Bank Details')

@section('content')
    <div class="container">

        {!! Form::model($partner, ['route' => ['partner.bank.store', $partner->id]]) !!}

        <div class="row my-2">
            <div class="col-2">
                {!! Form::label('bank_name', 'First Name') !!}
            </div>
            <div class="col-6">
                {!! Form::text('bank_name', null, ['placeholder' => 'ABC Bank', 'class' => 'w-100 form__input', 'required'])
                !!}
            </div>
        </div>
        <div class="row my-2">
            <div class="col-2">
                {!! Form::label('account_name', 'Account Name') !!}
            </div>
            <div class="col-6">
                {!! Form::text('account_name', null, ['placeholder' => 'ABCAccount', 'class' => 'w-100 form__input',
                'required']) !!}
            </div>
        </div>
        <div class="row my-2">
            <div class="col-2">
                {!! Form::label('account_number', 'Account Number') !!}
            </div>
            <div class="col-6">
                {!! Form::text('account_number', null, ['class' => 'w-100 form__input', 'required']) !!}
            </div>
        </div>
        <div class="row my-2">
            <div class="col-4 offset-8">
                <a href="javascript:history.back()">
                    <button type="button"
                        class="btn full-page-form__button full-page-form__button--back-border px-4 d-inline-block"
                        data-dismiss="modal">Back</button>
                </a>
                <button type="submit"
                    class="btn full-page-form__button d-inline-block full-page-form__button--save-color px-4">Update
                    Bank Details</button>
            </div>
        </div>
    </div>

    {!! Form::close() !!}

@stop

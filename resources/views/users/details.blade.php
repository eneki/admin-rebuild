@extends('platform::dashboard')

@section('title', $user->fullName)
@section('description', 'User Details')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                @include('users.partials.user-details-panel')
            </div>
            <div class="col-sm-4">
                @include('users.partials.user-useful-links')
            </div>
        </div>
    </div>
@stop

<div class="row border panel-background panel-border mr-6 mt-4">
    <div class="col-sm-4 white-background">
        <img src="{{asset('/img/user_avatar.png')}}" alt="" width="100%" height="100%">
    </div>
    <div class="col-sm-8 my-auto">
        <div class="row p-3">
            <div class="col-sm-12">
                <div class="font-bold font-1-5 mb-1">{{ $user->fullName }}</div>
                @if($user->phone)
                    <div class="mb-1">Phone Number: {{ $user->phone }}</div>
                @endif
                @if($user->email)
                    <div class="mb-1">Email Address: {{ $user->email }}</div>
                @endif
                @if($user->role == 'Contact Person')
                    <div class="mb-1">Role: {{ $user->role }} for {{ $user->partner->name }}</div>
                @else
                    <div class="mb-1">Role: {{ $user->role }}</div>
                @endif
                <div class="mb-1">ID Number/Passport: {{ $user->id_number }}</div>
                <div class="mb-1">Member Since: {{ $user->member_since }}</div>
            </div>
        </div>
    </div>
</div>

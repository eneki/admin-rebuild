<div>
    <div class="row">
        <div class="col-sm-12 font-1-25">Useful Information</div>
        <hr/>
        <ul>
            <li><a href="/users/admins" class="link-color">All Admins</a></li>
            <li><a href="/partner/{{ $user->partner->id }}/details" class="link-color">About {{ $user->partner->name }}</a></li>
        </ul>
    </div>
</div>

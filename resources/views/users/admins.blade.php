@extends('platform::dashboard')

@section('title','System Admins')

@section('content')
    <table class="table table-striped w-95 ml-3 mr-3" id="system-admins-table">
        <thead class="table-head-color text-left">
        <tr>
            <th scope="col">NAME</th>
            <th scope="col">PHONE</th>
            <th scope="col">EMAIL</th>
            <th scope="col">ADMIN SINCE</th>
            <th scope="col">ROLE</th>
            <th scope="col">ACTIONS</th>
        </tr>
        </thead>
    </table>
@stop

@section('pageSpecificScripts')
    <script src="{{ asset('/js/users/system-admins-table.js') }}" type="text/javascript"></script>
@endsection

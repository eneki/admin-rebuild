@extends('platform::dashboard')

@section('content')
    <div class="container">
        <h1 class="ml-1-neg mb-5 mt-1 font-thin h3 text-black print-display-none">Invoices</h1>

        <div class="customer-details-container">
            @include('invoices.partials.invoice-card-header')
            @include('invoices.partials.invoice-card-body')
            @include('invoices.partials.invoice-card-footer')
        </div>

        <div class="print-display-none">
            @include('invoices.partials.subscription-table')
        </div>
    </div>
@stop

@section('pageSpecificScripts')
    <script>
        $(document).ready(function() {
            $('.print-window').click(function() {
                window.print();
            });
        });

    </script>
@stop

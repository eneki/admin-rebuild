<div class="row">
    <div class="col-4">
        <div class="invoice-card__header-title mb-3">Upesy World</div>
        <div class="invoice-card__header-body">
            <div>Kipande Road, Nairobi, Kenya</div>
            <div>P: (254) 721-425662</div>
            <div>sales@upesy.co.ke</div>
        </div>
    </div>
    <div class="col-2 offset-6">
        <div class="invoice-card__header-title mb-3 text-right">Invoice</div>
        <button type="button" class="invoice-card__button w-100 invoice-card__button--print-color">
            <span class="invoice-card__button-text print-window print-display-none">Print</span>
        </button>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-4">
        <div class="invoice-card__header-body">
            <div><strong>Invoice to</strong></div>
        </div>
        <div class="invoice-card__header-body">
            <div>{{ $invoices[0]->partner_name }}</div>
        </div>
        <div class="invoice-card__header-body">
            <div>{{ $invoices[0]->partner_address }}</div>
        </div>
        <div class="invoice-card__header-body">
            <div>{{ $invoices[0]->partner_city }}</div>
        </div>
    </div>
    <div class="col-4 offset-4 text-right">
        <div class="invoice-card__header-body invoice-card__badge-container">
            <div><strong>Order Status: </strong>
                @if ($invoices[0]->invoiced == 0)
                    <span class="invoice-card__badge invoice-card__badge--pending-color">Pending</span>
                @else
                    <span class="invoice-card__badge invoice-card__badge--paid-color">Paid</span>
                @endif
            </div>
        </div>
    </div>
</div>

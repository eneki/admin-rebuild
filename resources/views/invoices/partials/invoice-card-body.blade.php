<div class="table-responsive">
    <table class="table w-100 mx-auto mt-3">
        <thead class="table-head-color text-left">
            <tr>
                <th scope="col">#</th>
                {{-- <th scope="col">PACKAGE</th> --}}
                <th scope="col">DESCRIPTION</th>
                {{-- <th scope="col">SERVICE</th> --}}
                <th scope="col">CUSTOMERS</th>
                <th scope="col">TOTAL PRICE</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($invoices as $invoice)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    {{-- <td>...</td> --}}
                    <td>Premium customers subscribed via {{ $invoice->partner_app }}</td>
                    {{-- <td>...</td> --}}
                    <td>{{ $customerCount }}</td>
                    <td>{{ $invoice->total_amount }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

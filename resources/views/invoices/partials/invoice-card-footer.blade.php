<div class="row">
    <div class="col-4 offset-8 text-right">
        <div class="invoice-card__header-body">
            <div><strong>Sub-total: </strong>...</div>
        </div>
        <div class="invoice-card__header-body">
            <div><strong>Discount: </strong>...</div>
        </div>
        <div class="invoice-card__header-body">
            <div><strong>VAT: </strong>...</div>
        </div>
        <div class="invoice-card__price-text my-3">
            <div>KES: {{ $totalAmount }}</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-2 offset-10">
        <a
            href="{{ url('invoices/partner/' . $invoices[0]->partner_id . '/partner-app/' . $invoices[0]->partner_app_id . '/mail') }}">
            <button type="button" class="w-100 invoice-card__button invoice-card__button--submit-color print-display-none">
                <span class="invoice-card__button-text">Submit</span>
            </button>
        </a>
    </div>
</div>

<div class="table-responsive subscription-invoice-table">
    <div class="row">
        <div class="col-2 offset-10">
            <form name="exportForm" class="mr-4 mb-4" action="/subscription-customers/partner/excel">
                <div>
                    <input type="hidden" name="current_url" id="invoice-partner-and-partner-app-hidden-input-excel" value="" />
                    <button type="submit"
                        class="w-100 invoice-card__button invoice-card__button--submit-color print-display-none">
                        <span class="invoice-card__button-text">Download Excel</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <table class="table" id="subscriptions-table">
        <thead class="table-head-color text-left">
            <tr>
                <th scope="col" class="print-display-none" style="width: 6%">PROFILE PIC</th>
                <th scope="col">NAME</th>
                <th scope="col" class="w-10 print-display-none">PHONE</th>
                <th scope="col" class="print-display-none">ORGANIZATION</th>
                <th scope="col" class="print-display-none">CUSTOMER ID</th>
                <th scope="col">INVOICE NO</th>
                <th scope="col">PACKAGE</th>
                <th scope="col">AMOUNT</th>
                <th scope="col">SUBSCRIPTION DATE</th>
                <th scope="col">EXPIRATION DATE</th>
                <th scope="col">APP</th>
                <th scope="col">PROVIDER</th>
                <th scope="col">SERVICE</th>
                <th scope="col">STATUS</th>
                <th scope="col" class="text-center print-display-none">VIEW</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($customerTransactions as $customerTransaction)
                <tr>
                    <td class="text-center print-display-none"><img src="{!!  $customerTransaction->image_url !!}"
                            alt="" class="large-table__image rounded-circle"></td>
                    <td>{!! $customerTransaction->firstName !!} {!! $customerTransaction->lastName !!}</td>
                    <td class="print-display-none">{!! $customerTransaction->phone !!}</td>
                    <td class="print-display-none">{!! $customerTransaction->organisation !!}</td>
                    <td class="print-display-none">{!! $customerTransaction->id_number !!}</td>
                    <td>{!! $customerTransaction->invoice_number !!}</td>
                    <td>{!! $customerTransaction->duration_name ?? 'Unknown' !!}</td>
                    <td>{!! $customerTransaction->amount !!}</td>
                    <td>{!! $customerTransaction->created_at !!}</td>
                    <td>{!! $customerTransaction->expires_at !!}</td>
                    <td>{!! $customerTransaction->partner_name !!}</td>
                    <td>{!! $customerTransaction->partner_app !!}</td>
                    <td>{!! $customerTransaction->service_name !!}</td>
                    <td>{!! $customerTransaction->status !!}</td>
                    <td class="text-center print-display-none">
                        <a href="{{ url('customers/' . $customerTransaction->id) }}"><i
                                class="fas fa-eye blue-font"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <span class="ml-5">Showing {{ $customerTransactionCount }} of {{ $customerTransactionCount }} customers</span>
    {{-- <span class="ml-5">Showing 1 - 5 of {{ $customerTransactionCount }}
        customers</span> --}}

    {{-- {{ $customerTransactions->links() }} --}}

</div>

<script>
    let query = window.location.href;
    console.log(query)
    document.getElementById('invoice-partner-and-partner-app-hidden-input-excel').value = query
</script>

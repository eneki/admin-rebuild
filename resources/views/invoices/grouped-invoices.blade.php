@extends('platform::dashboard')

@section('content')
    <div class="container">
        <h1 class="ml-1-neg mb-5 mt-1 font-thin h3 text-black">Invoices</h1>

        @foreach ($groupedInvoices as $key => $invoicesPerMonth)
            <div class="customer-details-container mb-3 grouped-invoices-card--box-shadow">
                <div class="row">
                    <div class="col-4">
                        <div class="invoice-card__header-title mb-3">{{ $key }}</div>
                    </div>
                </div>
                <hr>
                <div class="table-responsive">
                    <table class="table w-100 mx-auto mt-3">
                        <thead class="table-head-color text-left">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">PARTNER</th>
                                <th scope="col">DESCRIPTION</th>
                                <th scope="col" class="text-center">CUSTOMERS</th>
                                <th scope="col">TOTAL</th>
                                <th scope="col" class="text-center">PRINT</th>
                                <th scope="col" class="text-center">STATUS</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($invoicesPerMonth as $invoice)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $invoice->partner_name }}</td>
                                    <td>Premium customers subscribed via {{ $invoice->partner_app }}</td>
                                    <td class="text-center">{{ $transactionCount }}</td>
                                    <td>{{ $invoice->total_amount }}</td>
                                    <td class="text-center">
                                        <a
                                            href="{{ url('invoices/partner/' . $invoice->partner_id . '/partner-app/' . $invoice->partner_app_id) }}">
                                            <i class="fas fa-download grouped-invoices-card__table--icon-color"></i>
                                        </a>
                                    </td>
                                    @if ($invoice->invoiced == 0)
                                        <td>
                                            <div class="invoice-card__badge invoice-card__badge--pending-color">Pending
                                            </div>
                                        </td>
                                    @else
                                        <td>
                                            <div class="invoice-card__badge invoice-card__badge--paid-color">Paid</div>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <span class="ml-5">Showing 1 - 5 of {{ $invoicesPerMonth->count() }} transactions</span>

                    {{ \App\Helpers\General\CollectionHelper::paginate($invoicesPerMonth, 5)->links() }}

                </div>
            </div>
            <br>
        @endforeach

        <span class="ml-5">Showing 1 - 5 of {{ $groupedInvoiceCount }} months</span>

        {{ $groupedInvoices->links() }}

    </div>
    </div>
@stop

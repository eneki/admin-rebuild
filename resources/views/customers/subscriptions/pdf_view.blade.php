<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Subscriptions</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  </head>
  <body>
    <table class="table table-bordered">
    <thead>
      <tr class="table-danger">
        <td>Name</td>
        <td>Phone</td>
        <td>organisation</td>
        <td>ID Number</td>
      </tr>
      </thead>
      <tbody>
        @foreach ($employee as $data)
        <tr>
            <td>{{ $data->customer->fullName }}</td>
            <td>{{ $data->customer->phone }}</td>
            <td>{{ $data->customer->organisation }}</td>
            <td>{{ $data->customer->id_number }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>
@extends('platform::dashboard')

@section('content')
    <div class="row">
        <div class="col-4">
            <h1 class="ml-5 mb-3 font-thin h3 text-black">Premium Customers</h1>
        </div>
        <div class="col-6 offset-2 mb-3">
            <form action="/subscriptions/partner" class="ml-5 main-table__search-form">
                <label for="">Search</label>
                <input class="form-control form__table-search--border" type="text" name="searchValue" aria-label="Search">
            </form>
            <button onclick="clearFiltersMainSubscriptionsTable()"
                class="btn reset-button d-inline-block ml-3 main-table__reset-button">Reset</button>
        </div>
    </div>

    <div class="white-background ml-5 mr-5 pb-3 mb-3">
        <div class="dropdown pt-3">
            <i class="fa fa-ellipsis-v table__more-actions-icon" aria-hidden="true" data-toggle="modal"
                data-target="#moreActionsModal"></i>
            @include('customers.subscriptions.partials.more-actions-modal')
            <button class="btn filters-dropdown-button" type="button" data-target="#filterSubscriptionsTable"
                data-toggle="modal">
                Filters
            </button>
            @include('customers.subscriptions.partials.filters-modal')
        </div>

        <div class="table-responsive">
            <table class="table mx-auto mt-5 ml-3 mr-3" id="subscriptions-table" style="width: 150%;">
                <thead class="table-head-color text-left">
                    <tr>
                        <th scope="col" style="width: 6%">PROFILE PIC</th>
                        <th scope="col">NAME</th>
                        <th scope="col" class="w-10">PHONE</th>
                        <th scope="col">ORGANIZATION</th>
                        <th scope="col">CUSTOMER ID</th>
                        <th scope="col">INVOICE NO</th>
                        <th scope="col">PACKAGE</th>
                        <th scope="col">AMOUNT</th>
                        {{-- <th scope="col">DISCOUNT</th> --}}
                        <th scope="col">SUBSCRIPTION DATE</th>
                        <th scope="col">EXPIRATION DATE</th>
                        <th scope="col">APP</th>
                        <th scope="col">PROVIDER</th>
                        <th scope="col">SERVICE</th>
                        <th scope="col">STATUS</th>
                        <th scope="col">VIEW</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($subscriptions as $subscription)
                        <tr>
                            <td class="text-center"><img src="{!!  $subscription->customer->profile_pic !!}" alt=""
                                    class="large-table__image rounded-circle"></td>
                            <td>{!! $subscription->customer->fullName !!}</td>
                            <td>{!! $subscription->customer->phone !!}</td>
                            <td>{!! $subscription->customer->organisation !!}</td>
                            <td>{!! $subscription->customer->id_number !!}</td>
                            <td>{!! $subscription->invoice_number !!}</td>
                            <td>{!! $subscription->packagePricing ? $subscription->packagePricing->duration_name : 'Unknown'
                                !!}</td>
                            <td>{!! $subscription->amount !!}</td>
                            <td>{!! $subscription->subscription_date !!}</td>
                            <td>{!! $subscription->expiration_date !!}</td>
                            <td>{!! $subscription->partner->name !!}</td>
                            <td>{!! $subscription->providing_partner ? $subscription->providing_partner->partner->name :
                                'Unknown' !!}</td>
                            <td>{!! $subscription->service->name !!}</td>
                            <td>{!! $subscription->status !!}</td>
                            <td class="text-center">
                                <a href="{{ url('customers/' . $subscription->customer->id) }}"><i
                                        class="fas fa-eye blue-font"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <span class="ml-5">Showing 1 - 10 of {{ $subscriptionCount }} subscriptions</span>
            {{ $subscriptions->links() }}
        </div>
    </div>
@stop

@section('pageSpecificScripts')

    <script>
        clearFiltersMainSubscriptionsTable = () => {
            let
                href = '/subscriptions/partner',
                url = new URL(window.location.origin),
                newUrl = url.origin + href;

            window.location = newUrl
        }

    </script>

@endsection

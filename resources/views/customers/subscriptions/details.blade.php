@extends('platform::dashboard')

@section('title', $subscriptionCustomer->fullName)
@section('description', 'Subscription Details')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 offset-sm-2">
                @include('customers.subscriptions.partials.subscription-details-panel')
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                @include('customers.subscriptions.partials.active-beneficiaries')
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                @include('customers.subscriptions.partials.pending-beneficiaries')
            </div>
        </div>

    </div>
@stop

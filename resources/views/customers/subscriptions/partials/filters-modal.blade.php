<div class="modal fade" id="filterSubscriptionsTable" tabindex="-1" role="dialog"
    aria-labelledby="filterSubscriptionsTable" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal__close-icon--size" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/subscriptions/partner">
                    <div class="pt-3 pl-5 pb-3">
                        <div>
                            <label for="subscriptions_filter_partner" style="padding-right: 10.2rem;">App</label>
                            <select class="custom-select d-inline-block" id="subscriptions_filter_partner"
                                name="filter_partner">
                                <option value=""></option>
                                @foreach ($partners as $partner)
                                    <option value="{{ $partner->id }}">{{ $partner->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-2">
                            <label for="subscriptions_filter_partner_application"
                                style="padding-right: 8.4rem;">Provider</label>
                            <select class="custom-select d-inline-block" id="subscriptions_filter_partner_application"
                                name="filter_partner_application">
                                <option selected></option>
                                @foreach ($partners as $partner)
                                    <option value="{{ $partner->id }}">{{ $partner->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-2">
                            <label for="subscriptions_filter_service" style="padding-right: 9rem;">Service</label>
                            <select class="custom-select d-inline-block" id="subscriptions_filter_service"
                                name="filter_service">
                                <option selected></option>
                                @foreach ($services as $service)
                                    <option value="{{ $service->id }}">{{ $service->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-2">
                            <label for="subscriptions_filter_package" style="padding-right: 8.6rem;">Package</label>
                            <select class="custom-select d-inline-block" id="subscriptions_filter_package"
                                name="filter_package">
                                <option value=""></option>
                                @foreach ($packagePricingDurationNames as $packagePricingDurationName)
                                    <option value="{{ $packagePricingDurationName->duration_name }}">
                                        {{ $packagePricingDurationName->duration_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-2">
                            <label for="subscriptions_filter_status" style="padding-right: 9.4rem;">Status</label>
                            <select class="custom-select d-inline-block" id="subscriptions_filter_status"
                                name="filter_status">
                                <option value=""></option>
                                @foreach ($statuses as $status)
                                    <option value="{{ $status->status }}">{{ $status->status }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-2">
                            <label for="subscriptions_filter_start_date" style="padding-right: 2.9rem;">Subscription
                                Start
                                Date</label>
                            <input type="date" class="form-control custom-select" placeholder="Start Date"
                                aria-label="Start Date" aria-describedby="basic-addon1" name="filter_start_date"
                                id="subscriptions_filter_start_date">

                        </div>
                        <div class="my-2">
                            <label for="subscriptions_filter_end_date" style="padding-right: 3.2rem;">Subscription End
                                Date</label>
                            <input type="date" class="form-control custom-select" placeholder="End Date"
                                aria-label="End Date" aria-describedby="basic-addon1" name="filter_end_date"
                                id="subscriptions_filter_end_date">

                        </div>
                        <div class="dropdown-filters">
                            <button type="submit" id="subscriptions_filter"
                                class="btn filter-button d-inline-block ml-3"
                                onclick="checkWhetherTheTableContainsSearchResults()">
                                Filter
                            </button>
                            <button id="subscriptions_reset" onclick="clearFilters()"
                                class="btn reset-button d-inline-block ml-3">Reset
                            </button>
                        </div>
                        <input id="subscriptions-search-hidden-input" hidden name="searchValue">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@section('pageSpecificScripts')

    <script>
        function checkWhetherTheTableContainsSearchResults() {
            let search = location.search.substring(1)
            searchInputElement = document.getElementById('subscriptions-search-hidden-input');
            console.log(search)
            if (search != '') {
                searchInputElement.setAttribute('value', search);
            }
        }

        clearFilters = () => {
            let href = '/subscriptions/partner'
            window.href = href
        }

    </script>

@endsection

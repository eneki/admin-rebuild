<div class="modal fade" id="moreActionsModal" tabindex="-1" role="dialog" aria-labelledby="moreActionsModal"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal__close-icon--size" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="exportForm" action="/subscriptions/partner/pdf">
                    <div>
                        <input type="hidden" name="current_url" id="subscription-hidden-input-pdf" value="" />
                        <button class="table__more-actions-button" name="subscriptions-export" type="submit">
                            <i class="fas fa-file-pdf table__more-actions-export-icon table__more-actions-export-pdf-icon--color"></i> Download as <strong>PDF</strong>
                        </button>
                    </div>
                </form>
                <form name="exportForm" action="/subscriptions/partner/excel">
                    <div>
                        <input type="hidden" name="current_url" id="subscription-hidden-input-excel" value="" />
                        <button class="table__more-actions-button" name="subscriptions-export" type="submit">
                            <i class="fas fa-file-excel table__more-actions-export-icon table__more-actions-export-excel-icon--color"></i> Download as <strong>Excel</strong>
                        </button>
                    </div>
                </form>
                <form name="exportForm" action="/subscriptions/partner/excel">
                    <div>
                        <input type="hidden" name="current_url" id="subscription-hidden-input-csv" value="" />
                        <button class="table__more-actions-button" name="subscriptions-export" type="submit">
                            <i class="fas fa-file-csv table__more-actions-export-icon table__more-actions-export-csv-icon--color"></i> Download as <strong>CSV</strong>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- clicking export should send all queries plus an export to the route
--}}

<script>
    let query = window.location.search.substring(1);
    document.getElementById('subscription-hidden-input-pdf').value = query
    document.getElementById('subscription-hidden-input-excel').value = query
    document.getElementById('subscription-hidden-input-csv').value = query
</script>

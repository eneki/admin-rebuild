<div class="row mt-4">
    <div class="col-sm-6 ml-1-2-neg font-1-25 ml-5-neg">Active Beneficiaries</div>
</div>

<table class="table table-striped w-100 mx-auto mt-3">
    <thead class="thead-dark">
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Phone</th>
        <th scope="col">Email Address</th>
    </tr>
    </thead>
    <tbody>
    @foreach($activeBeneficiaries as $activeBeneficiary)
        <tr>
            <td>{!! $activeBeneficiary->beneficiary->fullName !!}</td>
            <td>{!! $activeBeneficiary->beneficiary->phone !!}</td>
            <td>{!! $activeBeneficiary->beneficiary->email !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="mx-2-25">
    <span>Showing 1 - 5 of {{ $activeBeneficiariesCount }} guardians</span>
    <span class="float-right">{{ $activeBeneficiaries->links() }}</span>
</div>

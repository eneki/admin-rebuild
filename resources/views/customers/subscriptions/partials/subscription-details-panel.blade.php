<div class="row p-3 border panel-background panel-border mr-6 mt-4">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-8">
                <div class="font-bold font-1-5 mb-1">Subscription Details</div>
            </div>
            <div class="col-sm-3 offset-sm-1">
                <a data-toggle="modal" data-target="#assignProvider" onclick="openAssignModal('{{ $subscription->id }}')" class="link-color green-text assignButton">
                    <button type="button" class="btn btn-success">Assign</button>
                </a>

                @include('customers.partials.assign-provider')
            </div>
        </div>
        @if($subscription->customer->phone)
            <div class="mb-1">Phone Number: {{ $subscription->customer->phone }}</div>
        @endif
        @if($subscription->customer->email)
            <div class="mb-1">Email Address: {{ $subscription->customer->email }}</div>
        @endif
        <div class="mb-1">Package: {{ $subscription->packagePricing ? $subscription->packagePricing->package->name : '' }}</div>
        <div class="mb-1">Days Valid: {{ $subscription->packagePricing ? $subscription->packagePricing->days : '' }}</div>
        <div class="mb-1">Subscription Date: {{ $subscription->subscription_date }}</div>
        <div class="mb-1">Expiry Date: {{ $subscription->expiration_date }}</div>
        <div class="mb-1">Status: {{ $subscription->status }}</div>
    </div>
</div>

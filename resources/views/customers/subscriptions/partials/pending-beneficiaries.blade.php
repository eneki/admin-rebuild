<div class="row mt-4">
    <div class="col-sm-6 ml-1-2-neg font-1-25 ml-5-neg">Pending Beneficiaries</div>
</div>

<table class="table table-striped w-100 mx-auto mt-3">
    <thead class="thead-dark">
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Phone</th>
        <th scope="col">Date Invited</th>
    </tr>
    </thead>
    <tbody>
    @foreach($pendingBeneficiaries as $pendingBeneficiary)
        <tr>
            <td>{!! $pendingBeneficiary->name !!}</td>
            <td>{!! $pendingBeneficiary->phone !!}</td>
            <td>{!! $pendingBeneficiary->date_invited !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="mx-2-25">
    <span>Showing 1 - 5 of {{ $pendingBeneficiariesCount }} guardians</span>
    <span class="float-right">{{ $pendingBeneficiaries->links() }}</span>
</div>

@extends('platform::dashboard')

@section('content')
    <div class="container">
        <h1 class="ml-1-neg mb-5 mt-1 font-thin h3 text-black">Customer Details</h1>

        <div class="customer-details-container">
            <div class="container mb-5">
                <div class="row">
                    <div class="col-sm-6">
                        @include('customers.partials.customer-details-panel')
                    </div>
                    @if (!empty($customer->residences->toArray()))
                        <div class="col-sm-6">
                            @include('customers.partials.customer-residence-details-panel')
                        </div>
                    @endif
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-sm-12 card__customer-detail-panel--border">
                    @include('customers.partials.subscriptions')
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-sm-12 card__customer-detail-panel--border">
                    @include('customers.partials.guardians')
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 card__customer-detail-panel--border">
                    @include('customers.partials.dependants')
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-sm-12 card__customer-detail-panel--border">
                    @include('customers.partials.beneficiaries')
                </div>
            </div> --}}
            <div class="row">
                <div class="col-sm-12 card__customer-detail-panel--border">
                    @include('customers.partials.alert-history')
                </div>
            </div>

            @include('customers.partials.assign-provider')
        </div>
    </div>
@stop

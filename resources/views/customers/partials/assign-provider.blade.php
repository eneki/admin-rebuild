<div class="modal fade" id="assignProvider" tabindex="-1" role="dialog" aria-labelledby="assignProvider"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal-icon" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title font-1-25  mb-2" id="assignProvider">Assign provider to</h5>

                <form method="post" id="assignForm">
                    @csrf
                    <div class="form-group">
                        <select name="provider" class="custom-select" id="inputGroupSelect01">
                            <option selected>Select Provider</option>
                            @foreach ($subscriptionProviders as $subscriptionProvider)
                                <option value="{{ $subscriptionProvider->id }}">{{ $subscriptionProvider->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Create Plan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    function openAssignModal(e) {
        let url = "/subscriptions/assign-provider/" + e;
        document.getElementById('assignForm').action = url;  //The id where to pass the value
    }
</script>

<div class="row mt-4">
    <div class="col-sm-6 customer-details-title-font mt-3 ml-3">Dependants</div>
    <div class="col-sm-5 form__table-search--margin">
        <form class="form my-2 my-lg-0 float-right" action='/customers/{{ $customer->id }}'>
            <label for="">Search</label>
            <input class="form-control form__table-search--border" type="text" name="beneficiariesSearchValue" aria-label="Search">
        </form>
    </div>
    <button onclick="clearFilters({{ $customer->id }})"
            class="btn reset-button d-inline-block ml-3 customer-details-page__reset-button">Reset</button>
</div>

<div class="table-responsive">

    <table class="table table-striped w-100 mx-auto mt-3">
        <thead class="table-head-color text-left">
            <tr>
                <th scope="col" class="w-20">NAME</th>
                <th scope="col" class="w-15">PHONE</th>
                <th scope="col" class="w-15">MEMBER SINCE</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($beneficiaries as $beneficiary)
                <tr>
                    <td>{!! $beneficiary->image_url !!}</td>
                    <td>{!! $beneficiary->fullName !!}</td>
                    <td>{!! $beneficiary->phone !!}</td>
                    <td>{!! $beneficiary->member_since !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

{{ $beneficiaries->links() }}

<script>
    clearFilters = (id) => {
        let 
        href = '/customers/' + id, 
        url = new URL(window.location.origin),
        newUrl = url.origin + href;

        window.location = newUrl;
    }
</script>
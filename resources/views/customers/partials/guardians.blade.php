<div class="row mt-4">
    <div class="col-sm-6 customer-details-title-font mt-5 ml-3">Guardians (Friends, Relatives, etc.)</div>
    <div class="col-sm-5 form__table-search--margin">
        <form class="form my-2 my-lg-0 float-right" action='/customers/{{ $customer->id }}'>
            <label for="">Search</label>
            <input class="form-control form__table-search--border" type="text" name="guardianSearchValue"
                aria-label="Search">
        </form>
    </div>
    <button onclick="clearFilters({{ $customer->id }})"
        class="btn reset-button d-inline-block ml-3 customer-details-page__reset-button">Reset</button>
</div>

<div class="table-responsive">
    <table class="table w-100 mx-auto mt-3">
        <thead class="table-head-color text-left">
            <tr>
                <th scope="col" class="w-10">PROFILE PIC</th>
                <th scope="col">NAME</th>
                <th scope="col">PHONE</th>
                <th scope="col">EMAIL</th>
                <th scope="col">MEMBER SINCE</th>
                <th scope="col" class="text-center">VIEW PROFILE</th>
                <th scope="col" class="text-center">DELINK GUARDIAN</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($guardians as $guardian)
                @if ($guardian->guardian)
                    <tr>
                        <td class="text-center"><img src="{!!  $guardian->guardian->profile_pic !!}" alt="" width="50%"
                                class="rounded-circle guardian-table__image"></td>
                        <td>{!! $guardian->guardian->fullName !!}</td>
                        <td>{!! $guardian->guardian->phone !!}</td>
                        <td>{!! $guardian->guardian->email !!}</td>
                        <td>{!! $guardian->guardian->member_since !!}</td>
                        <td class="text-center">
                            <a href="{{ route('customers.details', $guardian->guardian->id) }}"><i
                                    class="fas fa-eye blue-font"></i></a>
                        </td>
                        <td class="text-center">
                            <a data-toggle="modal" data-target="#deleteGuardian" class="link-color">
                                <i class="fas fa-trash red-text"></i>
                            </a>
                            @include('customers.partials.delete-guardian')
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>

{{ $guardians->links() }}

<script>
    clearFilters = (id) => {
        let
            href = '/customers/' + id,
            url = new URL(window.location.origin),
            newUrl = url.origin + href;

        window.location = newUrl
    }

</script>

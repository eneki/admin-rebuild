<div class="modal fade" id="filterCustomersTable" tabindex="-1" role="dialog" aria-labelledby="filterCustomersTable"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal__close-icon--size" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/customers/partner">
                    <div class="pt-3 pl-5 pb-3">
                        <div>
                            <label for="customers_filter_partner_application" style="padding-right: 5rem;">Partner
                                Application</label>
                            <select class="custom-select d-inline-block" id="customers_filter_partner_application"
                                name="filter_partner_application">
                                <option value="">Select</option>
                                @foreach ($partnerApplications as $partnerApplication)
                                    <option value="{{ $partnerApplication->id }}">{{ $partnerApplication->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-2">
                            <label for="customers_filter_start_date" style="padding-right: 3.58rem;">Membership Start
                                Date</label>
                            <input type="date" class="form-control custom-select" placeholder="Start Date"
                                aria-label="Start Date" aria-describedby="basic-addon1" name="filter_start_date"
                                id="customers_filter_start_date">
                        </div>
                        <div class="my-2">
                            <label for="customers_filter_start_date" style="padding-right: 4rem;">Membership End
                                Date</label>
                            <input type="date" class="form-control custom-select" placeholder="End Date"
                                aria-label="End Date" aria-describedby="basic-addon1" name="filter_end_date"
                                id="customers_filter_end_date">
                        </div>
                        <div class="dropdown-filters">
                            <button type="submit" id="customers_filter"
                                onclick="checkWhetherTheTableContainsSearchResults()"
                                class="btn filter-button d-inline-block ml-3">
                                Filter
                            </button>
                            <button id="customers_reset" onclick="clearFilters()"
                                class="btn reset-button d-inline-block ml-3">Reset
                            </button>
                        </div>
                        <input id="customers-search-hidden-input" hidden name="searchValue">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@section('pageSpecificScripts')

    <script>
        function checkWhetherTheTableContainsSearchResults() {
            let search = location.search.substring(1)
            searchInputElement = document.getElementById('customers-search-hidden-input');

            if (search != '') {
                searchInputElement.setAttribute('value', search);
            }
        }

        clearFilters = () => {
            let href = '/subscriptions/partner'
            window.href = href
        }

    </script>

@endsection

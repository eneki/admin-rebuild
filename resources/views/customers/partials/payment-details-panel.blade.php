<div class="row mt-4">
    <div class="col-sm-6 ml-1-2-neg font-1-25 ml-5-neg">Payment details</div>
</div>

<div class="row">
    <div class="col-sm-6">
        <table class="table table-borderless ml-3-8-neg mt-3">
            <tbody>
            <tr>
                <th scope="row" width="30%">Amount</th>
                <td>{{ $lastPaymentDetails->amount }}</td>
            </tr>
            <tr>
                <th scope="row" width="30%">Receipt Number</th>
                <td>{{ $lastPaymentDetails->receipt_number }}</td>
            </tr>
            <tr>
                <th scope="row" width="30%">Payment Date</th>
                <td>{{ $lastPaymentDetails->modified_payment_date }}</td>
            </tr>
            <tr>
                <th scope="row" width="30%">Description</th>
                <td>{{ $lastPaymentDetails->description }}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-sm-6">
        <table class="table table-borderless ml-3-8-neg mt-3">
            <tbody>
            <tr>
                <th scope="row" width="30%">Invoice Number</th>
                <td>{{ $lastPaymentDetails->invoice_number }}</td>
            </tr>
            <tr>
                <th scope="row" width="30%">Invoice Date</th>
                <td>{{ $lastPaymentDetails->modified_invoice_date }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


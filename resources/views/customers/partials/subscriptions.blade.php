<div class="row mt-4">
    <div class="col-sm-6 customer-details-title-font mt-5 ml-3">Subscription Details</div>
    <div class="col-sm-5 form__table-search--margin">
        <form class="form my-2 my-lg-0 float-right" action='/customers/{{ $customer->id }}'>
            <label for="">Search</label>
            <input class="form-control form__table-search--border" type="text" name="subscriptionsSearchValue" aria-label="Search">
        </form>
    </div>
    <button onclick="clearFilters({{ $customer->id }})" class="btn reset-button d-inline-block ml-3 customer-details-page__reset-button">Reset</button>
</div>

<div class="table-responsive">

    <table class="table w-100 mx-auto mt-3">
        <thead class="table-head-color text-left">
            <tr>
                <th scope="col">STATUS</th>
                <th scope="col" class="w-15">PROVIDER</th>
                <th scope="col">PERIOD</th>
                <th scope="col">SERVICE</th>
                <th scope="col" class="w-15">SUBSCRIBED ON</th>
                <th scope="col" class="w-20">EXPIRY DATE</th>
                <th scope="col">APP</th>
                <th scope="col">AMOUNT</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($subscriptions as $subscription)
                <tr>
                    @if ($subscription->status == 'active')
                        <td class='table__solved-status-column--color table__status-column--uc-transform'>{!!
                            $subscription->status !!}</td>
                    @elseif ($subscription->status == 'expired')
                        <td class='table__pending-status-column--color table__status-column--uc-transform'>{!!
                            $subscription->status !!}</td>
                    @elseif ($subscription->status == 'pending')
                        <td class='table__unsolved-status-column--color table__status-column--uc-transform'>{!!
                            $subscription->status !!}</td>
                    @else
                        <td class='table__false-alarm-status-column--color table__status-column--uc-transform'> {!! $subscription->status !!}</td>
                    @endif
                    <td>{!! $subscription->providing_partner ? $subscription->providing_partner->partner_name : 'Unknown' !!}</td>
                    <td>{!! $subscription->packagePricing ? $subscription->packagePricing->duration_name : 'Unknown' !!}</td>
                    <td>{!! $subscription->service->name !!}</td>
                    <td>{!! $subscription->subscription_date !!}</td>
                    <td>{!! $subscription->expiration_date !!}</td>
                    <td>{!! $subscription->partner->name !!}</td>
                    <td>{!! $subscription->packagePricing ? $subscription->packagePricing->amount : 'Unknown' !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

{{ $subscriptions->links() }}

<script>
    clearFilters = (id) => {
        let 
        href = '/customers/' + id, 
        url = new URL(window.location.origin),
        newUrl = url.origin + href;

        window.location = newUrl
    }
</script>
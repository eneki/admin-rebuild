<div class="modal fade" id="residenceLocation" tabindex="-1" role="dialog" aria-labelledby="residenceLocation"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal__close-icon--size" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="customer-details-title-font mt-3 mb-4 mx-auto">Customer's Residence's Location</div>

            <div class="modal-body">
                <div id="residence_location_map" class="mx-auto mb-5" style="width: 75%; height: 300px;"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filterAlertHistoryTable" tabindex="-1" role="dialog"
    aria-labelledby="filterAlertHistoryTable" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal__close-icon--size" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/customers/{{ $customer->id }}">
                    @csrf
                    <div class="pt-3 pl-5 pb-3">
                        <div class="my-2">
                            <label for="filter_service" style="padding-right: 9rem;">Service</label>
                            <select class="custom-select d-inline-block" name="filter_service">
                                <option selected></option>
                                @foreach ($services as $service)
                                    <option value="{{ $service->id }}">{{ $service->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-2">
                            <label for="filter_status" style="padding-right: 9.4rem;">Status</label>
                            <select class="custom-select d-inline-block" name="filter_status">
                                <option value=""></option>
                                @foreach ($statuses as $status)
                                    <option value="{{ $status->status }}">{{ $status->status }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-2">
                            <label for="filter_start_date" style="padding-right: 2.9rem;">Subscription Start Date</label>
                            <input type="date" class="form-control custom-select" placeholder="Start Date"
                                aria-label="Start Date" aria-describedby="basic-addon1"
                                name="filter_start_date">

                        </div>
                        <div class="my-2">
                            <label for="filter_end_date" style="padding-right: 3.2rem;">Subscription End Date</label>
                            <input type="date" class="form-control custom-select" placeholder="End Date"
                                aria-label="End Date" aria-describedby="basic-addon1"
                                name="filter_end_date">

                        </div>
                        <div class="dropdown-filters">
                            <button type="button" class="btn reset-button d-inline-block ml-3">Reset</button>
                            <button type="submit" class="btn filter-button d-inline-block ml-3">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteGuardian" tabindex="-1" role="dialog"
     aria-labelledby="deleteGuardian" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal-icon" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title font-1-25  mb-2" id="deleteGuardian">Delink Guardian</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delink this guardian?</p>
            </div>
            <div class="modal-footer">
                <a href="{{ route('customers.guardian.delete', $guardian->guardian->id) }}" class="btn btn-secondary button-color border-0 text-light">Delink</a>
            </div>
        </div>
    </div>
</div>

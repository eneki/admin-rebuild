<div class="row mt-4">
    <div class="col-sm-6 customer-details-title-font mt-5 ml-3">Alert History</div>
    <div class="col-sm-5 form__table-search--margin">
        
        <form class="form my-2 my-lg-0 float-right" action='/customers/{{ $customer->id }}'>
            <label for="">Search</label>
            <input class="form-control form__table-search--border" type="text" name="alertHistorySearchValue"
                aria-label="Search">
        </form>
    </div>
    <div class="customer-details-page__alert-history-div--position">
        <button class="btn customer-details-page__filter-button" type="button" data-target="#filterAlertHistoryTable"
            data-toggle="modal">
            Filters
        </button>
        @include('customers.partials.filters-modal')

        <button onclick="clearFilters({{ $customer->id }})"
            class="btn reset-button d-inline-block ml-3">Reset</button>
    </div>

</div>

<div class="table-responsive">
    <table class="table w-100 mx-auto mt-3">
        <thead class="table-head-color text-left">
            <tr>
                <th scope="col">TYPE</th>
                <th scope="col">REPORTED ON</th>
                <th scope="col">STATUS</th>
                <th scope="col" class="text-center">VIEW</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($reportedIncidents as $reportedIncident)
                <tr>
                    <td>{!! $reportedIncident->incident->name !!}
                    </td>
                    <td>{!! $reportedIncident->reported_on !!}</td>
                    @if ($reportedIncident->status == 'Solved')
                        <td class="table__solved-status-column--color">{!! $reportedIncident->status !!}</td>
                    @elseif ($reportedIncident->status == 'Unsolved')
                        <td class="table__unsolved-status-column--color">{!! $reportedIncident->status !!}</td>
                    @elseif ($reportedIncident->status == 'False Alarm')
                        <td class="table__false-alarm-status-column--color">{!! $reportedIncident->status !!}</td>
                    @else
                        <td class="table__pending-status-column--color">{!! $reportedIncident->status !!}</td>
                    @endif
                    @if ($reportedIncident->status == 'Solved')
                        <td class="text-center">
                            <a
                                href="{{ url('pending-incidents/' . $reportedIncident->id . '/report/customer/' . $customer->id) }}"><i
                                    class="fas fa-eye blue-font"></i></a>
                        </td>
                    @else
                        <td class="text-center">
                            <a
                                href="{{ url('dispatched-and-resolved-incidents/' . $reportedIncident->id . '/report/customer/' . $customer->id) }}"><i
                                    class="fas fa-eye blue-font"></i></a>
                        </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

{{ $reportedIncidents->links() }}

<script>
    clearFilters = (id) => {
        let
            href = '/customers/' + id,
            url = new URL(window.location.origin),
            newUrl = url.origin + href;

        window.location = newUrl
    }

</script>

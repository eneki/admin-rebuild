<div class="row card__customers--border card__customer-details--margin">
    <div class="row">
        <div class="col-sm-2">
            <img src="{!! $customer->profile_pic !!}" alt="" width="100%" class="ml-3 mt-3 details-page-image">
        </div>
        <div class="col-sm-10 my-auto">
            <div class="row p-3">
                <div class="col-sm-12">
                    <div class="panel-title">
                        <a href="{{ route('customers.details', $customer->id) }}" class="link-color">{{ $customer->fullName }}</a>
                    </div>
                    @if ($customer->phone)
                        <div>
                            <span class="panel-sub-title">Phone Number:</span>
                            <span class="panel-details">{{ $customer->phone }}</span>
                        </div>
                    @endif
                    @if ($customer->email)
                        <div>
                            <span class="panel-sub-title">Email Address:</span>
                            <span class="panel-details"> {{ $customer->email }}</span>
                        </div>
                    @endif
                    <div>
                        <span class="panel-sub-title">ID Number:</span>
                        <span class="panel-details">{{ $customer->id_number }}</span>
                    </div>
                    <div>
                        <span class="panel-sub-title">Age:</span>
                        <span class="panel-details">{{ $customer->age }}</span>
                    </div>
                    <div>
                        <span class="panel-sub-title">Gender:</span>
                        <span class="panel-details">
                            @if ($customer->gender == 'M')
                                Male
                            @elseif($customer->gender == 'F')
                                Female
                            @else
                                Unknown
                            @endif
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row lower-panel-customer-details-section">
        <div class="col ml-5 mt-3 mb-3">
            <div>
                @if ($currentModeOfPayment)
                    <span class="panel-sub-title">Current mode of payment:</span>
                    <span class="panel-details text-capitalize">{{ $currentModeOfPayment->paymentMethod->name }}</span>
                @endif
            </div>
            <div>
                <span class="panel-sub-title">Organization:</span>
                <span class="panel-details text-capitalize">{{ $customer->organisation }}</span>
            </div>
            <div>
                <span class="panel-sub-title">Dependency:</span>
                <span class="panel-details text-capitalize"></span>
            </div>
        </div>
    </div>
</div>

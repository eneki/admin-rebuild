<div class="row card__customers--border card__customer-residence-details--margin card__customer-residence--height">
    <div class="col my-auto">
        <div class="row p-3">
            <div class="col-sm-12 card__customer-residence-text--margin">
                <div class="panel-title">Residence</div>
                    <div>
                        <span class="panel-sub-title">Physical Address:</span>
                        <span class="panel-details">{{ $customer->residences[0]->address }}</span>
                    </div>
                    <div>
                        <span class="panel-sub-title">Estate:</span>
                        <span class="panel-details"> {{ $customer->residences[0]->estate_name }}</span>
                    </div>
                    <div>
                        <span class="panel-sub-title">Name:</span>
                        <span class="panel-details"> {{ $customer->residences[0]->name }}</span>
                    </div>
                    <div>
                        <span class="panel-sub-title">House Number:</span>
                        <span class="panel-details">{{ $customer->residences[0]->house_number }}</span>
                    </div>
                    <div>
                        <span class="panel-sub-title">Floor Number:</span>
                        <span class="panel-details">{{ $customer->residences[0]->floor_number }}</span>
                    </div>
                    <div>
                        <span class="panel-sub-title">Code:</span>
                        <span class="panel-details">
                            @if ($customer->residences[0]->floor_number == 'work')
                                Work
                            @elseif ($customer->residences[0]->floor_number == 'home')
                                Home
                            @else
                                Other
                            @endif
                        </span>
                        <button class="btn customer-card__view-map-button float-right" type="button"
                            data-target="#residenceLocation" data-toggle="modal"
                            onclick="initialize({{ $customer->residences[0]->lat }}, {{ $customer->residences[0]->long }})">
                            View Map
                        </button>
                    </div>
                    @include('customers.partials.residence-location')
            </div>
        </div>
    </div>
</div>


<script>
    function initialize(lat, long) {
        var latlng = new google.maps.LatLng(lat, long);
        var map = new google.maps.Map(document.getElementById('residence_location_map'), {
            center: latlng,
            zoom: 20
        });
        var marker = new google.maps.Marker({
            map: map,
            position: latlng,
            draggable: false,
            anchorPoint: new google.maps.Point(0, -29)
        });
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marker, 'click', function() {
            var iwContent = '<div id="iw_container">' +
                '<div class="iw_title"><b>Location</b> : Noida</div></div>';
            // including content to the infowindow
            infowindow.setContent(iwContent);
            // opening the infowindow in the current map and at the current marker location
            infowindow.open(map, marker);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>

@extends('platform::dashboard')

@section('content')
    <div class="row">
        <div class="col-4">
            <h1 class="ml-5 mb-3 font-thin h3 text-black">Customers</h1>
        </div>
        <div class="col-6 offset-2  mb-3">
            <form action="/customers/partner" class="ml-5 main-table__search-form">
                <label for="">Search</label>
                <input class="form-control form__table-search--border" type="text" name="searchValue" aria-label="Search">
            </form>
            <button onclick="clearFiltersMainCustomersTable()"
                class="btn reset-button d-inline-block ml-3 main-table__reset-button">Reset</button>
        </div>
    </div>

    <div class="white-background ml-5 mr-5 pb-3 mb-3">
        <div class="dropdown pt-3">
            <i class="fa fa-ellipsis-v table__more-actions-icon" aria-hidden="true" data-toggle="modal"
                data-target="#moreActionsModal"></i>
            @include('customers.partials.more-actions-modal')
            <button class="btn filters-dropdown-button" type="button" data-target="#filterCustomersTable"
                data-toggle="modal">
                Filters
            </button>
        </div>
        @include('customers.partials.customers-table-filters-modal')

        <div class="table-responsive">
            <table class="table mx-auto mt-5">
                <thead class="table-head-color text-left">
                    <tr>
                        <th scope="col" class="w-10">PROFILE PIC</th>
                        <th scope="col">NAME</th>
                        <th scope="col" class="w-10">PHONE</th>
                        <th scope="col">EMAIL</th>
                        <th scope="col">ORGANIZATION</th>
                        <th scope="col">MEMBER SINCE</th>
                        <th scope="col">PARTNER APPLICATION</th>
                        {{-- <th scope="col">View on Map</th> --}}
                        <th scope="col" class="text-center">VIEW PROFILE</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($customers as $customer)
                        <tr>
                            <td class="text-center"><img src="{!!  $customer->profile_pic !!}" alt="" width="50%"
                                    class="rounded-circle small-table__image"> </td>
                            <td>{!! $customer->fullName !!}</td>
                            <td>{!! $customer->phone !!}</td>
                            <td>{!! $customer->email !!}</td>
                            <td>{!! $customer->organisation !!}</td>
                            <td>{!! $customer->member_since !!}</td>
                            <td>{!! $customer->partner->name !!}</td>
                            <td class="text-center">
                                <a href="{{ url('customers/' . $customer->id) }}"><i class="fas fa-eye blue-font"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <span class="ml-5">Showing 1 - 10 of {{ $customerCount }} customers</span>

            {{ $customers->links() }}
        </div>
    </div>
@stop

@section('pageSpecificScripts')

    <script>
        clearFiltersMainCustomersTable = () => {
            let
                href = '/customers/partner',
                url = new URL(window.location.origin),
                newUrl = url.origin + href;

            window.location = newUrl
        }

    </script>

@endsection

@extends('platform::app')

@section('body-left')
    <div style="position: fixed;">
        <div class="d-sm-flex d-md-block p-3 mt-md-4 w-100 v-center">
            <div class="left-panel-logo">Upesy</div>
        </div>

        <nav class="collapse d-md-block w-100 mb-md-5" id="headerMenuCollapse">

            @include('platform::partials.search')

            @includeWhen(Auth::check(), 'platform::partials.profile')

            <ul class="nav flex-column m-b">
                {!! Dashboard::menu()->render('Main') !!}
            </ul>

        </nav>

        <div class="h-100 w-100 position-relative to-top cursor mt-md-5 divider"
            data-action="click->layouts--html-load#goToTop" title="{{ __('Go to top') }}">
            <div class="bottom-left w-100 mb-2 pl-3">
                <small><i class="icon-arrow-up mr-2"></i> {{ __('Go to top') }}</small>
            </div>
        </div>

        <div class="p-3 m-b m-t d-none d-lg-block w-100">
            @includeFirst([config('platform.template.footer'), 'platform::footer'])
        </div>
    </div>
@endsection

@section('body-right')
    <div class="custom-navbar print-display-none">
        <div class="d-sm-flex flex-row flex-wrap text-center text-sm-left align-items-center custom-navbar-contents">
            <div class="p-3 v-center mt-0-75-neg">
                <div class="mr-3">
                    @include('platform::partials.notificationProfile')
                </div>

                <div class="dropdown col no-padder">
                    <a href="#" class="nav-link p-0 v-center" data-toggle="dropdown">
                        <span style="width:auto;font-size: 1rem;">
                            <span class="text-ellipsis"><span
                                    class="border-left pl-3">{{ Auth::user()->presenter()->title() }} </span><i
                                    class="fas fa-chevron-up mr-3 ml-3"></i></span>
                        </span>
                        <span class="thumb-sm avatar mr-3">
                            <img src="{{ Auth::user()->presenter()->image() }}" class="b b-dark bg-light">
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-left dropdown-menu-arrow bg-white">

                        {!! Dashboard::menu()->render('Profile', 'platform::partials.dropdownMenu') !!}

                        <a href="#" class="dropdown-item">
                            <strong>Status: </strong> Online
                        </a>
                        <a href="#" class="dropdown-item">
                            <strong>Role: </strong> {{ Auth::user()->presenter()->subTitle() }}
                        </a>
                        @if (Dashboard::menu()
            ->container->where('location', 'Profile')
            ->isNotEmpty())
                            <div class="dropdown-divider"></div>
                        @endif

                        @if (\Illuminate\Support\Facades\Auth::user()->role == 'Admin' || \Illuminate\Support\Facades\Auth::user()->role == 'Super Admin')
                            <a href="{{ route('platform.systems.index') }}" class="dropdown-item">
                                <i class="icon-settings mr-2" aria-hidden="true"></i>
                                <span>{{ __('My Users (admin only)') }}</span>
                            </a>
                        @endif

                        @if (config('app.env') == 'production')
                            <a href="{{ route('platform.logout') }}" class="dropdown-item" data-controller="layouts--form"
                                data-action="layouts--form#submitByForm" data-layouts--form-id="logout-form"
                                dusk="logout-button">
                                <i class="icon-logout mr-2" aria-hidden="true"></i>
                                <span>{{ __('Sign out') }}</span>
                            </a>
                            <form id="logout-form" class="hidden" action="{{ route('platform.logout') }}" method="POST"
                                data-controller="layouts--form" data-action="layouts--form#submit">
                                @csrf
                            </form>
                        @else
                            @if (\Orchid\Access\UserSwitch::isSwitch())
                                <a href="#" class="dropdown-item" data-controller="layouts--form"
                                    data-action="layouts--form#submitByForm" data-layouts--form-id="return-original-user">
                                    <i class="icon-logout mr-2" aria-hidden="true"></i>
                                    <span>{{ __('Back to my account') }}</span>
                                </a>
                                <form id="return-original-user" class="hidden" data-controller="layouts--form"
                                    data-action="layouts--form#submit" action="{{ route('platform.switch.logout') }}"
                                    method="POST">
                                    @csrf
                                </form>
                            @else
                                <a href="{{ route('platform.logout') }}" class="dropdown-item"
                                    data-controller="layouts--form" data-action="layouts--form#submitByForm"
                                    data-layouts--form-id="logout-form" dusk="logout-button">
                                    <i class="icon-logout mr-2" aria-hidden="true"></i>
                                    <span>{{ __('Sign out') }}</span>
                                </a>
                                <form id="logout-form" class="hidden" action="{{ route('platform.logout') }}" method="POST"
                                    data-controller="layouts--form" data-action="layouts--form#submit">
                                    @csrf
                                </form>
                            @endif
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>

<div class="p-3 mt-md-4 @hasSection('navbar') @else d-none d-md-block          @endif">
<div class="v-md-center">
    <div class="d-none d-md-block col-xs-12 col-md no-padder">
        <h1 class="m-n font-thin h3 text-black">@yield('title')</h1>
        <small class="text-muted" title="@yield('description')">@yield('description')</small>
    </div>
    <div class="col-xs-12 col-md-auto ml-auto no-padder">
        <ul class="nav command-bar justify-content-sm-end justify-content-start v-center">
            @yield('navbar')
        </ul>
    </div>
</div>
</div>

@if (Breadcrumbs::exists())
{{ Breadcrumbs::view('platform::partials.breadcrumbs') }}
@endif

<div class="d-flex">
<div class="app-content-body" id="app-content-body">
    @include('platform::partials.alert')
    @yield('content')
</div>
</div>
@endsection

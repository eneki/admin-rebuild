<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" data-controller="layouts--html-load">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title','Upesy') - @yield('description','Admin')</title>
    <meta name="csrf_token" content="{{ csrf_token() }}" id="csrf_token" data-turbolinks-permanent>
    <meta name="auth" content="{{ Auth::check() }}" id="auth" data-turbolinks-permanent>
    @if (file_exists(public_path('/css/orchid/orchid.css')))
        <link rel="stylesheet" type="text/css" href="{{ mix('/css/orchid/orchid.css') }}">
    @else
        <link rel="stylesheet" type="text/css" href="{{ orchid_mix('/css/orchid.css', 'orchid') }}">
    @endif

    @stack('head')

    <meta name="turbolinks-root" content="{{ Dashboard::prefix() }}">
    <meta name="dashboard-prefix" content="{{ Dashboard::prefix() }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.css">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

    {{-- Font --}}
    <link
        href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap"
        rel="stylesheet">

    {{-- Zing Chart --}}
    <script src="https://cdn.zingchart.com/zingchart.min.js"></script>

    {{-- Orchid--}}
    <script src="{{ orchid_mix('/js/manifest.js', 'orchid') }}" type="text/javascript"></script>
    <script src="{{ orchid_mix('/js/vendor.js', 'orchid') }}" type="text/javascript"></script>
    <script src="{{ orchid_mix('/js/orchid.js', 'orchid') }}" type="text/javascript"></script>

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>

    {{-- Datatables
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.colVis.min.js"></script> --}}

    {{-- Maps--}}
    {{-- <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
    --}}
    <script defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAC1R81QvjsLKAtc9L7mB-3JI8Jp7UNSh4&callback=initMap">
    </script>

    @foreach (Dashboard::getResource('stylesheets') as $stylesheet)
        <link rel="stylesheet" href="{{ $stylesheet }}">
    @endforeach

    @stack('stylesheets')

    @foreach (Dashboard::getResource('scripts') as $scripts)
        <script src="{{ $scripts }}" defer type="text/javascript"></script>
    @endforeach
</head>

<body>

    <div class="app row m-n" id="app" data-controller="@yield('controller')" @yield('controller-data')>
        <div class="container-fluid w-95">
            <div class="row">
                <div
                    class="aside col-xs-12 col-md-2 offset-xxl-0 col-xl-2 col-xxl-3 no-padder bg-dark print-display-none">
                    <div class="d-md-flex align-items-start flex-column d-sm-block h-full">
                        @yield('body-left')
                    </div>
                </div>
                <div
                    class="col-md col-xl col-xxl-9 bg-white shadow no-padder min-vh-100 overflow-hidden content-background">
                    @yield('body-right')
                </div>
            </div>
        </div>


        @include('platform::partials.toast')
    </div>

    @stack('scripts')

    @yield('pageSpecificScripts')

    {{-- Toastr --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>


</body>

</html>

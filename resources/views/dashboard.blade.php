@extends('platform::dashboard')

@section('title', 'Statistical Overview')

@section('content')
    @if($loggedInUserPartnerId == 1)
        <div class="row container-fluid mb-3 mt-3 incident-tally-container">
            <div class="col col-md-3">
                <div class="row incident-tally--border">
                    <div class="col-8 incident-tally-text">
                        <div>All</div>
                        <div>Incidents</div>
                    </div>
                    <div class="col-4 incident-tally-number">{{ $totalIncidents }}</div>
                </div>
            </div>
            <div class="col col-md-3">
                <div class="row incident-tally--border">
                    <div class="col-8 incident-tally-text">
                        <div>Pending</div>
                        <div>Incidents</div>
                    </div>
                    <div class="col-4 incident-tally-number">{{ $totalPendingIncidents }}</div>
                </div>
            </div>
            <div class="col col-md-3">
                <div class="row incident-tally--border">
                    <div class="col-8 incident-tally-text">
                        <div>Resolved</div>
                        <div>Incidents</div>
                    </div>
                    <div class="col-4 incident-tally-number">{{ $totalResolvedIncidents }}</div>
                </div>
            </div>
            <div class="col col-md-3">
                <div class="row incident-tally--border">
                    <div class="col-8 incident-tally-text">
                        <div>Dispatched</div>
                        <div>Incidents</div>
                    </div>
                    <div class="col-4 incident-tally-number">{{ $totalDispatchedIncidents }}</div>
                </div>
            </div>
        </div>

        @if ($loggedInUserPartnerId == 1)
            <div class="container-fluid mb-3 mt-3 incident-tally-container">
                <div id="premiumAndStandardCustomersChart"></div>
            </div>
        @else
            <div class="container-fluid mb-3 mt-3 incident-tally-container">
                <div id="registeredCustomersChart"></div>
            </div>
        @endif

        <div class="container-fluid mb-3 mt-3 incident-tally-container">
            <div id='testChart'></div>
        </div>
    @else 
        <div class="ml-3">Dashboard goes here</div>
    @endif
@stop

@section('pageSpecificScripts')
    <script>
        var months = @JSON($months),
            standardPartnerCustomers = @JSON($standardPartnerCustomers),
            premiumPartnerCustomers = @JSON($premiumPartnerCustomers),
            premiumAndStandardCustomersJson = {
                type: 'bar',
                title: {
                    text: 'Registration Trend',
                    fontSize: 24,
                },
                "scroll-x": {
                    "handle": {
                        "background-color": "#E80C7A"
                    }
                },
                "scroll-y": {
                    "handle": {
                        "background-color": "#FF0DFF"
                    }
                },
                legend: {
                    draggable: true,
                },
                scaleX: {
                    "zooming": true,
                    "zoom-to": [0, 12],
                    label: {
                        text: 'Months'
                    },
                    itemsOverlap: true,
                    labels: months
                },
                scaleY: {
                    "zooming": true,
                    "zoom-to": [0, 250],
                    label: {
                        text: 'Number of customers'
                    },
                    itemsOverlap: true
                },
                plot: {
                    // Animation docs here:
                    // https://www.zingchart.com/docs/tutorials/styling/animation#effect
                    animation: {
                        effect: 'ANIMATION_EXPAND_BOTTOM',
                        method: 'ANIMATION_STRONG_EASE_OUT',
                        sequence: 'ANIMATION_BY_NODE',
                        speed: 275,
                    }
                },
                series: [{
                        // Plot 1 values, linear data
                        values: premiumPartnerCustomers,
                        text: 'Premium Partner Customers',
                    },
                    {
                        // Plot 2 values, linear data
                        values: standardPartnerCustomers, // Number of users
                        text: 'Standard Partner Customers'
                    }
                ]
            };
        console.log(months)
        zingchart.render({
            id: 'premiumAndStandardCustomersChart',
            data: premiumAndStandardCustomersJson,
            height: 800,
            width: "100%"
        });


        // var registeredCustomers = @JSON($registeredCustomers),
        //     months = @JSON($months),
        //     registeredCustomersJson = {
        //         "type": "line",
        //         "title": {
        //             "text": "Registration Trend",
        //             "font-size": "24px",
        //             "adjust-layout": true
        //         },

        //         "scale-x": {
        //             "zooming": true,
        //             "zoom-to": [0, 12],
        //             label: {
        //                 text: 'Months'
        //             },
        //             labels: months,
        //         },
        //         "series": [{
        //             "values": registeredCustomers,
        //             "text": "Registered Customers",
        //             "line-color": "#007790",
        //         }]
        //     };

        // zingchart.render({
        //     id: 'registeredCustomersChart',
        //     data: registeredCustomersJson,
        //     height: '100%',
        //     width: '100%'
        // });

        var incidentDistribution = @JSON($incidentDistribution);
        var series = [];
        for (var key in incidentDistribution) {
            series.push({
                values: [incidentDistribution[key]],
                text: [key],
            })
        }

        var testjson = {
            "type": "pie",
            "title": {
                "text": "Incident Distribution"
            },
            "plot": {
                "value-box": {
                    "font-size": 10,
                    text: '%t\n%npv%',
                    "font-weight": "normal",
                    "placement": "out"
                }
            },
            "nav": {
                "threshold": "4%",
                "others": {
                    "text": "Others",
                    "background-color": "#999 #333"
                },
                "slice": 0.35,
                "back": {
                    "padding": 10,
                    "font-weight": "bold",
                    "color": "#fff",
                    "font-size": 12,
                    "border": "3px solid #47a",
                    "border-radius": 9,
                    "background-color": "#369",
                    "shadow": true,
                    "shadow-alpha": 0.5
                }
            },
            "series": series
        };

        zingchart.render({
            id: 'testChart',
            data: testjson,
            height: 400,
            width: "100%"
        });

    </script>
@endsection

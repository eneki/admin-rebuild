@push('head')
    <link href="{{ Storage::url('img/upesy_logo.png') }}" id="favicon" rel="icon">
@endpush

<div style="margin-left: 2.5rem;">
    <p class="h2 n-m font-thin v-center">
        <img src="{{ Storage::url('img/upesy_logo.png') }}" alt="" width="20%" height="20%">
        <span class="m-l d-none d-sm-block" style="font-size: 3rem;">
            <strong>Upesy</strong>
        </span>
    </p>
    <p style=" margin-left: 1rem;">A Safer World</p>
</div>

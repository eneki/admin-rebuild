<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class TransactionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subscriptions = DB::table('subscriptions')
                ->join('customers','customers.id','subscriptions.customer_id')
                ->join('partners','customers.partner_id','partners.id')
                ->join('subscription_providers','subscription_providers.subscription_id','subscriptions.id')
                ->whereNull('subscriptions.deleted_at')
                ->whereNull('customers.deleted_at')
                ->whereIn('subscriptions.status',['active','expired'])
                ->select(
                    'subscriptions.invoice_number as invoice_number',
                    'partners.name as partner_app',
                    'subscription_providers.partner_name as partner_name',
                    'subscription_providers.partner_id as partner_id',
                    'customers.partner_id as partner_app_id',
                    'subscriptions.amount as amount',
                    'subscriptions.id as subscription_id'
                );


                // invoice to partner
                $invoiced_to_partner = $subscriptions
                    ->whereNull('subscriptions.invoice_number')
                    ->distinct()
                    ->get()->each(function($subscription){
                    $transactions = DB::table('transactions');
                    $transactions->insert([
                        'invoice_number' => $subscription->invoice_number,
                        'partner_app' => $subscription->partner_app,
                        'partner_name' => $subscription->partner_name,
                        'partner_id' => $subscription->partner_id,
                        'partner_app_id' =>$subscription->partner_app_id,
                        'amount' => $subscription->amount,
                        'discount' => 0,
                        'amount_to_upesy' => 0,
                        'amount_to_partner' => 0,
                        'subscription_id' => $subscription->subscription_id,
                        'invoiced' => 1,
                        'invoice_to' => 'PARTNER',
                    ]);
    
                });

                // invoice to upesy
                $invoiced_to_provider = $subscriptions
                    ->whereNotNull('subscriptions.invoice_number')
                    ->distinct()
                    ->get()->each(function($subscription){
                    $transactions = DB::table('transactions');
                    $transactions->insert([
                        'invoice_number' => $subscription->invoice_number,
                        'partner_app' => $subscription->partner_app,
                        'partner_name' => $subscription->partner_name,
                        'partner_id' =>$subscription->partner_id,
                        'partner_app_id' =>$subscription->partner_app_id,
                        'amount' => $subscription->amount,
                        'discount' => 0,
                        'amount_to_upesy' => 0,
                        'amount_to_partner' => 0,
                        'subscription_id' => $subscription->subscription_id,
                        'invoiced' => 1,
                        'invoice_to' => 'PROVIDER',
                    ]);
    
                });
        }

        
}

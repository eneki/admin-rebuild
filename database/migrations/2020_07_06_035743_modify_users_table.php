<?php

use App\Models\Users\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->json('permissions');
            $table->timestamp('last_login')->nullable();
        });

        $permissions = [
            "platform.index" => "1", 
            "platform.systems.index"=> "1", 
            "platform.systems.roles"=> "1", 
            "platform.systems.users"=> "1", 
            "platform.systems.attachment"=> "1"
        ];

        User::all()->each(function ($user, $key) use ($permissions){
            $user->update(['permissions' => $permissions]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}

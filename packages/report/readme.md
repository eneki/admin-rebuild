# Incident Pacakage for upesy old database

1. Add the following to your composer.json

```json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://source.cloud.google.com/upesy-8898e/report-package"
        }
    ],
    "require": {
        "upesy/report": "dev-master"
    }
} 
```
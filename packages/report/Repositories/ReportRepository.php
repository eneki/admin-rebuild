<?php

namespace Report\Repositories;

use App\Models\Incidents\Incident;
use App\Models\Incidents\Report;
use App\Models\Partners\Partner;
use App\Models\Subscriptions\Subscription;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ReportRepository
{
    private \Illuminate\Database\Query\Builder $reports;

    /**
     * Construct 
     * 
     * @return void
     */
    public function __construct()
    {
        $this->reports = DB::table('reports')
            ->join('customers', 'reports.reported_by', '=', 'customers.id')
            ->join('incidents', 'reports.incident_id', '=', 'incidents.id');
    }

    /**
     * free get beneficiary partner reports
     * 
     * @param array $partner_id 
     * @param array $status
     * @param array $type
     * @param bool $subscribed
     * 
     */
    public function partnerBeneficiaryReports(
        array $partner_id = [1],
        array $status = ['Pending'],
        array $type = ['Security']
    ) {
        $reports = $this->reports;

        $reports = $reports
            ->join('beneficiaries', 'beneficiaries.beneficiary_id', '=', 'reports.reported_by')
            ->join('subscriptions', 'subscriptions.id', '=', 'beneficiaries.subscription_id')
            ->where('subscriptions.status', 'active');

        $reports = $reports
            ->whereIn('reports.status', $status)
            ->whereIn('incidents.name', $type);



        return $reports->distinct()->get();
    }

    /**
     * free get partner reports
     * 
     * @param array $partner_id 
     * @param array $status
     * @param array $type
     * @param bool $subscribed
     * 
     */
    public function partnerReports(
        array $partner_id = [1],
        array $status = ['Pending'],
        array $type = ['Security'],
        bool $subscribed = false
    ) {
        $reports = $this->reports;

        if ($subscribed) {
            $reports = $reports
                ->join('subscriptions', 'subscriptions.id', '=', 'beneficiaries.subscription_id')
                ->where('subscriptions.status', 'active');
        }

        $reports = $reports
            ->whereIn('reports.status', $status)
            ->whereIn('incidents.name', $type);



        return $reports->distinct()->get();
    }

    public function getIncidentTally()
    {
        $today = Carbon::today();

        $allIncidentsToday = Report::where('created_at', '>=', $today->copy())->count();
        $totalIncidents = Cache::get('all_incidents') + $allIncidentsToday;

        $allPendingIncidentsToday = Report::where('created_at', '>=', $today->copy())
            ->where('status', 'pending')
            ->count();
        $totalPendingIncidents = Cache::get('pending_incidents') + $allPendingIncidentsToday;

        $allDispatchedIncidentsToday = Report::where('created_at', '>=', $today->copy())
            ->where('status', 'dispatched')
            ->count();
        $totalDispatchedIncidents = Cache::get('dispatched_incidents') + $allDispatchedIncidentsToday;

        $allResolvedIncidentsToday = Report::where('created_at', '>=', $today->copy())
            ->where('status', 'solved')
            ->count();
        $totalResolvedIncidents = Cache::get('resolved_incidents') + $allResolvedIncidentsToday;

        return [
            'totalPendingIncidents' => $totalPendingIncidents,
            'totalDispatchedIncidents' => $totalDispatchedIncidents,
            'totalIncidents' => $totalIncidents,
            'totalResolvedIncidents' => $totalResolvedIncidents,
        ];
    }

    public function cacheDailyIncidentTally()
    {
        // If Gravity
            // All incidents 
            // Dispatched incidents 
            // Solved incidents 
            // Pending incidents 
        // If G4S
        // If partner has standard/premium equivalent
        // Else

        $partners = Partner::select('id', 'partner_id')->get();
        $today = Carbon::today()->endOfDay();
        $statuses = config('constants.reportStatus');

        foreach ($partners as $partner) {
            if ($partner->id == 1) {
                $allIncidents = Report::where('created_at', '<', $today->copy())
                    ->count();
                Cache::put('all_incidents', $allIncidents);

                $pendingIncidents = Report::where('created_at', '<', $today->copy())
                    ->where('status', 'pending')
                    ->count();
                Cache::put('pending_incidents', $pendingIncidents);

                $resolvedIncidents = Report::where('created_at', '<', $today->copy())
                    ->where('status', 'solved')
                    ->count();
                Cache::put('resolved_incidents', $resolvedIncidents);

                $dispatchedIncidents = Report::where('created_at', '<', $today->copy())
                    ->where('status', 'dispatched')
                    ->count();
                Cache::put('dispatched_incidents', $dispatchedIncidents);
            }
            if ($partner->id == 8) {
                $customersInSubscriptionTable = Subscription::groupBy('customer_id')->pluck('customer_id')->all();

                $incidents = Report::where('is_demo', '<>', 1)
                    ->where(function ($q) use ($customersInSubscriptionTable, $partner) {
                        $q->whereHas('customer', function ($q) use ($customersInSubscriptionTable, $partner) {
                            // inactive or no subscriptions (free customers)
                            $q->where(function ($q) use ($customersInSubscriptionTable, $partner) {
                                $q->where(function ($q) {
                                    $q->whereHas('subscriptions', function ($q) {
                                        $q->where('status', '!=', config('constants.subscriptionStatus.active'));
                                    });
                                })
                                    ->orWhere(function ($q) use ($customersInSubscriptionTable) {
                                        $q->whereNotIn('id', $customersInSubscriptionTable);
                                    });
                            })
                                // g4s customers
                                ->orWhere(function ($q) use ($partner) {
                                    $q->whereHas('subscriptions', function ($q) use ($partner) {
                                        $q->where('status', config('constants.subscriptionStatus.active'))
                                            ->whereHas('providing_partner', function ($q) use ($partner) {
                                                $q->where('partner_id', $partner->id);
                                            });
                                    });
                                });
                        });
                    })
                    ->count();
                Cache::put('all_incidents_partner_' . $partner->id, $incidents);

                foreach ($statuses as $status) {
                    $incidents = Report::where('is_demo', '<>', 1)
                        ->where('status', $status)
                        ->where(function ($q) use ($customersInSubscriptionTable, $partner) {
                            $q->whereHas('customer', function ($q) use ($customersInSubscriptionTable, $partner) {
                                // inactive or no subscriptions (free customers)
                                $q->where(function ($q) use ($customersInSubscriptionTable, $partner) {
                                    $q->where(function ($q) {
                                        $q->whereHas('subscriptions', function ($q) {
                                            $q->where('status', '!=', config('constants.subscriptionStatus.active'));
                                        });
                                    })
                                        ->orWhere(function ($q) use ($customersInSubscriptionTable) {
                                            $q->whereNotIn('id', $customersInSubscriptionTable);
                                        });
                                })
                                    // g4s customers
                                    ->orWhere(function ($q) use ($partner) {
                                        $q->whereHas('subscriptions', function ($q) use ($partner) {
                                            $q->where('status', config('constants.subscriptionStatus.active'))
                                                ->whereHas('providing_partner', function ($q) use ($partner) {
                                                    $q->where('partner_id', $partner->id);
                                                });
                                        });
                                    });
                            });
                        })
                        ->count();
                    Cache::put($status . '_incidents_partner_' . $partner->id, $incidents);
                }
            } else {
                $incidents = Report::where('is_demo', '<>', 1)
                    ->whereHas('customer', function ($q) use ($partner) {
                        $q->whereHas('subscriptions', function ($q) use ($partner) {
                            $q->where('status', config('constants.subscriptionStatus.active'))
                                ->whereHas('providing_partner', function ($q) use ($partner) {
                                    $q->where('partner_id', $partner->id)
                                        ->orWhere('partner_id', $partner->partner_id);
                                });
                        });
                    })
                    ->count();
                Cache::put('all_incidents_partner_' . $partner->id, $incidents);

                foreach ($statuses as $status) {
                    $incidents = Report::where('is_demo', '<>', 1)
                        ->where('status', $status)
                        ->whereHas('customer', function ($q) use ($partner) {
                            $q->whereHas('subscriptions', function ($q) use ($partner) {
                                $q->where('status', config('constants.subscriptionStatus.active'))
                                    ->whereHas('providing_partner', function ($q) use ($partner) {
                                        $q->where('partner_id', $partner->id)
                                            ->orWhere('partner_id', $partner->partner_id);
                                    });
                            });
                        })
                        ->count();
                    Cache::put($status . '_incidents_partner_' . $partner->id, $incidents);
                }
            }
        }

        $incidentIdsAndNames = Incident::select('id', 'name')
            ->get()
            ->toArray();

        $reportCounts = array();
        foreach ($partners as $partner) {
            foreach ($incidentIdsAndNames as $incident) {
                if ($partner->id == 1) {
                    if (isset($reportCounts[$incident['name']])) {
                        $reportCounts[$incident['name']] += Report::where('created_at', '<', $today->copy())
                            ->where('incident_id', $incident['id'])
                            ->count();
                    } else {
                        $reportCounts[$incident['name']] = Report::where('created_at', '<', $today->copy())
                            ->where('incident_id', $incident['id'])
                            ->count();
                    }
                    Cache::put('incident_type_distribution', $reportCounts);
                } elseif ($partners->id == 8) {
                    // Logic...
                } else {
                    $partnerIncidents = array();
                    foreach ($incidentIdsAndNames as $incident) {
                        $partnerIncidents[$incident['name']] = Report::where('is_demo', '<>', 1)
                            ->where('incident_id', $incident['id'])
                            ->whereHas('customer', function ($q) use ($partner) {
                                $q->whereHas('subscriptions', function ($q) use ($partner) {
                                    $q->whereHas('providing_partner', function ($q) use ($partner) {
                                        $q->where('partner_id', $partner->id)
                                            ->orWhere('partner_id', $partner->id);
                                    });
                                });
                            })
                            ->count();
                        Cache::put('incident_type_distribution_partner_' . $partner->id, $partnerIncidents);
                    }
                }
            }
        }
        // Add g4s to constants.php
        // When did G4S take over free customers? Set that as a constant in the constants file
    }
}

FROM raven1994/laravel-app:latest

# install extenstions
RUN install-php-extensions gd pdo_mysql redis zip

# Copy source code to default app path
COPY . $WORKING_DIRECTORY

# write access to the storage and cache folders
RUN chgrp -R www-data $WORKING_DIRECTORY/storage $WORKING_DIRECTORY/bootstrap/cache
RUN chmod -R ug+rwx $WORKING_DIRECTORY/storage $WORKING_DIRECTORY/bootstrap/cache

VOLUME $WORKING_DIRECTORY/storage

# install packages
RUN COMPOSER_MEMORY_LIMIT=-1 composer install -n --no-dev

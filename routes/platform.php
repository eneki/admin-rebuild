<?php

declare(strict_types=1);

use \App\Orchid\Screens\Invoice\{InvoiceDetails, InvoicesScreen};
use App\Orchid\Screens\Examples\ExampleFieldsScreen;
use App\Orchid\Screens\Examples\ExampleLayoutsScreen;
use App\Orchid\Screens\Examples\ExampleScreen;
use App\Orchid\Screens\Partner\PartnerCreateScreen;
use App\Orchid\Screens\Partner\PartnerListScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

/**
 * Invoice routes
 */
Route::screen('invoice/{partner_id}/{partner_app_id}', InvoiceDetails::class)->name('invoice.details');
Route::screen('invoice', InvoicesScreen::class)->name('invoice.list');


// Main
Route::get('/main', [\App\Http\Controllers\DashboardController::class, 'index'])->name('platform.main');
Route::get('/home', [\App\Http\Controllers\DashboardController::class, 'index']);

// Users...
Route::get('users/admins', [\App\Http\Controllers\UserController::class, 'admins'])
    ->name('user.admins');
Route::screen('users/{user}/edit', UserEditScreen::class)->name('platform.systems.users.edit');
Route::get('users/{user}/details', [\App\Http\Controllers\UserController::class, 'details'])
    ->name('user.profile');
Route::screen('users', UserListScreen::class)->name('platform.systems.users');

Route::get('partner/{partner}/users', [\App\Http\Controllers\PartnerController::class, 'users'])
    ->name('partner.users');
Route::get('partner/{partner}/user/show', [\App\Http\Controllers\UserController::class, 'showCreateUser'])
    ->name('partner.user.show');
Route::post('partner/{partner}/user/store', [\App\Http\Controllers\UserController::class, 'store'])
    ->name('partner.user.store');

// Roles...
Route::screen('roles/{roles}/edit', RoleEditScreen::class)->name('platform.systems.roles.edit');
Route::screen('roles/create', RoleEditScreen::class)->name('platform.systems.roles.create');
Route::screen('roles', RoleListScreen::class)->name('platform.systems.roles');

//Partners
Route::get('partner/create', [\App\Http\Controllers\PartnerController::class, 'create'])
    ->name('partner.create');
Route::post('partner/save', [\App\Http\Controllers\PartnerController::class, 'store'])
    ->name('partner.store');
Route::get('partner/{partner}/deactivate', [\App\Http\Controllers\PartnerController::class, 'deactivateOrReactivate'])
    ->name('partner.deactivate');
Route::get('partner/{partner}/details', [\App\Http\Controllers\PartnerController::class, 'show'])
    ->name('partner.details');
Route::post('partner/{partner}/bank/store', [\App\Http\Controllers\PartnerController::class, 'bankStore'])
    ->name('partner.bank.store');
Route::get('partner/{partner}/bank/show', [\App\Http\Controllers\PartnerController::class, 'showEditBankDetails'])
    ->name('partner.bank.show');
Route::get('partners', [\App\Http\Controllers\PartnerController::class, 'index'])
    ->name('partners');

// Packages
Route::get('partner/{partner}/packages', [\App\Http\Controllers\PackageController::class, 'partnerPackages'])
    ->name('partner.packages');

// Services
Route::get('partner/{partner}/show/configure-services', [\App\Http\Controllers\ServiceController::class, 'showPartnerServices'])
    ->name('partner.show.configure-services');

// Incidents
Route::get('pending-incidents', [\App\Http\Controllers\IncidentController::class, 'pendingIncidents'])
    ->name('pending-incidents');

Route::get('dispatched-incidents', [\App\Http\Controllers\IncidentController::class, 'dispatchedIncidents'])
    ->name('dispatched-incidents');

Route::get('resolved-incidents', [\App\Http\Controllers\IncidentController::class, 'resolvedIncidents'])
    ->name('resolved-incidents');

Route::get('pending-incidents/{report}/report/customer/{customer}', [\App\Http\Controllers\IncidentController::class, 'showPendingIncident'])
    ->name('pending-incident-report');

Route::get('dispatched-and-resolved-incidents/{report}/report/customer/{customer}', [\App\Http\Controllers\IncidentController::class, 'showDispatchedIncident'])
    ->name('dispatched-incident-report');

//Route::get('resolved-incidents/{report}/report', [\App\Http\Controllers\IncidentController::class, 'showResolvedIncident'])
//    ->name('resolved-incident-report');

Route::get('incident/{report}/dispatch', [\App\Http\Controllers\IncidentController::class, 'showDispatchIncidentForm'])
    ->name('incident-dispatch-show');

Route::post('incident/{report}/{customer}/dispatch', [\App\Http\Controllers\IncidentController::class, 'dispatchIncident'])
    ->name('incident-dispatch');

Route::get('incident/{report}/act-upon', [\App\Http\Controllers\IncidentController::class, 'showCreateIncidentReportForm'])
    ->name('incident-act-upon-show');

Route::post('incident/{report}/{customer}/act-upon', [\App\Http\Controllers\IncidentController::class, 'actUponIncident'])
    ->name('incident-act-upon');

Route::get('pending-incidents/{report}/report/customer/current-location/refresh', [\App\Http\Controllers\IncidentController::class, 'refreshCurrentLocation'])
    ->name('incident-refresh-current-location');

Route::get('pending-incidents/refresh/{id?}', [\App\Http\Controllers\IncidentController::class, 'refreshPendingIncidents'])
    ->name('pending-incident-refresh');

// Customers
Route::get('customers/partner', [\App\Http\Controllers\CustomerController::class, 'index'])
    ->name('customers');

Route::get('customers/partner/pdf', [\App\Http\Controllers\CustomerController::class, 'exportPdf'])
    ->name('customers.exportPdf');

Route::get('customers/partner/excel', [\App\Http\Controllers\CustomerController::class, 'exportExcel'])
    ->name('customers.exportExcel');

Route::get('customers/partner/csv', [\App\Http\Controllers\CustomerController::class, 'exportCsv'])
    ->name('customers.exportCsv');

Route::get('customer/{partner}/delete', [\App\Http\Controllers\PartnerController::class, 'delete'])
    ->name('customer.delete');

Route::get('customers/{customer}/subscriptions', [\App\Http\Controllers\CustomerController::class, 'customerSubscriptions'])
    ->name('customers.subscriptions');

Route::get('customers/{customer}/guardian/delete', [\App\Http\Controllers\CustomerController::class, 'customerGuardianDelete'])
    ->name('customers.guardian.delete');

Route::get('customers/{customer}', [\App\Http\Controllers\CustomerController::class, 'show'])
    ->name('customers.details');

// Route::get('filter-alert-history/{id}', [\App\Http\Controllers\CustomerController::class, 'filterAlertHistory'])
// ->name('customers.filterAlertHistory');


// Subscriptions

Route::get('subscriptions/partner', [\App\Http\Controllers\SubscriptionController::class, 'index'])
    ->name('subscriptions');

Route::get('subscriptions/partner/pdf', [\App\Http\Controllers\SubscriptionController::class, 'exportPdf'])
    ->name('subscriptions.exportPdf');

Route::get('subscriptions/partner/excel', [\App\Http\Controllers\SubscriptionController::class, 'exportExcel'])
    ->name('subscriptions.exportExcel');

Route::get('subscriptions/partner/csv', [\App\Http\Controllers\SubscriptionController::class, 'exportCsv'])
    ->name('subscriptions.exportCsv');

Route::post('subscriptions/assign-provider/{subscription}', [\App\Http\Controllers\SubscriptionController::class, 'assignProvider'])
    ->name('subscriptions.assignProvider');

Route::get('subscriptions/{subscription}', [\App\Http\Controllers\SubscriptionController::class, 'show'])
    ->name('subscriptions.details');


// Invoices 
Route::get('grouped-invoices', [\App\Http\Controllers\InvoiceController::class, 'getGroupedInvoices'])
    ->name('grouped-invoices');

Route::get('invoices/partner/{partnerId}/partner-app/{partnerAppId}', [\App\Http\Controllers\InvoiceController::class, 'getCustomerInvoices'])
    ->name('invoices.details');

Route::get('invoices/partner/{partnerId}/partner-app/{partnerAppId}/mail', [\App\Http\Controllers\InvoiceController::class, 'sendMail'])
    ->name('invoices.details.mail');

Route::get('subscription-customers/partner/excel', [\App\Http\Controllers\InvoiceController::class, 'exportCsv'])
    ->name('invoices.exportCsv');


// Run Scheduler
Route::get('run-laravel-scheduler', [\App\Http\Controllers\DashboardController::class, 'runScheduler']);

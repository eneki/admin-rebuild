<?php
return [
    'superAdmin' => 'Super Admin',
    'reportStatus' => [
        'dispatched' => 'dispatched',
        'resolved' => 'solved',
        'pending' => 'pending'
    ],
    'subscriptionStatus' => [
        'active' => 'active',
        'expired' => 'expired',
        'cancelled' => 'cancelled'
    ],
    'superAdminId' => 1,
    'reportStatusArray' => [
        'Solved' => 'Solved',
        'Unsolved' => 'Unsolved',
        'False Alarm' => 'False Alarm'
    ]
];
